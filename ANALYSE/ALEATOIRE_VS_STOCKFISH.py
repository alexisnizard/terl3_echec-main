#Objectif : obtenir pour 10 000 games un winrate de aleatoire vs différent élo de stockfish (spoiler : son winrate sera très problablement de ...... 0% )

from stockfish import Stockfish
import matplotlib.pyplot as plt
from terl3_echec.ANALYSE.GameAnalyse import *
from terl3_echec.fonctions_utils import *
import numpy as np
from random import randint
import warnings
import time
import os
import platform

#Tester 10 000 (100 000 idéalement) partie aléatoire vs stockfish. On part de l'echiquier de départ à chaque fois

#Avec matplotlib :
    #Graph camembert des endgames (Nul par 50 coups / Nul par pat / Echec et mat)
    #Graph camembert des endgames Nul / Echec et mat
    #Graph baton de win blanc / noir / match null
    #Nombre de coup moyen par partie
    #Nombre de coup moyen avant d'arrivé à la 1,2,3,4,5,10,15 ème capture de piece
    #Nombre de coup moyen avant la premiere promotion de pion

#Faire les mêmes tests sur un echiquier en fin de partie avec peu de pièce

algebraic_square_map = {
    'h1': 0,
    'g1': 1,
    'f1': 2,
    'e1': 3,
    'd1': 4,
    'c1': 5,
    'b1': 6,
    'a1': 7,

    'h2': 8,
    'g2': 9,
    'f2': 10,
    'e2': 11,
    'd2': 12,
    'c2': 13,
    'b2': 14,
    'a2': 15,

    'h3': 16,
    'g3': 17,
    'f3': 18,
    'e3': 19,
    'd3': 20,
    'c3': 21,
    'b3': 22,
    'a3': 23,
    
    'h4': 24,
    'g4': 25,
    'f4': 26,
    'e4': 27,
    'd4': 28,
    'c4': 29,
    'b4': 30,
    'a4': 31,

    'h5': 32,
    'g5': 33,
    'f5': 34,
    'e5': 35,
    'd5': 36,
    'c5': 37,
    'b5': 38,
    'a5': 39,

    'h6': 40,
    'g6': 41,
    'f6': 42,
    'e6': 43,
    'd6': 44,
    'c6': 45,
    'b6': 46,
    'a6': 47,

    'h7': 48,
    'g7': 49,
    'f7': 50,
    'e7': 51,
    'd7': 52,
    'c7': 53,
    'b7': 54,
    'a7': 55,
    
    'h8': 56,
    'g8': 57,
    'f8': 58,
    'e8': 59,
    'd8': 60,
    'c8': 61,
    'b8': 62,
    'a8': 63
}

def bitboards_to_int(echiquier):
    val = echiquier.ech[0]
    for i in range(1,11):
        val |= echiquier.ech[i]
    return val

def afficher_Echiquier_dans_console_2(echiquier, cpt_global):

    ligne = 8
    derniere_case = (1 << 63)
    for i in range(64):
        if i % 8  == 0:
            print('')
            print(' '+str(ligne)+' ' , end='')
            ligne -= 1
        if (echiquier.ech[0]) & (derniere_case >> i):
            print(' P', end='')
        elif (echiquier.ech[1]) & (derniere_case >> i):
            print(' R', end='')
        elif (echiquier.ech[2]) & (derniere_case >> i):
            print(' N', end='')
        elif (echiquier.ech[3]) & (derniere_case >> i):
            print(' B', end='')
        elif (echiquier.ech[4]) & (derniere_case >> i):
            print(' Q', end='')
        elif (echiquier.ech[5]) & (derniere_case >> i):
            print(' K', end='')

        elif (echiquier.ech[6]) & (derniere_case >> i):
            print(' p', end='')
        elif (echiquier.ech[7]) & (derniere_case >> i):
            print(' r', end='')
        elif (echiquier.ech[8]) & (derniere_case >> i):
            print(' n', end='')
        elif (echiquier.ech[9]) & (derniere_case >> i):
            print(' b', end='')
        elif (echiquier.ech[10]) & (derniere_case >> i):
            print(' q', end='')
        elif (echiquier.ech[11]) & (derniere_case >> i):
            print(' k', end='')
        else:
            print(' .', end='')
    print("\n\n    a b c d e f g h\n")
    print("    Bitboard : "+str(bitboards_to_int(echiquier)))
    print("    Fen : "+to_fen(echiquier, cpt_global)+"\n")
    

def case_to_numero_de_case(case):
    return algebraic_square_map[case]

def numero_de_case_to_type_de_piece(ech, case):
    i = 0
    while i < 12 and not(ech.ech[i] & (1 << case)):
        i+=1
    return i

def jouer_stockfish2(game, stockfish):

    #best_move = stockfish.get_best_move()
    best_move = stockfish.get_best_move_time(1)
    #print("best_move : ", best_move)

    best_move_from = best_move[:2]
    #print("best_move_from : ", best_move_from)
    
    best_move_to = best_move[2:]
    #print("best_move_to : ", best_move_to)

    a = game.get_coup_legaux()
    
    # cas sans promotion 
    if (len(best_move_to)==2):

        numero_de_case_from = case_to_numero_de_case(best_move_from)
        numero_de_case_to = case_to_numero_de_case(best_move_to)
        #print("numero_de_case_from : ", numero_de_case_from)
        #print("numero_de_case_to : ", numero_de_case_to)
        
        for i in a[0]:
            if ((i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
               return i
           
        afficher_Echiquier_dans_console_2(game.echiquier, 1)
        print("coup_legaux :", best_move)
        print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[0]")
        print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
        print("coup_legaux = ", a)
        exit(1)
        
    elif (len(best_move_to)==3):

        best_move_from = best_move[:2]
        #print("best_move_from : ",best_move_from)
        
        best_move_to = best_move[2:-1]
        #print("best_move_to : ",best_move_to)
        
        promotion_to = best_move[-1]
        #print("promotion_to : ",promotion_to)
        
        numero_de_case_from = case_to_numero_de_case(best_move_from)
        numero_de_case_to = case_to_numero_de_case(best_move_to)
        #print("numero_de_case_from : ", numero_de_case_from)
        #print("numero_de_case_to : ", numero_de_case_to)

        #print("a = ",a)
        
        if promotion_to == 'q':
            if game.coul == 1:
                for i in a[1]:
                    if ((i[4]==4) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("q : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)
                
            elif(game.coul == 0):
                for i in a[1]:
                    if ((i[4]==10) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("q : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)

            else:
                print("erreur : couleur inconnu : ", game.coul) 
            
        elif promotion_to == 'r':
            if game.coul == 1:
                for i in a[1]:
                    if ((i[4]==1) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("r : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)
                
            elif(game.coul == 0):
                for i in a[1]:
                    if ((i[4]==7) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("r : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)

            else:
                print("erreur : couleur inconnu : ", game.coul)
            
        elif promotion_to == 'n':
            if game.coul == 1:
                for i in a[1]:
                    if ((i[4]==2) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("n : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)
                
            elif(game.coul == 0):
                for i in a[1]:
                    if ((i[4]==8) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("n : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)

            else:
                print("erreur : couleur inconnu : ", game.coul)
                
        elif promotion_to == 'b':
            if game.coul == 1:
                for i in a[1]:
                    if ((i[4]==3) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("b : ",i)
                        return i
                
                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)
                
            elif(game.coul == 0):
                for i in a[1]:
                    if ((i[4]==9) and (i[-2]==numero_de_case_from) and (i[-1]==numero_de_case_to)):
                        #print("b : ",i)
                        return i

                afficher_Echiquier_dans_console_2(game.echiquier, 1)
                print("coup_legaux :", best_move)
                print("erreur : Pas trouver ce coup legaux dans game.get_coup_legaux()[1]")
                print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
                print("coup_legaux = ", a)

            else:
                print("erreur : couleur inconnu : ", game.coul)
            
        else:
            print("type de promotion incorrect : ", promotion_to)

    else:
        print("erreur best_move incorrect : ", best_move)


def trouver_index(game, le_coup_a_jouer_stockfish):
    index = 0
    #si c'est un coup sans promo on charche dans coup_legaux[0]
    if (len(le_coup_a_jouer_stockfish)==6):
        i = 0
        while (i < len(game.coup_legaux[0]) and (game.coup_legaux[0][i] != le_coup_a_jouer_stockfish)):
            i += 1
        if i >= len(game.coup_legaux[0]):
            afficher_Echiquier_dans_console_2(game.echiquier, 1)
            print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
            print("index = ", i)
            print("pas touver le coup a jouer dans coup_legaux[0]")
            print("le_coup_a_jouer_stockfish : ", le_coup_a_jouer_stockfish)
            print("coup_legaux[0] = ", game.coup_legaux[0])
            exit(2)
        index = i
        #print("index : ", index)
        #print("coup_legaux[0][",index,"] = ", game.coup_legaux[0][index])
        #print("le_coup_a_jouer_stockfish = ", le_coup_a_jouer_stockfish)
    
    #sinon on cherche dans coup_legaux[1]
    else:
        i = 0
        while (i < len(game.coup_legaux[1]) and (game.coup_legaux[1][i] != le_coup_a_jouer_stockfish)):
            i += 1
        if i >= len(game.coup_legaux[1]):
            afficher_Echiquier_dans_console_2(game.echiquier, 1)
            print("game.echiquier.ech = ",[game.echiquier.ech[i] for i in range(15)])
            print("index = ",i)
            print("pas touver le coup a jouer dans coup_legaux[1]")
            print("le_coup_a_jouer_stockfish : ", le_coup_a_jouer_stockfish)
            print("coup_legaux[1] = ", game.coup_legaux[1])
            exit(2)

        index = i + len(game.coup_legaux[0])
        """
        print("i : ", i)
        print("index : ", index)
        print("coup_legaux[1][",i,"] = ", game.coup_legaux[1][i])
        x = index - len(game.coup_legaux[0])
        print("coup_legaux[1][",x,"] = ", game.coup_legaux[1][x])
        print("le_coup_a_jouer_stockfish = ", le_coup_a_jouer_stockfish)
        """
        
    return index

start = time.time()

if (platform.system() == 'Linux'):
    # Dans le path on met l'emplacement de notre fichier stockfich
    stockfish = Stockfish(path="stockfish_14.1_linux_x64_popcnt/stockfish_14.1_src/src/stockfish")

elif (platform.system() == 'Windows'):
    print("ATTENTION: vous devez remplacer le path a la ligne 380 du fichier aleatoire_vs_stockfish par le votre") 
    stockfish = Stockfish(path=r"C:\Users\fureu\Downloads\terl3_echec\terl3_echec\ANALYSE\stockfish_15_win_x64\stockfish_15_x64.exe")

else:
    print("Votre OS n'est pas pris en charge par notre application")
    print("Merci de bien vouloir installer Linux")
    exit(0)
    

while 1:
    print("Quel type d'aléatoire voulez-vous utilisé ?")
    print("0 - Chaque pièce à une probabilité aléatoire d'etre choisit ,puis on choisit aléatoirement un coup parmis un de ses coups")
    print("1 - On choisit un coup aléatoire parmis tous les coups aléatoire possible")
    choix_aleatoire=int(input())
    if (choix_aleatoire==0 or choix_aleatoire==1):
        break
    
print("Sur combien d'échiquier aléatoire voulez vous faire les tests ?")
choix_nbr_echiquier=int(input())


while 1:
    print("Quel mode de jeu voulez-vous ?\n0 : Aleatoire vs Stockfish \n1 : Stockfish vs Aleatoire")
    choix_mode=int(input())
    if (choix_mode==0 or choix_mode==1):
        break

#stockfish.set_elo_rating(500)

if choix_aleatoire==0:
    print('.')

else:
    # mode : Aleatoire vs Stockfish
    if choix_mode == 0:
        i = 0
        endgame=[0,0,0,0]#nul 50 coup ;nul par pat; win blanc ; win noir
        nbr_coup_par_partie=[]
        nbr_coup_avant_capture=[ [] for i in range(15)]
        nbr_coup_avant_capture_quand_blanc_gagnes=[ [] for i in range(15)]
        nbr_coup_avant_capture_quand_noir_gagnes = [[] for i in range(15)]
        nbr_coup_avant_capture_quand_ya_nul = [[] for i in range(15)]
        nbr_no_capture = [0 for i in range(15)] #on compte le nombre de fois où on a pas atteind la Xeme capture
        nbr_coup_avant_ppp=[]
        taille_sans_zero=0
        while i < choix_nbr_echiquier:
            print("Debut game ",i)
            ech = Echiquier()
            game = GameAnalyse(ech,choix_mode) 
            cpt_global = 1
            res=-2
            #print("Echiquier de depart : ")
            #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)
            while 1:
                nbr_coup = len(game.coup_legaux[0])
                nbr_coup_promo = len(game.coup_legaux[1])
                nbr_coup_total = nbr_coup + nbr_coup_promo
                
                if nbr_coup_total == 0:
                    le_coup_a_joue = 0
                else:
                    le_coup_a_joue = randint(0, nbr_coup_total - 1)
                    
                res = game.jouer(le_coup_a_joue)
                if res == 0 or res == -1 or res == 1 or res == 2:
                    break

                #print("Echiquier apres coup aleatoire : ")
                #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)

                fen = to_fen(game.echiquier, cpt_global)
                stockfish.set_fen_position(fen)

                le_coup_a_jouer_stockfish = jouer_stockfish2(game, stockfish)
                index = trouver_index(game, le_coup_a_jouer_stockfish)             
                res = game.jouer(index)
                #print("Echiquier apres coup de Stockfish : ")
                #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)
                cpt_global += 1
                if res == 0 or res == -1 or res == 1 or res == 2:
                    break
            

            if res == 2:#nul par 50 coups
                endgame[0]+=1
            elif res == 0: #nul par pat
                endgame[1] += 1
            elif res == 1:#win blanc
                endgame[2] += 1
            elif res == -1:#win noir
                endgame[3] += 1

            #print("endgame = ", endgame)

            nbr_coup_par_partie.append(game.nbr_coup_joue//2)

            for j in range(15):
                if game.quand_arrive_lattaque[j]!=1:
                    nbr_coup_avant_capture[j].append(game.quand_arrive_lattaque[j])
                else:
                    nbr_no_capture[j]+=1

                if res==1:#victoire blanc
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_blanc_gagnes[j].append(game.quand_arrive_lattaque[j])

                elif res==-1:#victoire noir
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_noir_gagnes[j].append(game.quand_arrive_lattaque[j])
                elif res==2:#nul 50 cops
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_ya_nul[j].append(game.quand_arrive_lattaque[j])


            if game.premiere_promo_pion!=0:
                nbr_coup_avant_ppp.append(game.premiere_promo_pion)
            else:
                taille_sans_zero+=1
                
            print("Fin game ",i)
            i += 1
            
    # mode : Stockfish vs Aleatoire
    else:
        i = 0
        endgame=[0,0,0,0]#nul 50 coup ;nul par pat; win blanc ; win noir
        nbr_coup_par_partie=[]
        nbr_coup_avant_capture=[ [] for i in range(15)]
        nbr_coup_avant_capture_quand_blanc_gagnes=[ [] for i in range(15)]
        nbr_coup_avant_capture_quand_noir_gagnes = [[] for i in range(15)]
        nbr_coup_avant_capture_quand_ya_nul = [[] for i in range(15)]
        nbr_no_capture = [0 for i in range(15)] #on compte le nombre de fois où on a pas atteind la Xeme capture
        nbr_coup_avant_ppp=[]
        taille_sans_zero=0
        while i < choix_nbr_echiquier:
            print("Debut game ",i)
            ech = Echiquier()
            game = GameAnalyse(ech,choix_mode) 
            cpt_global = 1
            res=-2
            #print("Echiquier de depart : ")
            #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)
            while 1:
                fen = to_fen(game.echiquier, cpt_global)
                stockfish.set_fen_position(fen)

                le_coup_a_jouer_stockfish = jouer_stockfish2(game, stockfish)
                #print("le_cou_a_jouer_stockfish : ", le_coup_a_jouer_stockfish)
                #print("game.echiquier.ech =", [game.echiquier.ech[i] for i in range(15)])

                index = trouver_index(game, le_coup_a_jouer_stockfish)
                res = game.jouer(index)
                #print("Echiquier apres coup de Stockfish : ")
                #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)
                cpt_global += 1
                if res == 0 or res == -1 or res == 1 or res == 2:
                    break

                nbr_coup = len(game.coup_legaux[0])
                nbr_coup_promo = len(game.coup_legaux[1])
                nbr_coup_total = nbr_coup + nbr_coup_promo
                
                if nbr_coup_total == 0:
                    le_coup_a_joue = 0
                else:
                    le_coup_a_joue = randint(0, nbr_coup_total - 1)
                    
                res = game.jouer(le_coup_a_joue)
                #print("Echiquier apres coup aleatoire : ")
                #afficher_Echiquier_dans_console_2(game.echiquier, cpt_global)
                if res == 0 or res == -1 or res == 1 or res == 2:
                    break
                
            if res == 2:#nul par 50 coups
                endgame[0]+=1
            elif res == 0: #nul par pat
                endgame[1] += 1
            elif res == 1:#win blanc
                endgame[2] += 1
            elif res == -1:#win noir
                endgame[3] += 1

            #print("endgame = ", endgame)

            nbr_coup_par_partie.append(game.nbr_coup_joue//2)

            for j in range(15):
                if game.quand_arrive_lattaque[j]!=1:
                    nbr_coup_avant_capture[j].append(game.quand_arrive_lattaque[j])
                else:
                    nbr_no_capture[j]+=1

                if res==1:#victoire blanc
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_blanc_gagnes[j].append(game.quand_arrive_lattaque[j])

                elif res==-1:#victoire noir
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_noir_gagnes[j].append(game.quand_arrive_lattaque[j])
                elif res==2:#nul 50 cops
                    if game.quand_arrive_lattaque[j] != 1:
                        nbr_coup_avant_capture_quand_ya_nul[j].append(game.quand_arrive_lattaque[j])


            if game.premiere_promo_pion!=0:
                nbr_coup_avant_ppp.append(game.premiere_promo_pion)
            else:
                taille_sans_zero+=1
                
            print("Fin game ",i)
            i += 1

        
# pour le Diagramme à barres
def addlabels(x, y):
    for e in range(len(x)):
        plt.text(e, y[e], y[e])

# DIAGRAMME CIRCULAIRE des endgames :
noms_2 = ["Nul par 50 coups", "Nul par pat", "Echec et mat"]
nul50_pat_echec_m_2 = [endgame[0],endgame[1],endgame[2]+endgame[3]]

fig, (ax2,ax3)  =  plt.subplots(1,2,figsize=(12,6))

def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct * total / 100.0))
        # si on voulait affiché les pats / regles des 50 coups dans un graph camembert de taille 2 et non de taille 3
        # (au final c'est une mauvaise idée je l'ai pas implémenter mais ca marche)
        #if values == nul50_pat_echec_m_2 and  val == endgame[0]+endgame[1]:
        #    return '{p:.2f}% ({b:d})\n[{v:d} CC ,{h:d} Pat]'.format(p=pct, b=val,v=endgame[0],h=endgame[1])
        return '{p:.2f}%  ({v:d})'.format(p=pct, v=val)

    return my_autopct


ax2.pie(nul50_pat_echec_m_2, labels=noms_2,shadow=True,autopct=make_autopct(nul50_pat_echec_m_2),colors=['#2B9953','#3AB767','#86EF83'])#radius=0.7
ax2.set_title('Répartition des fins de parties sur les ' + str(choix_nbr_echiquier) + ' échiquiers')

plt.subplots_adjust(wspace=0.4)

wblanc_wnoir_null = [endgame[2], endgame[3], endgame[0] + endgame[1]]
nom_histo = ['Win Blanc', 'Win Noir', 'Match Nul']
ajust_text=ax3.bar(nom_histo, wblanc_wnoir_null,width=0.5,align='center',color=['#6CC8B0',(0.3,0.7,0.6,0.9),(0.3,0.6,0.5,0.9)])
ax3.set_title('Répartition des victoires et matchs nuls sur les ' + str(choix_nbr_echiquier) + ' échiquiers')
ax3.set_ylabel("Nombre de parties")

for p in ajust_text:
    height = p.get_height()
    ax3.text(x=p.get_x() + p.get_width() / 2, y=p.get_y() + height * 1.01,
             s="{}".format(height),
             ha='center')

# Diagramme à barres nbr coups moyen par partie / nbr coups moyen avant ppp :

nbr_coup_par_partie = np.array(nbr_coup_par_partie)
nbr_coup_avant_ppp = np.array(nbr_coup_avant_ppp)

taille_avec_zero=len(nbr_coup_avant_ppp)

a1 = np.NaN if np.all(nbr_coup_par_partie != nbr_coup_par_partie) else np.nanmean(nbr_coup_par_partie)
a = np.around(a1)

b1 = np.NaN if np.all(nbr_coup_avant_ppp != nbr_coup_avant_ppp) else np.nanmean(nbr_coup_avant_ppp)
b = np.around(b1)

c1 = np.NaN if np.all(nbr_coup_par_partie != nbr_coup_par_partie) else np.nanmedian(nbr_coup_par_partie)
c = np.around(c1)

d1 =  np.NaN if np.all(nbr_coup_avant_ppp != nbr_coup_avant_ppp) else np.nanmedian(nbr_coup_avant_ppp)
d = np.around(d1)

nbr_coup = [a, b, c, d]

labels = ['Avant de terminer une partie', 'Avant la première promotion de pion']

moyenne = [nbr_coup[0], nbr_coup[1]]  # premier index : nbr de coup dans une partie , deuxieme index : nbr de coup dans une partie avant ppp
median = [nbr_coup[2], nbr_coup[3]]

mi1 = np.NaN if np.all(nbr_coup_par_partie != nbr_coup_par_partie) else np.nanmin(nbr_coup_par_partie)
#print("mi1 = ", mi1)

mi2 = np.NaN if np.all(nbr_coup_avant_ppp != nbr_coup_avant_ppp) else np.nanmin(nbr_coup_avant_ppp)
#print("mi2 = ", mi2)

min = [mi1, mi2]

ma1 = np.NaN if np.all(nbr_coup_par_partie != nbr_coup_par_partie) else np.nanmax(nbr_coup_par_partie)
#print("ma1 = ", ma1)

ma2 = np.NaN if np.all(nbr_coup_avant_ppp != nbr_coup_avant_ppp) else np.nanmax(nbr_coup_avant_ppp)
#print("ma2 = ", ma2)

max = [ma1, ma2]

x = np.arange(len(labels))
width = 0.15

fig, (ax ,ax_p)= plt.subplots(1,2,figsize=(13,5))
rects1 = ax.bar(x + 0.00, moyenne, width,color=(0.3,0.5,0.4,0.9) ,label='Moyen')
rects2 = ax.bar(x +0.15, median, width,color=(0.3,0.6,0.5,0.9), label='Médian')
rects3 = ax.bar(x + 0.30, min, width,color=(0.3,0.7,0.6,0.9), label='Minimum')
rects4 = ax.bar(x + 0.45, max, width,color=(0.3,0.8,0.7,0.9), label='Maximum')


# camembert : nombre partie avec promo ,nombre partie sans promo
avec_sans_ppp=[taille_avec_zero,taille_sans_zero]
ax_p.pie(avec_sans_ppp, labels=["Avec promotion", "Sans promotion"], shadow=True, autopct='%1.1f%%', colors=['#2B9953', '#86EF83'])
ax_p.set_title("Pourcentage des parties ayant eu au moins une promotion de pion")

x1, x2, y1, y2 = ax.axis()
ax.axis((x1, x2, y1, y2 + 25))#on augmente un peu en haut


ax.set_ylabel('Nombre de coups')
ax.set_title("Nombre de coups nécessaire pour terminer une partie (gauche)\n et promouvoir notre premier pion (droite). Testé sur " +
             str(choix_nbr_echiquier) + "\néchiquiers (à gauche) et "+str(taille_avec_zero)+" échiquiers (à droite)")
ax.set_xticks([r + 0.15 for r in range(2)], ['Avant de terminer une partie', 'Avant la première promotion de pion'])
ax.legend()


def autolabel(rects):#met un label au dessus de chaque graph
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)

fig.tight_layout()

#Pr ttes partie (exclue les 1) ,lorsque ya win blanc ,lorsque ya win noir ,lorsque ya pat, lorsque ya nul 50 coup

nbr_coup_avant_capture = np.array(nbr_coup_avant_capture,dtype=object)
nbr_coup_avant_capture_quand_blanc_gagnes=np.array(nbr_coup_avant_capture_quand_blanc_gagnes,dtype=object)
nbr_coup_avant_capture_quand_noir_gagnes=np.array(nbr_coup_avant_capture_quand_noir_gagnes,dtype=object)
nbr_coup_avant_capture_quand_ya_nul=np.array(nbr_coup_avant_capture_quand_ya_nul,dtype=object)
les_x=[i for i in range(1,16)]

les_moyennes=[]
les_moyennes_b=[]
les_moyennes_n=[]
les_moyennes_nul=[]
for i in range(15):
    if len(nbr_coup_avant_capture[i])>0:
        les_moyennes.append(round(np.mean(nbr_coup_avant_capture[i])))

    if len(nbr_coup_avant_capture_quand_blanc_gagnes[i])>0:
        les_moyennes_b.append(round(np.mean(nbr_coup_avant_capture_quand_blanc_gagnes[i])))


    if len(nbr_coup_avant_capture_quand_noir_gagnes[i])>0:
        les_moyennes_n.append(round(np.mean(nbr_coup_avant_capture_quand_noir_gagnes[i])))


    if len(nbr_coup_avant_capture_quand_ya_nul[i])>0:
        les_moyennes_nul.append(round(np.mean(nbr_coup_avant_capture_quand_ya_nul[i])))



fig, (ax4, ax5) = plt.subplots(1, 2, figsize=(18, 6),gridspec_kw={'width_ratios': [2, 1]})

ax4.plot(np.arange(1,len(les_moyennes)+1),les_moyennes, label="Pour toutes les parties",marker='o')
ax4.plot(np.arange(1,len(les_moyennes_b)+1), les_moyennes_b, label="Lorsque les blancs gagnent", marker='o')
ax4.plot(np.arange(1,len(les_moyennes_n)+1), les_moyennes_n, label="Lorsque les noirs gagnent", marker='o')
ax4.plot(np.arange(1,len(les_moyennes_nul)+1), les_moyennes_nul, label="Lorsqu'il y a nul par 50 coups", marker='o')
ax4.set_title('Nombre de coups moyens avant d\'arriver à la n-ème captures de pièces.\nTesté sur ' + str(choix_nbr_echiquier) + ' échiquiers')
ax4.set_ylabel("Nombre de coups moyens")
ax4.set_xlabel("Capture n°")
ax4.legend()

tab_nbr_ech=[choix_nbr_echiquier for i in range(15)]

tab_nbr_ech=[tab_nbr_ech[i]-nbr_no_capture[i] for i in range(15)]
tab_noms=[str(i) for i in range(1,16)]


transfo_pourcentage=ax5.bar(tab_noms, tab_nbr_ech,width=0.5,align='center',color=(0.3,0.6,0.5,0.9))

for p in transfo_pourcentage:
    height = p.get_height()
    en_pourcent=int((height/choix_nbr_echiquier) * 100)
    ax5.text(x=p.get_x() + p.get_width() / 2, y=p.get_y() + height *1.02,
            s="{}%".format(en_pourcent),
            ha='center',
            fontsize='x-small')




ax5.set_ylabel('Nombre de parties')
ax5.set_xlabel('Capture n°')
ax5.set_title("Nombre de parties allant jusqu'à la n-ème capture")

end = time.time()

min=(end - start) // 60
if min !=0:
    sc=(end - start)-(min*60)
else:
    sc=end - start


print("Temps mis pour générer et analyser les "+str(choix_nbr_echiquier)+" échiquiers : ",min, " mins ",sc," secs")

plt.show()

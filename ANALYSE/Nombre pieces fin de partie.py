import numpy as np

with open('trace_d\'execution') as f:
    traces=f.readlines()

def separer_victoire_def_nul(trace):
    list_endgame=[]
    for t in trace:
        a = t.split('par ')
        a = a[1].split(' ,le')
        list_endgame.append(a[0])

    return list_endgame

def garder_que_fen(trace):
    list_fen=[]
    for t in trace:
        a=t.split('n :  ')
        a =a[1].split(' ')
        list_fen.append(a[0])

    return list_fen


def fen_to_nbr_piece(fen):
    cpt_n=0
    cpt_b=0
    noires=['p','r','n','b','q','k']
    blanches = ['P','R', 'N', 'B', 'Q', 'K']
    for ff in fen:
        if ff in noires:
            cpt_n+=1
        if ff in blanches:
            cpt_b+=1

    return [cpt_b,cpt_n]

nbr_piece_b=[]
nbr_piece_n=[]
for i in garder_que_fen(traces):
    print(fen_to_nbr_piece(i))
    nbr_piece_b.append(fen_to_nbr_piece(i)[0])
    nbr_piece_n.append(fen_to_nbr_piece(i)[1])



print(np.array(nbr_piece_b))
print(np.array(nbr_piece_n))

print("MOYENNE DU NOMBRE DE PIECE BLANCHES EN FIN DE PARTIES ",np.mean(np.array(nbr_piece_b)))
print("MOYENNE DU NOMBRE DE PIECE NOIRES EN FIN DE PARTIES ",np.mean(np.array(nbr_piece_n)))

print("MEDIAN DU NOMBRE DE PIECE BLANCHES EN FIN DE PARTIES ",np.median(np.array(nbr_piece_b)))
print("MEDIAN DU NOMBRE DE PIECE NOIRES EN FIN DE PARTIES ",np.median(np.array(nbr_piece_n)))

cpt_vic_b=0
cpt_vic_n=0
cpt_match_nul_50=0
cpt_match_nul_pat=0
for j in separer_victoire_def_nul(traces):
    if j == "MATCH NUL 50 coups":
        cpt_match_nul_50+=1
    elif j == "VICTOIRE BLANCHE":
        cpt_vic_b+=1
    elif j == "VICTOIRE NOIRE":
        cpt_vic_n+=1
    elif j=="MATCH NUL PAT":
        cpt_match_nul_pat+=1
    else:
        print("ERREUR")

print("match nul 50 coups : ",cpt_match_nul_50)
print("victoire blanches : ",cpt_vic_b)
print("victoire noires : ",cpt_vic_n)
print("match nul pat : ",cpt_match_nul_pat)
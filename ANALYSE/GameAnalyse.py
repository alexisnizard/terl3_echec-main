from terl3_echec.Echiquier import *
from terl3_echec.fonctions_utils import *
import copy

#Ceci est une copie de la class Game ,avec quelques modification en plus utile pour l'analyse des données

class GameAnalyse:

    def __init__(self,echiquier,notre_couleur):

        # Notre echiquier de depart (on en fait une copie ça sera plus pratique pour monte carlo)
        self.echiquier=Echiquier(echiquier.ech[0],echiquier.ech[1],echiquier.ech[2],echiquier.ech[3],echiquier.ech[4],
                                 echiquier.ech[5],echiquier.ech[6],echiquier.ech[7],echiquier.ech[8],echiquier.ech[9],
                                 echiquier.ech[10],echiquier.ech[11],echiquier.ech[12],echiquier.ech[13],echiquier.ech[14],copy.deepcopy(echiquier.ech_position))

        #notre couleur
        self.coul=notre_couleur
        #nbr coup joué:
        self.nbr_coup_joue=0

        #cb de coup joué avant capture de piece
        self.quand_arrive_lattaque=[1 for i in range(15)]
        self.on_avance_lindice=0

        self.premiere_promo_pion=0
        self.bool_ppp=1

        # Les coups légaux disponibles
        if self.echiquier.ech[12] & 1:  # si c'est aux blancs de joué
            self.coup_legaux = self.echiquier.gen_coup_legaux(0)
            # coup legaux pour le RN:
            self.coup_legaux_RN = self.coup_legaux

        else:  # si c'est aux noirs
            self.coup_legaux = self.echiquier.gen_coup_legaux(1)

        # On va compté le nombre de piece et renvoyé match nul qd il ya - de 6 piece ds l'echiq (3 blanc 3 noir) ,c'est pour rendre plus rapide le RN
        self.cpt_piece_blanche = 0
        self.cpt_piece_noir = 0
        for nbr_p in range(6):
            self.cpt_piece_blanche += self.cpt_nbr_piece(self.echiquier.ech[nbr_p])
            self.cpt_piece_noir += self.cpt_nbr_piece(self.echiquier.ech[nbr_p + 6])


    # renvoit le nombre de piece dans un bitboard
    def cpt_nbr_piece(self, bitboard):
        cpt = 0

        for i in range(64):
            if (1 << i) & bitboard:
                cpt += 1

        return cpt

    # 0 si c'est aux blancs de joué ,1 si c'est aux noirs
    def a_quel_couleur_de_jouer(self):
        if self.echiquier.ech[12] & 1:
            return 0
        else:
            return 1

    # on retourne le tableau des coups legaux
    def get_coup_legaux(self):
        return self.coup_legaux

    #on retourne les coups legaux en notation algébrique
    def get_coup_legaux_NA(self):
        coup_rendu = []

        # on append les coups legaux
        for i in self.coup_legaux[0]:
            coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

        # on append les promotions de pions
        for i in self.coup_legaux[1]:

            if i[4] == 4 or i[4] == 10:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
            elif i[4] == 1 or i[4] == 7:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
            elif i[4] == 2 or i[4] == 8:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
            else:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')

        return sorted(coup_rendu)


    # on retourne les coups legaux en notation algébrique pour le RN
    def get_coup_legaux_NA_RN(self):

        tab_bonne_index = []

        if not self.echiquier.ech[12] & 1:
            copy_echiquier = copy.deepcopy(self.echiquier)  # on fait une copie qu'on redonnera à la fin
            self.echiquier.flip_echiquier_verticalement()  # on flip verticalement
            self.echiquier.ech[12] -= 1  # on fait croire que c'est au blancs de joué
            self.echiquier.ech_position = [self.echiquier.get_position(self.echiquier.ech[i]) for i in
                                           range(12)]  # on change echiq position
            coup_legaux_RN = self.echiquier.gen_coup_legaux(0)

            for i in coup_legaux_RN[0]:
                cpt_index = 0
                for j in self.coup_legaux[0]:

                    if i[-1] ^ 56 == j[-1] and i[-2] ^ 56 == j[-2]:
                        tab_bonne_index.append(cpt_index)
                        break
                    cpt_index += 1

            for i in coup_legaux_RN[1]:
                cpt_index = len(coup_legaux_RN[0])
                for j in self.coup_legaux[1]:
                    if i[-4] == (j[-4] - 6) and i[-1] ^ 56 == j[-1] and i[-2] ^ 56 == j[-2]:
                        tab_bonne_index.append(cpt_index)
                        break
                    cpt_index += 1
            self.echiquier = copy_echiquier

        else:
            # coup legaux pour le RN:
            coup_legaux_RN = self.coup_legaux

        coup_rendu = []
        # on append les coups legaux
        for i in coup_legaux_RN[0]:
            coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

        # on append les promotions de pions
        for i in coup_legaux_RN[1]:

            if i[4] == 4 or i[4] == 10:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
            elif i[4] == 1 or i[4] == 7:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
            elif i[4] == 2 or i[4] == 8:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
            else:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')

        return coup_rendu, tab_bonne_index

    #TODO : A implémenter
    def trois_echiq_identiques(self):
        print(".")

    # TODO : A implémenter
    def insuffisance_piece(self):
        print(".")


    # -1 les noirs ont gagné ,0 match nul, 1 les blancs on gagné
    def verif_cas_terminaux(self):

        # ya 90% de chance que la game se finit en 50 coups à cause des poids aleatoire, alors on boost le training du RN ainsi
        #if self.cpt_piece_noir <= 3 and self.cpt_piece_blanche <= 3:
        #    return 2  # match nul

        #regle 50 coups
        if self.echiquier.ech[14]>=100:
            #print("Debug : match nul par la regle des 50 coups")
            return 2 #match nul

        #regle echec et mat OU pat
        if not(self.coup_legaux[0]) and not(self.coup_legaux[1]): #si il n'y a aucun coup possible a joué

            if self.echiquier.ech[12] & 1:  # si c'est aux blancs de jouer
                attaque_ennemie = self.echiquier.gen_attaque_global(1)
                if attaque_ennemie & self.echiquier.ech[5]:# si notre roi est en echec
                    #print("Debug : victoire noire par echec et mat")
                    return -1 #victoire des noirs
                else:
                    #print("Debug : match nul par la regle du pat")
                    return 0
            else:
                attaque_ennemie = self.echiquier.gen_attaque_global(0)
                if attaque_ennemie & self.echiquier.ech[11]:  # si notre roi est en echec
                    #print("Debug : victoire blanche par echec et mat")
                    return 1  # victoire des blancs
                else:
                    #print("Debug : match nul par la regle du pat")
                    return 0

        #TODO:
        #regle 3 echiquier identiques

        #TODO:
        #regle insuffisance de piece

        return 3  # sinon on retourne 3 et on continue de joué la partie


    # on passe en parametre l'index du tableau de self.coup_legaux[0] ,si il est > len(self.coup_legaux[0]) c'est l'index de self.coup_legaux[1]
    # (donc on joue une promotion de pion)
    # en plus de modifié les echiquiers allié et ennemie (si il y a une capture de piece) et l'echiquier de position ,on modifie les droit de rock ,
    # de prise en passant et on incremente le compteur de demi coup joué sans capture où mouvement de pion.
    # Apres avoir fait tout ca on met a jour la liste des coups legaux pour le prochain coup et on change le tour de role
    def jouer(self,index):

        fin_partie=self.verif_cas_terminaux()
        if fin_partie!=3: #si on a un return autre que 3 on arrete la partie
            return fin_partie

        if self.echiquier.ech[12] & 1: #si c'est aux blancs de jouer
            index_tour=1
            pos_roi_rock = 1
            si_pos_roi_rock = [0, 2]
            si_non_pos_roi_rock = [7, 4]
            huit = 8
            match_notre_coul=0
        else:
            index_tour = 7
            pos_roi_rock = 57
            si_pos_roi_rock=[56,58]
            si_non_pos_roi_rock = [63, 60]
            huit = -8
            match_notre_coul = 1

        pas_de_capture_ou_mouvement_pion=1
        reset_en_passant=1
        not_promo = 1

        if self.coup_legaux[1]:
            if index >= len(self.coup_legaux[0]):
                index-= len(self.coup_legaux[0])
                not_promo=0

        #print("Ech pos:",self.echiquier.ech_position)


        if not_promo:# si on veut joué un coup legaux qui n'est pas une promotion de pion
            #print("coup leg: ", self.coup_legaux[0][index])
            # on modifie l'echiquier allié, si c'est un rock c'est l'echiquier du roi allié qui est modifié
            self.echiquier.ech[self.coup_legaux[0][index][0]]=self.coup_legaux[0][index][1]

            # on modifie l'echiquier position (si rock c'est celui du roi qui est modifié)
            for i in range(len(self.echiquier.ech_position[self.coup_legaux[0][index][0]])):# 2 iterations en moyenne
                if self.echiquier.ech_position[self.coup_legaux[0][index][0]][i]==self.coup_legaux[0][index][-2]:
                    self.echiquier.ech_position[self.coup_legaux[0][index][0]][i]=self.coup_legaux[0][index][-1]
                    break


            if self.coup_legaux[0][index][2] != 5:  # si l'index ne correspond pas à celui du roi blanc (== on a manger une piece ou on rock)

                # on modifie l'echiquier ennemie ,si c'est un rock c'est lechiquier de la tour allié qui est modifié
                self.echiquier.ech[self.coup_legaux[0][index][2]] = self.coup_legaux[0][index][3]

                if self.coup_legaux[0][index][2]!=index_tour: # si on ne rock pas

                    if index_tour == 1:
                        self.cpt_piece_noir-=1
                    else:
                        self.cpt_piece_blanche-=1


                    # on supprime la position de l'echiquier position
                    copy_ech_position=[]

                    if self.echiquier.ech[13]!=0 and self.coup_legaux[0][index][-1]== self.echiquier.ech[13] \
                            and (self.coup_legaux[0][index][0] == 0 or self.coup_legaux[0][index][0] == 6): # en passant

                        for i in range(len(self.echiquier.ech_position[self.coup_legaux[0][index][2]])):
                            if self.echiquier.ech_position[self.coup_legaux[0][index][2]][i] != \
                                    self.coup_legaux[0][index][-1]-huit:
                                copy_ech_position.append(self.echiquier.ech_position[self.coup_legaux[0][index][2]][i])
                        self.echiquier.ech_position[self.coup_legaux[0][index][2]] = copy_ech_position

                    else:
                        for i in range(len(self.echiquier.ech_position[self.coup_legaux[0][index][2]])):
                            if self.echiquier.ech_position[self.coup_legaux[0][index][2]][i]!=self.coup_legaux[0][index][-1]:
                                copy_ech_position.append(self.echiquier.ech_position[self.coup_legaux[0][index][2]][i])
                        self.echiquier.ech_position[self.coup_legaux[0][index][2]]=copy_ech_position

                    if match_notre_coul==self.coul:
                        if self.on_avance_lindice < 15:
                            if self.on_avance_lindice != 14:
                                self.quand_arrive_lattaque[self.on_avance_lindice+1] += self.quand_arrive_lattaque[self.on_avance_lindice]
                        self.on_avance_lindice+=1  # on augmente l'index car on a manger une piece

                    pas_de_capture_ou_mouvement_pion=0 #si ya une capture le bool passe a false
                    self.echiquier.ech[14]=0
                else: #si on rock
                    if match_notre_coul == self.coul:
                        if self.on_avance_lindice < 15:
                            self.quand_arrive_lattaque[self.on_avance_lindice] += 1  # on augmente le compteur car on ne mange pas de piece

                    if self.coup_legaux[0][index][-1]==pos_roi_rock:

                        # on modifie l'echiquier position de la tour
                        for i in range(len(self.echiquier.ech_position[self.coup_legaux[0][index][2]])):
                            if self.echiquier.ech_position[self.coup_legaux[0][index][2]][i] == si_pos_roi_rock[0]:
                                self.echiquier.ech_position[self.coup_legaux[0][index][2]][i] = si_pos_roi_rock[1]
                                break

                    else:
                        for i in range(len(self.echiquier.ech_position[self.coup_legaux[0][index][2]])):
                            if self.echiquier.ech_position[self.coup_legaux[0][index][2]][i] == si_non_pos_roi_rock[0]:
                                self.echiquier.ech_position[self.coup_legaux[0][index][2]][i] = si_non_pos_roi_rock[1]
                                break
            else:
                if match_notre_coul == self.coul:
                    if self.on_avance_lindice < 15:
                        self.quand_arrive_lattaque[self.on_avance_lindice] += 1  # on augmente le compteur car on ne mange pas de piece

            if self.coup_legaux[0][index][0]==0 or self.coup_legaux[0][index][0]==6:#si un pion avance
                pas_de_capture_ou_mouvement_pion = 0 #le bool passe a false
                self.echiquier.ech[14]=0
                if abs(self.coup_legaux[0][index][-1] - self.coup_legaux[0][index][-2]) == 16:#si on avance le pion de 2
                    self.echiquier.ech[13] = self.coup_legaux[0][index][-2] + huit #on uptade l'index en passant
                    reset_en_passant=0

            if self.echiquier.ech[12] & 30:# si au moins un des 4 rocks est encore possible
                if self.coup_legaux[0][index][0]== 5: #si le roi blanc se deplace
                    self.echiquier.ech[12] = self.echiquier.ech[12] & 25 #11001 ,on rend impossible les rocks blancs pour le restant de la game

                if self.coup_legaux[0][index][0] == 11:  # si le roi noir se deplace
                    self.echiquier.ech[12] = self.echiquier.ech[12] & 7  # 00111 ,on rend impossible les rocks noir pour le restant de la game

                if self.coup_legaux[0][index][0] == 1:  # si une tour blanche se deplace
                    if self.coup_legaux[0][index][-2]==0:
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 29  # 11101 ,on rend impossible les rocks blancs coté roi
                    else:
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 27

                if self.coup_legaux[0][index][0] == 7: # si une tour noir se deplace
                    if self.coup_legaux[0][index][-2]==56:
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 23  # 10111 ,on rend impossible les rocks blancs coté roi
                    else:
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 15
                
                if self.coup_legaux[0][index][2] == 1: # si une tour blanche est capturer
                    if self.coup_legaux[0][index][-1] == 0: # si c'est la tour du coté du roi qui est capturer
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 29 # 11101 ,on rend impossible les rocks blancs coté roi
                    else:
                        if self.coup_legaux[0][index][-1]==7: #si c'est la tour coter de la reine qui est capturer
                            self.echiquier.ech[12] = self.echiquier.ech[12] & 27 # 11011 , on rend impossible les rocks blancs coté reine

                if self.coup_legaux[0][index][2] == 7: # si une tour noir est capturer
                    if self.coup_legaux[0][index][-1]==56: # si c'est la tour du coter du roi qui est capturer 
                        self.echiquier.ech[12] = self.echiquier.ech[12] & 23  # 10111 ,on rend impossible les rocks noirs coté roi
                    else:
                    	if self.coup_legaux[0][index][-1]==63: # si c'est la tour du coter de la reine qui est capturer
                            self.echiquier.ech[12] = self.echiquier.ech[12] & 15 # 01111 , on rend impossible les rocks noirs coté reine

        else: # si ya promotion de pion
            #print("coup leg: ", self.coup_legaux[1][index])
            if match_notre_coul == self.coul and self.bool_ppp:
                self.premiere_promo_pion=(self.nbr_coup_joue+1) // 2 #on divise par 2 car on veut que nos coups a nous
                self.bool_ppp=0

            pas_de_capture_ou_mouvement_pion = 0 #le bool passe a false car un pion avance forcement
            self.echiquier.ech[14]=0
            # on modifie l'echiquier allié (celui du pion)
            self.echiquier.ech[self.coup_legaux[1][index][0]] = self.coup_legaux[1][index][1]

            # on supprime le pion de l'echiquier position
            copy_ech_position = []
            for i in range(len(self.echiquier.ech_position[self.coup_legaux[1][index][0]])):
                if self.echiquier.ech_position[self.coup_legaux[1][index][0]][i] != self.coup_legaux[1][index][-2]:
                    copy_ech_position.append(self.echiquier.ech_position[self.coup_legaux[1][index][0]][i])

            self.echiquier.ech_position[self.coup_legaux[1][index][0]] = copy_ech_position
            # on modifie l'echiquier allié (celui de la dame/tour/cavalier/fou)
            self.echiquier.ech[self.coup_legaux[1][index][4]] = self.coup_legaux[1][index][5]

            # on ajoute la dame/tour/cavalier/fou dans l'echiquier position
            self.echiquier.ech_position[self.coup_legaux[1][index][4]].append(self.coup_legaux[1][index][-1])


            if self.coup_legaux[1][index][2] != 5:  # si l'index ne correspond pas à celui du roi blanc (== on a manger une piece)
                # on modifie l'echiquier ennemie
                self.echiquier.ech[self.coup_legaux[1][index][2]] = self.coup_legaux[1][index][3]

                # on supprime la position de l'ennemie de l'echiquier position
                copy_ech_position = []
                for i in range(len(self.echiquier.ech_position[self.coup_legaux[1][index][2]])):
                    if self.echiquier.ech_position[self.coup_legaux[1][index][2]][i] != self.coup_legaux[1][index][-1]:
                        copy_ech_position.append(self.echiquier.ech_position[self.coup_legaux[1][index][2]][i])

                self.echiquier.ech_position[self.coup_legaux[1][index][2]] = copy_ech_position
                if match_notre_coul == self.coul:
                    if self.on_avance_lindice < 15:
                        if self.on_avance_lindice != 14:
                            self.quand_arrive_lattaque[self.on_avance_lindice + 1] += self.quand_arrive_lattaque[
                                self.on_avance_lindice]
                    self.on_avance_lindice += 1  # on augmente l'index car on a manger une piece
                  
                if self.echiquier.ech[12] & 30:
                    if self.coup_legaux[1][index][2] == 1: # si une tour blanche est capturer
                        if self.coup_legaux[1][index][-1] == 0: # si c'est la tour du coté du roi qui est capturer
                            self.echiquier.ech[12] = self.echiquier.ech[12] & 29 # 11101 ,on rend impossible les rocks blancs coté roi
                        else:
                            if self.coup_legaux[1][index][-1]==7: #si c'est la tour coter de la reine qui est capturer
                                self.echiquier.ech[12] = self.echiquier.ech[12] & 27 # 11011 , on rend impossible les rocks blancs coté reine
                                
                    if self.coup_legaux[1][index][2] == 7: # si une tour noir est capturer
                        if self.coup_legaux[1][index][-1]==56: # si c'est la tour du coter du roi qui est capturer 
                            self.echiquier.ech[12] = self.echiquier.ech[12] & 23  # 10111 ,on rend impossible les rocks noirs coté roi
                        else:
                    	    if self.coup_legaux[1][index][-1]==63: # si c'est la tour du coter de la reine qui est capturer
                                self.echiquier.ech[12] = self.echiquier.ech[12] & 15 # 01111 , on rend impossible les rocks noirs coté reine 

            else :
                if match_notre_coul == self.coul:
                    if self.on_avance_lindice < 15:
                        self.quand_arrive_lattaque[self.on_avance_lindice] += 1  # on augmente le compteur car on ne mange pas de piece


        #print("Ech pos apres:", self.echiquier.ech_position)
        #print()


        if pas_de_capture_ou_mouvement_pion:
            self.echiquier.ech[14]+=1 #on incremente le compter de demi-coup

        if reset_en_passant:
            self.echiquier.ech[13]=0

        if index_tour==1:#si c'etait au blanc de joué
            self.coup_legaux = self.echiquier.gen_coup_legaux(1)# on calcul coup pseudo legaux des noirs
            self.echiquier.ech[12]=self.echiquier.ech[12] - 1# on change le tour ,c'est au noir de joué mainteant
        else: # et inversement
            self.coup_legaux = self.echiquier.gen_coup_legaux(0)
            self.echiquier.ech[12] = self.echiquier.ech[12] + 1

        self.nbr_coup_joue+=1

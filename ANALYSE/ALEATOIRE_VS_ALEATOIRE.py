import matplotlib.pyplot as plt
from terl3_echec.ANALYSE.GameAnalyse import *
import numpy as np
from random import randint
import time

#Tester 10 000 (100 000 idéalement) partie aléatoire vs aléatoire . On part de l'echiquier de départ à chaque fois

#Avec matplotlib :
    #Graph camembert des endgames (Nul par 50 coups / Nul par pat / Echec et mat)
    #Graph camembert des endgames Nul / Echec et mat
    #Graph baton de win blanc / noir / match null
    #Nombre de coup moyen par partie
    #Nombre de coup moyen avant d'arrivé à la 1,2,3,4,5,10,15 ème capture de piece
    #Nombre de coup moyen avant la premiere promotion de pion

#Faire les mêmes tests sur un echiquier en fin de partie avec peu de pièce

print("Quel type d'aléatoire voulez-vous utilisé")
print("0 - Chaque pièce à une probabilité aléatoire d'etre choisit ,puis on choisit aléatoirement un coup parmis un de ses coups")
print("1 - On choisit un coup aléatoire parmis tout les coups aléatoire possible")
choix_aleatoire=int(input())

print("Sur combien d'échiquier aléatoire voulez vous faire les tests ?")
choix_nbr_echiquier=int(input())
print("Quel couleur êtes vous ? 0 : blanc, 1 : noir ?")
choix_coul=int(input())

start = time.time()

if choix_aleatoire==0:
    i = 0
    endgame = [0, 0, 0, 0]  # nul 50 coup ;nul par pat; win blanc ; win noir
    nbr_coup_par_partie = []
    nbr_coup_avant_capture = [[] for i in range(15)]
    nbr_coup_avant_capture_quand_blanc_gagnes = [[] for i in range(15)]
    nbr_coup_avant_capture_quand_noir_gagnes = [[] for i in range(15)]
    nbr_coup_avant_capture_quand_ya_nul = [[] for i in range(15)]
    nbr_no_capture = [0 for i in range(15)]  # on compte le nombre de fois où on a pas atteind la Xeme capture
    nbr_coup_avant_ppp = []
    taille_sans_zero = 0
    while i < choix_nbr_echiquier:
        ech = Echiquier()
        game = GameAnalyse(ech, choix_coul)  # mettre la couleur en param
        # tableau_des_choix = []
        res = -2
        while 1:

            quel_piece = [] #index du tab coup_legaux
            quel_deplacement=[[] for i in range(64)]
            cpt_p=0
            la_piece_a_t_elle_deja_ete_choisie=[]
            for k in game.coup_legaux[0]:
                if k[-2] not in la_piece_a_t_elle_deja_ete_choisie:
                    quel_piece.append(cpt_p)
                quel_deplacement[k[-2]].append(cpt_p)
                la_piece_a_t_elle_deja_ete_choisie.append(k[-2])
                cpt_p+=1

            for k in game.coup_legaux[1]:
                if k[-2] not in la_piece_a_t_elle_deja_ete_choisie:
                    quel_piece.append(cpt_p)
                quel_deplacement[k[-2]].append(cpt_p)
                la_piece_a_t_elle_deja_ete_choisie.append(k[-2])
                cpt_p+=1

            nbr_coup = len(game.coup_legaux[0])
            nbr_coup_promo = len(game.coup_legaux[1])
            nbr_coup_total = nbr_coup + nbr_coup_promo

            if nbr_coup_total == 0:
                le_coup_a_joue = 0
            else:
                if len(quel_piece)==1:
                    quel_piece_jouer=quel_piece[0]
                else:
                    quel_piece_jouer=randint(0,len(quel_piece)-1)
                    quel_piece_jouer=quel_piece[quel_piece_jouer]

                if quel_piece_jouer < nbr_coup:
                    if len(quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]]) == 1:
                        le_coup_a_joue = quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]][0] # 4 indices de tableau ! ca devient compliqué à suivre la
                    else:
                        le_coup_a_joue = randint(0,len(quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]]) - 1)
                        le_coup_a_joue =  quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]][le_coup_a_joue]

                else:

                    if len(quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]]) == 1:
                        le_coup_a_joue = quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]][0] # 4 indices de tableau ! ca devient compliqué à suivre la
                    else:
                        le_coup_a_joue = randint(0,len(quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]]) - 1)
                        le_coup_a_joue =  quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]][le_coup_a_joue]

            # tableau_des_choix.append(le_coup_a_joue)
            res = game.jouer(le_coup_a_joue)
            if res == 0 or res == -1 or res == 1 or res == 2:
                break

        if res == 2:  # nul par 50 coups
            endgame[0] += 1
        elif res == 0:  # nul par pat
            endgame[1] += 1
        elif res == 1:  # win blanc
            endgame[2] += 1
        elif res == -1:  # win noir
            endgame[3] += 1

        nbr_coup_par_partie.append(game.nbr_coup_joue // 2)

        for j in range(15):
            if game.quand_arrive_lattaque[j] != 1:
                nbr_coup_avant_capture[j].append(game.quand_arrive_lattaque[j])
            else:
                nbr_no_capture[j] += 1

            if res == 1:  # victoire blanc
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_blanc_gagnes[j].append(game.quand_arrive_lattaque[j])

            elif res == -1:  # victoire noir
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_noir_gagnes[j].append(game.quand_arrive_lattaque[j])
            elif res == 2:  # nul 50 cops
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_ya_nul[j].append(game.quand_arrive_lattaque[j])

        if game.premiere_promo_pion != 0:
            nbr_coup_avant_ppp.append(game.premiere_promo_pion)
        else:
            taille_sans_zero += 1
        i += 1

else:
    i = 0
    endgame=[0,0,0,0]#nul 50 coup ;nul par pat; win blanc ; win noir
    nbr_coup_par_partie=[]
    nbr_coup_avant_capture=[ [] for i in range(15)]
    nbr_coup_avant_capture_quand_blanc_gagnes=[ [] for i in range(15)]
    nbr_coup_avant_capture_quand_noir_gagnes = [[] for i in range(15)]
    nbr_coup_avant_capture_quand_ya_nul = [[] for i in range(15)]
    nbr_no_capture = [0 for i in range(15)] #on compte le nombre de fois où on a pas atteind la Xeme capture
    nbr_coup_avant_ppp=[]
    taille_sans_zero=0
    while i < choix_nbr_echiquier:
        ech = Echiquier()
        game = GameAnalyse(ech,choix_coul) #mettre la couleur en param
        #tableau_des_choix = []
        res=-2
        while 1:
            nbr_coup = len(game.coup_legaux[0])
            nbr_coup_promo = len(game.coup_legaux[1])
            nbr_coup_total = nbr_coup + nbr_coup_promo

            if nbr_coup_total == 0:
                le_coup_a_joue = 0
            else:
                le_coup_a_joue = randint(0, nbr_coup_total - 1)
            #debug
            #tableau_des_choix.append(le_coup_a_joue)
            res = game.jouer(le_coup_a_joue)
            if res == 0 or res == -1 or res == 1 or res == 2:
                break

        if res == 2:#nul par 50 coups
            endgame[0]+=1
        elif res == 0: #nul par pat
            endgame[1] += 1
        elif res == 1:#win blanc
            endgame[2] += 1
        elif res == -1:#win noir
            endgame[3] += 1

        nbr_coup_par_partie.append(game.nbr_coup_joue//2)

        #debug :
        #if game.nbr_coup_joue//2 <5:
        #    print(tableau_des_choix)

        for j in range(15):
            if game.quand_arrive_lattaque[j]!=1:
                nbr_coup_avant_capture[j].append(game.quand_arrive_lattaque[j])
            else:
                nbr_no_capture[j]+=1

            if res==1:#victoire blanc
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_blanc_gagnes[j].append(game.quand_arrive_lattaque[j])

            elif res==-1:#victoire noir
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_noir_gagnes[j].append(game.quand_arrive_lattaque[j])
            elif res==2:#nul 50 cops
                if game.quand_arrive_lattaque[j] != 1:
                    nbr_coup_avant_capture_quand_ya_nul[j].append(game.quand_arrive_lattaque[j])



        if game.premiere_promo_pion!=0:
            nbr_coup_avant_ppp.append(game.premiere_promo_pion)
        else:
            taille_sans_zero+=1
        i += 1


# pour le Diagramme à barres
def addlabels(x, y):
    for e in range(len(x)):
        plt.text(e, y[e], y[e])

# DIAGRAMME CIRCULAIRE des endgames :
noms_2 = ["Nul par 50 coups", "Nul par pat", "Echec et mat"]
nul50_pat_echec_m_2 = [endgame[0],endgame[1],endgame[2]+endgame[3]]

fig, (ax2,ax3)  =  plt.subplots(1,2,figsize=(12,6))

def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct * total / 100.0))
        # si on voulait affiché les pats / regles des 50 coups dans un graph camembert de taille 2 et non de taille 3
        # (au final c'est une mauvaise idée je l'ai pas implémenter mais ca marche)
        #if values == nul50_pat_echec_m_2 and  val == endgame[0]+endgame[1]:
        #    return '{p:.2f}% ({b:d})\n[{v:d} CC ,{h:d} Pat]'.format(p=pct, b=val,v=endgame[0],h=endgame[1])
        return '{p:.2f}%  ({v:d})'.format(p=pct, v=val)

    return my_autopct


ax2.pie(nul50_pat_echec_m_2, labels=noms_2,shadow=True,autopct=make_autopct(nul50_pat_echec_m_2),colors=['#2B9953','#3AB767','#86EF83'])#radius=0.7
ax2.set_title('Répartition des fins de parties sur les ' + str(choix_nbr_echiquier) + ' échiquiers')

plt.subplots_adjust(wspace=0.4)

wblanc_wnoir_null = [endgame[2], endgame[3], endgame[0] + endgame[1]]
nom_histo = ['Win Blanc', 'Win Noir', 'Match Nul']
ajust_text=ax3.bar(nom_histo, wblanc_wnoir_null,width=0.5,align='center',color=['#6CC8B0',(0.3,0.7,0.6,0.9),(0.3,0.6,0.5,0.9)])
ax3.set_title('Répartition des victoires et matchs nuls sur les ' + str(choix_nbr_echiquier) + ' échiquiers')
ax3.set_ylabel("Nombre de parties")

for p in ajust_text:
    height = p.get_height()
    ax3.text(x=p.get_x() + p.get_width() / 2, y=p.get_y() + height * 1.01,
             s="{}".format(height),
             ha='center')

# Diagramme à barres nbr coups moyen par partie / nbr coups moyen avant ppp :

nbr_coup_par_partie = np.array(nbr_coup_par_partie)
nbr_coup_avant_ppp = np.array(nbr_coup_avant_ppp)

taille_avec_zero=len(nbr_coup_avant_ppp)

nbr_coup = [round(np.mean(nbr_coup_par_partie)), round(np.mean(nbr_coup_avant_ppp)), round(np.median(nbr_coup_par_partie)),
            round(np.median(nbr_coup_avant_ppp))]

labels = ['Avant de terminer une partie', 'Avant la première promotion de pion']

moyenne = [nbr_coup[0], nbr_coup[1]]  # premier index : nbr de coup dans une partie , deuxieme index : nbr de coup dans une partie avant ppp
median = [nbr_coup[2], nbr_coup[3]]
min = [np.amin(nbr_coup_par_partie), np.amin(nbr_coup_avant_ppp)]
max = [np.amax(nbr_coup_par_partie), np.amax(nbr_coup_avant_ppp)]

x = np.arange(len(labels))
width = 0.15

fig, (ax ,ax_p)= plt.subplots(1,2,figsize=(13,5))
rects1 = ax.bar(x + 0.00, moyenne, width,color=(0.3,0.5,0.4,0.9) ,label='Moyen')
rects2 = ax.bar(x +0.15, median, width,color=(0.3,0.6,0.5,0.9), label='Médian')
rects3 = ax.bar(x + 0.30, min, width,color=(0.3,0.7,0.6,0.9), label='Minimum')
rects4 = ax.bar(x + 0.45, max, width,color=(0.3,0.8,0.7,0.9), label='Maximum')

# camembert : nombre partie avec promo ,nombre partie sans promo
avec_sans_ppp=[taille_avec_zero,taille_sans_zero]
ax_p.pie(avec_sans_ppp, labels=["Avec promotion", "Sans promotion"], shadow=True, autopct='%1.1f%%', colors=['#2B9953', '#86EF83'])
ax_p.set_title("Pourcentage des parties ayant eu au moins une promotion de pion")

x1, x2, y1, y2 = ax.axis()
ax.axis((x1, x2, y1, y2 + 25))#on augmente un peu en haut


ax.set_ylabel('Nombre de coups')
ax.set_title("Nombre de coups nécessaire pour terminer une partie (gauche)\n et promouvoir notre premier pion (droite). Testé sur " +
             str(choix_nbr_echiquier) + "\néchiquiers (à gauche) et "+str(taille_avec_zero)+" échiquiers (à droite)")
ax.set_xticks([r + 0.15 for r in range(2)], ['Avant de terminer une partie', 'Avant la première promotion de pion'])
ax.legend()

def autolabel(rects):#met un label au dessus de chaque graph
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)

fig.tight_layout()

#Pr ttes partie (exclue les 1) ,lorsque ya win blanc ,lorsque ya win noir ,lorsque ya pat, lorsque ya nul 50 coup

nbr_coup_avant_capture = np.array(nbr_coup_avant_capture,dtype=object)
nbr_coup_avant_capture_quand_blanc_gagnes=np.array(nbr_coup_avant_capture_quand_blanc_gagnes,dtype=object)
nbr_coup_avant_capture_quand_noir_gagnes=np.array(nbr_coup_avant_capture_quand_noir_gagnes,dtype=object)
nbr_coup_avant_capture_quand_ya_nul=np.array(nbr_coup_avant_capture_quand_ya_nul,dtype=object)
les_x=[i for i in range(1,16)]

les_moyennes=[]
les_moyennes_b=[]
les_moyennes_n=[]
les_moyennes_nul=[]
for i in range(15):
    if len(nbr_coup_avant_capture[i])>0:
        les_moyennes.append(round(np.mean(nbr_coup_avant_capture[i])))

    if len(nbr_coup_avant_capture_quand_blanc_gagnes[i])>0:
        les_moyennes_b.append(round(np.mean(nbr_coup_avant_capture_quand_blanc_gagnes[i])))


    if len(nbr_coup_avant_capture_quand_noir_gagnes[i])>0:
        les_moyennes_n.append(round(np.mean(nbr_coup_avant_capture_quand_noir_gagnes[i])))


    if len(nbr_coup_avant_capture_quand_ya_nul[i])>0:
        les_moyennes_nul.append(round(np.mean(nbr_coup_avant_capture_quand_ya_nul[i])))



fig, (ax4, ax5) = plt.subplots(1, 2, figsize=(18, 6),gridspec_kw={'width_ratios': [2, 1]})

ax4.plot(np.arange(1,len(les_moyennes)+1),les_moyennes, label="Pour toutes les parties",marker='o')
ax4.plot(np.arange(1,len(les_moyennes_b)+1), les_moyennes_b, label="Lorsque les blancs gagnent", marker='o')
ax4.plot(np.arange(1,len(les_moyennes_n)+1), les_moyennes_n, label="Lorsque les noirs gagnent", marker='o')
ax4.plot(np.arange(1,len(les_moyennes_nul)+1), les_moyennes_nul, label="Lorsqu'il y a nul par 50 coups", marker='o')
ax4.set_title('Nombre de coups moyens avant d\'arriver à la n-ème captures de pièces.\nTesté sur ' + str(choix_nbr_echiquier) + ' échiquiers')
ax4.set_ylabel("Nombre de coups moyens")
ax4.set_xlabel("Capture n°")
ax4.legend()

tab_nbr_ech=[choix_nbr_echiquier for i in range(15)]

tab_nbr_ech=[tab_nbr_ech[i]-nbr_no_capture[i] for i in range(15)]
tab_noms=[str(i) for i in range(1,16)]


transfo_pourcentage=ax5.bar(tab_noms, tab_nbr_ech,width=0.5,align='center',color=(0.3,0.6,0.5,0.9))

for p in transfo_pourcentage:
    height = p.get_height()
    en_pourcent=int((height/choix_nbr_echiquier) * 100)
    ax5.text(x=p.get_x() + p.get_width() / 2, y=p.get_y() + height *1.02,
            s="{}%".format(en_pourcent),
            ha='center',
            fontsize='x-small')




ax5.set_ylabel('Nombre de parties')
ax5.set_xlabel('Capture n°')
ax5.set_title("Nombre de parties allant jusqu'à la n-ème capture")

end = time.time()

min=(end - start) // 60
if min !=0:
    sc=(end - start)-(min*60)
else:
    sc=end - start


print("Temps mis pour générer et analyser les "+str(choix_nbr_echiquier)+" échiquiers : ",min, " mins ",sc," secs")
plt.show()

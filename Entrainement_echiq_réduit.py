from terl3_echec.MonteCarlo import *
import time

class Entrainement:

    def __init__(self, RN):
        self.RN=RN


    def generer_episode(self):

        #on prépare les tableaux de nos training datas
        position_echiquier=[]
        prior_ameliore=[]
        endgame=[]

        #Chaque partie commencera avec l'echiquier de départ
        ech_depart=Echiquier(551366426624 ,8 ,1048576 ,32 ,2048 ,16 ,0 ,2377900603251621888 ,274894684160 ,18018796555993088 ,288230376151711744 ,1125899906842624,1,0,0)
        game=Game(ech_depart)


        favorise_alea_debut=0
        while game.verif_cas_terminaux() == 3: # Tant que la partie n'est pas finit

            #on met la position d'echiquier dans le tableau
            position_echiquier.append(game.echiquier.echiq_to_input())

            # on initialise les resultats de fin de partie (biensur si c'est le noirs qui gagne à la fin on swapera 1 et -1 ,si ya match nul on mettra à 0)
            if game.echiquier.ech[12] & 1: # si c'est aux blancs de jouer
                endgame.append(1)
            else:
                endgame.append(-1)

            # On lance notre MCTS
            branche_pere_RACINE = Branche(None, None, None)
            branche_pere_RACINE.N = 1

            racine = Noeud(game, branche_pere_RACINE)
            #start = time.time()
            if favorise_alea_debut<10:
                MCT = MonteCarlo(racine, self.RN,0,1.0)
            else:
                MCT = MonteCarlo(racine, self.RN)
            prior_amel = MCT.demarrer_la_recherche()

            # Rappel : le prior renvoyé par notre RN est un tableau de taille FIXE de 1880 cases (il possède tout les coup légaux "ascendant" possible)
            # La taille du prior amélioré renvoyé par notre MCT est bien plus petite ,elle correspond tout juste au nombre de coup legaux disponible
            # Donc ce qu'on va faire c'est qu'on va transformé ce tableau en tableau de 1880 ,où les coups non legaux vaudront 0

            #on met le prior ameliorer dans le tableau
            prior_amel_1880=[0.0 for i in range(1880)]

            for coup, prob,_ in prior_amel:
                index_coup = algebrique_to_index_PRIOR[coup] #on cherche l'index du coup grâce à notre dictionnaire
                prior_amel_1880[index_coup] = prob

            prior_ameliore.append(prior_amel_1880)# on

            # on selectionne le coup de facon aleatoire en suivant une loi de Bernoulli (= loi binomial où n = 1) mais qui s'applique aux 1880 cases du tableau
            # (ca sappel une loi multinomial)
            # Exemple : on a le tableau [0.5,0.2,0.1] , la loi multinomial nous renverra l'index 0 ds 50% des cas ,l'index 1 ds 20% des cas et l'index 2 ds 10% des cas
            tableau_de_1880_cases_avec_un_seul_des_indices_choisit_aleatoirement = np.random.multinomial(1, prior_amel_1880)
            # where est utilisé pour trouvé l'index ,on rajoute [0][0] car where renvoit un tableau de couple [index,type de l'index]
            index_coup_a_jouer = np.where(tableau_de_1880_cases_avec_un_seul_des_indices_choisit_aleatoirement == 1)[0][0]

            for coup, prob, index_coup in prior_amel:
                est_ce_le_bonne_index = algebrique_to_index_PRIOR[coup]
                if est_ce_le_bonne_index == index_coup_a_jouer:
                    game.jouer(index_coup) # on joue le coup
                    #end = time.time()
                    #print("hop on a joué ,temps mis ",end-start," sc ,voici l'echiquier : ",to_fen(game.echiquier))
                    break

        game_res = game.verif_cas_terminaux()

        # On change les valeurs si ya match nul ou que les noirs qui ont win
        if game_res == 2 or game_res==0:
            que_des_zero=[0 for i in range(len(endgame))]
            endgame=que_des_zero
            print("Match nul ,fen : ",to_fen(game.echiquier))


        if game_res == -1:
            for i in range(0, len(endgame)):
                endgame[i] = endgame[i] * -1.0
            print("Victoire noire")
        else:
            print("Victoire blanche")

        favorise_alea_debut+=1
        return position_echiquier, prior_ameliore, endgame


from keras.models import Model
from keras.layers import *
import tensorflow as tf

#La convolution est en channel_last par default, or l'output de conv2D est égal au nombre de channel créer (== nombre de filter donnée en parametre) * height * width
# Notre input etant 13*8*8 ,le channel doit etre modifié en première position et non en dernière position ,sinon c'est la width qui change et ca fait plus aucun sens
tf.keras.backend.set_image_data_format('channels_first')


#La fonction va nous permettre de créer un block résiduel : c'est à dire deux couches convolutif successif sauf qu'a la fin on fait une skip connection : on additionne la
#couche d'input avec la couche de sortie
def block_residuel(couche_precedente):
    couche_1 = Conv2D(filters=128,kernel_size=3,padding='same',kernel_initializer=  tf.keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='uniform'))(couche_precedente)
    couche_1 = BatchNormalization()(couche_1)
    couche_1 = ReLU()(couche_1)

    couche_2 = Conv2D(filters=128,kernel_size=3, padding='same',kernel_initializer= tf.keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='uniform'))(couche_1)
    couche_2 = BatchNormalization()(couche_2)
    couche_2 = ReLU()(couche_2)
    out = Add()([couche_precedente,couche_2])
    return out


# On definie la taille de l'input de notre RN ,un tenseur de taille 14 * 8 * 8 qui répresente les 12 echiquiers + la couleur indiquant
# le tour de rôle + aide pour la detection du padding ajouté par la convolution
inputRN = Input(shape=(14,8,8))


#Ajout d'une première couche convolutif (128 filtres ,kernel (3*3) et stride (1*1)) ,suivit d'une batch normalisation et d'une fonction d'activation reLU
#On ajoute padding="same" pour pas que la convolution nous réduise la taille de notre input et on initialise les poids de facon random
couche=Conv2D(filters=128, kernel_size=3,padding="same",input_shape=(14,8,8),kernel_initializer=tf.keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='uniform'))(inputRN)
couche=BatchNormalization()(couche)
couche=ReLU()(couche)

#On créer les 19 blocks résiduels
for nbr_c in range(19):
    couche=block_residuel(couche)


#Couche de convolution pour notre Prior
couche_Prior=Conv2D(filters=32, kernel_size=1,padding="same")(couche) #remplacé filter 2 par 32 cf Leela chess zero
couche_Prior=BatchNormalization()(couche_Prior)
couche_Prior=ReLU()(couche_Prior)

#On transforme le tenseur de taille 2 * 8 * 8 en un vecteur 1D pour pouvoir ensuite le passé dans une couche complètement connecté
flatten_Prior=Flatten()(couche_Prior)

#Enfin on ajoute une couche complètement connecté qui renverra 1880 output ,la fonction d'actication softmax fait en sorte
#d'arrangé les 1880 valeurs en probabilité inclus dans ]0,1[ dont la somme vaut 1
outputPrior = Dense(1880, name='Prior', activation='softmax')(flatten_Prior) #Dense = complètement connecté


#Couche de convolution pour notre ValueNetwork
couche_ValueNetwork=Conv2D(filters=32, kernel_size=1,padding="same")(couche) #remplacé filter 1 par 32 cf Leela chess zero
couche_ValueNetwork=BatchNormalization()(couche_ValueNetwork)
couche_ValueNetwork=ReLU()(couche_ValueNetwork)

#On transforme le tenseur de taille 1 * 8 * 8 en un vecteur 1D
flatten_ValueNetwork=Flatten()(couche_ValueNetwork)

#Une première couche complètement connecté qui renverra 256 output ,fonction d'activation reLU (max entre 0 et la valeur renvoyé par la somme des poids)
outputValueNetwork = Dense(128,activation="relu")(flatten_ValueNetwork)

#Dernière couche complètement connecté ,elle renvoit 1 output car on veut comme resultat un nombre compris entre -1 et 1.
#On utilise tanh pour cela (e**x - e**(-x) ) / (e**x + e**-x)
outputValueNetwork = Dense(1, activation='tanh',name='ValueNetwork')(outputValueNetwork)


#Création de notre modèle
modele = Model(inputRN, [outputPrior,outputValueNetwork])

#Fonction de cross entropy , from_logits aide notre RN en lui indiquant qu'on a fait softmax juste avant donc qu'il ne doit pas le refaire
#https://stackoverflow.com/questions/57253841/from-logits-true-and-from-logits-false-get-different-training-result-for-tf-loss
cross_entropy = tf.keras.losses.CategoricalCrossentropy(from_logits=False)


#Gradient descent pour uptade les poids
# La fonction loss sera la somme de l'erreur quadratique moyenne de notre Value Network et de l'entropie croisé de notre Prior
modele.compile(optimizer = 'SGD', loss={'ValueNetwork' : 'mean_squared_error', 'Prior' : cross_entropy})

modele.save('RN_init.keras')


from terl3_echec.RN_utils import *
from random import randint
from terl3_echec.Game import *
from terl3_echec.TESTING.TEST_ECHIQUIER.bitboard_utils import generer_un_echiquier_realiste_aleatoire


ech=Echiquier()

print(ech.echiq_to_input_2())
#créer un tableau de 1968 cases de proba aleatoire dont la somme vaut 1
def creer_un_tableau_de_1968_case_de_proba_aleatoire():

    tab = [randint(1,100) for i in range(2008)]
    som = sum(tab)
    tab = [i / som for i in tab]

    return tab

ech_alea=generer_un_echiquier_realiste_aleatoire()
print(ech_alea.echiq_to_input())
print(len(ech_alea.echiq_to_input()))
for j in range(10000):
    alea_1968_cases=creer_un_tableau_de_1968_case_de_proba_aleatoire()

    ech_alea=generer_un_echiquier_realiste_aleatoire()

    game=Game(ech_alea)

    #print(game.get_coup_legaux_NA())
    #print(len(game.get_coup_legaux_NA()))

    resultat=outputPrior_to_coup_legaux(alea_1968_cases,game)
    #print(resultat)
    #print(len(resultat))

    bl="True"
    for i in resultat:
        if alea_1968_cases[algebrique_to_index_PRIOR[i[0]]]!=i[1]:
            bl="False"
            print("Erreur")


    #print("Les resultats sont ils identiques à ceux du tableau ? : ",bl)

    #print(sorted(resultat, key=lambda x: x[1]))

    #print(ech_alea.echiq_to_input())
from keras.models import Model
from keras.layers import *
import tensorflow as tf

#tf.keras.backend.set_image_data_format('channels_first')
#inputRN = Input((13,))
#inputRN = Input(shape=(13,64))

#inputRN= Input((832,))

inputRN = Input(shape=(13,64))

#couche1 = Dense(128, activation='relu')(inputRN)

couche1 = Dense(256, activation='relu')(inputRN)
print("couche1 shape",couche1.shape)
print(couche1)
flatten=Flatten()(couche1)
print("flatten shape",flatten.shape)
outputPrior = Dense(1968, name='Prior', activation='softmax')(flatten)
print("outputPrior shape",outputPrior.shape)

#cross entropy binaire
cross_entr_bin = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
model = Model(inputRN, outputPrior)
model.compile(optimizer = 'SGD', loss={'Prior' : cross_entr_bin})

model.save('test_model.keras')

'''
couche1 shape (None, 13, 256)
flatten shape (None, 3328)
outputPrior shape (None, 1968)
KerasTensor(type_spec=TensorSpec(shape=(None, 13, 256), dtype=tf.float32, name=None), name='dense/Relu:0', description="created by layer 'dense'")
'''
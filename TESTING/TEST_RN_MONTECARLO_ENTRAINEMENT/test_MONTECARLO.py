from terl3_echec.MonteCarlo import *
import sys
np.set_printoptions(threshold=sys.maxsize) #permet de print les numpys array completement dans le terminal

piont_blanc=(1<<39) | (1<<30) | (1<<29)
tour_blanc=(1 << 3)
fou_blanc=(1 << 5)
cavalier_blanc=(1 << 20)
dame_blanc=(1 << 11)
roi_blanc=(1 << 4)

piont_noir=0
tour_noir=(1 << 56) | (1 << 61)
fou_noir=(1 << 54) | (1 << 42)
cavalier_noir=(1 << 24) | (1 << 38)
dame_noir=(1 << 58)
roi_noir=(1 << 50)


ech=Echiquier(piont_blanc,tour_blanc,cavalier_blanc,fou_blanc,dame_blanc,roi_blanc,piont_noir,tour_noir,cavalier_noir,fou_noir,dame_noir,roi_noir,1,0,0)
ech2=Echiquier(551366426624 ,8 ,1048576 ,32 ,2048 ,16 ,0 ,2377900603251621888 ,274894684160 ,18018796555993088 ,288230376151711744 ,1125899906842624,1,0,0)

print(to_fen(ech))
print("et lautre",to_fen(ech2))
'''
a=[0 for i in range(1880)]

tab=[]
for e in range(20):
    tab.append(random.uniform(0, 1))

som=0
for e in tab:
    som+=e

for e in range(len(tab)):
    tab[e]=tab[e]/som

cpt=0
for e in range(200,240,2):
    a[e]=tab[cpt]
    cpt+=1

rand_idx = np.random.multinomial(1, a)
print(rand_idx)
idx = np.where(rand_idx==1)

print("ind",idx)
'''

np.set_printoptions(precision=3, suppress=True)

model = keras.models.load_model("../../RN/RN_128x20_echiq_depart_apres7_iterations_on_met_moins_un_nul.keras")
#TESTING/TEST_RN_MONTECARLO_ENTRAINEMENT/test_model_14-8-8.keras

# On créer une instance de la classe Game qui démarre avec l'échiquier de départ
ech_depart = Echiquier()
'''
ech_depart.ech[6]|=(1<<14)
ech_depart.ech[0]= ech_depart.ech[0] & ~ (1<<14)
ech_depart.ech[1]= ech_depart.ech[1] & ~ (1<<7)
ech_depart.ech[2]= ech_depart.ech[2] & ~ (1<<6)
ech_depart.ech[3]= ech_depart.ech[3] & ~ (1<<5)
ech_depart.ech_position = [ech_depart.get_position(ech_depart.ech[i]) for i in range(12)]
print("to fen",to_fen(ech_depart))
'''
game = Game(ech_depart,0)

# On créer la branche père de la racine
# On initialise le N de cette branche à 1 pour eviter que lors de la première iteration tous les UCT soit égaux à 0 (et donc qu'on
# tire aléatoirement notre coup). Avec 1 on commence directement par le meilleur coup du prior, c'est plus optimisé.
branche_pere_RACINE = Branche(None, None,None)
branche_pere_RACINE.N = 1

# On créer la racine
racine = Noeud(game, branche_pere_RACINE)

# On lance la recherche de notre arbre Monte Carlo, il renvoit un tableau de triplet : [nom du coup ,sa probabilité,index du coup]
MCT = MonteCarlo(racine,model,0,4.0,4.0)
probs = MCT.demarrer_la_recherche()

print(np.array(probs))

'''
0 1
8 2
16 3 
24 4
32 5
40 6 
48 7
56 8
'''
print("alors",0%2!=0)
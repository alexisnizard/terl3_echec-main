import keras
import numpy as np
from terl3_echec.Game import *
from terl3_echec.TESTING.TEST_ECHIQUIER.bitboard_utils import generer_un_echiquier_realiste_aleatoire
from terl3_echec.RN_utils import *

from keras.models import Model
from keras.layers import *
import tensorflow as tf
from random import randint

import matplotlib.pyplot as plt

import sys
np.set_printoptions(threshold=sys.maxsize) #permet de print les numpys array completement dans le terminal

while 1:
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Test echiq_to_13_input (13 inputs)")
    print("2 - Test echiq_to_input (13 * 64 inputs)")
    print("3 - Test echiq_to_input_1D (832 inputs)")
    print("4 - Test echiq_to_input_1D (14*8*8 inputs)")
    print("5 - Test echiq_to_input_1D (13*8*8 inputs et 1968 sorties)")
    print("6 - Test echiq_to_input_1D (14*8*8 inputs et 1880 sorties)")
    print("7 - test value network sur des echiq randoms")
    print("0 - Exit")
    choix = int(input())

    if choix == 0:
        break
    elif choix == 1:
        ech = Echiquier()
        inp_ech = ech.echiq_to_13_input()
        print(inp_ech)
        print(len(inp_ech))

    elif choix==2:
        ech = Echiquier()
        inp_ech = ech.echiq_to_input()


        def afficher_tab(tab):
            ligne = []
            for i in range(len(tab)):
                ligne.append(tab[i])
                if len(ligne) == 8:
                    print(ligne)
                    ligne = []


        for j in inp_ech:
            afficher_tab(j)
            print('\n')

        print(inp_ech)
        print(len(inp_ech))

        ech = Echiquier()
        inp_ech = ech.echiq_to_input()
        # inp_ech=generer_un_echiquier_realiste_aleatoire(16).echiq_to_12_input()
        test = [i * 100000 for i in range(12)]

        model = keras.models.load_model("test_model.keras")
        q = model.predict(np.array([inp_ech]))

        print(q)




    elif choix==3:
        ech = Echiquier()
        inp_ech = ech.echiq_to_input_1D()


        def afficher_tab(tab):
            ligne = []
            for i in range(len(tab)):
                ligne.append(tab[i])
                if len(ligne) == 8:
                    print(ligne)
                    ligne = []
                if i % 64 == 0 and i != 0:
                    print('\n')


        afficher_tab(inp_ech)

        print(inp_ech)
        print(len(inp_ech))
    elif choix==4:
        ech = Echiquier()
        inp_ech = ech.echiq_to_input_2()
        print(inp_ech)
        model = keras.models.load_model("test_model_2.keras")
        q = model.predict(np.array([inp_ech]))
        print(q)
        print(len(q[0][0]))

        som=0
        for j in q[0][0]:
            som+=j

        print("il faut que ca soit à peu pret egal a 1 : ",som)

    elif choix == 5:
        ech = Echiquier()
        inp_ech = ech.echiq_to_input_2()
        print(inp_ech)
        model = keras.models.load_model("test_model_2.keras")
        q = model.predict(np.array([inp_ech]))
        print(q)
        print(len(q[0][0]))

        som = 0
        for j in q[0][0]:
            som += j

        print("il faut que ca soit à peu pret egal a 1 : ", som)

    elif choix==6:
        model = keras.models.load_model("RN_128x20_echiq_depart_apres1_iterations.keras")

        for i in range(10):
            ech = generer_un_echiquier_realiste_aleatoire(32)

            inp_ech = ech.echiq_to_input()
            print(inp_ech)
            q = model.predict(np.array([inp_ech]))
            #q[0][0] = np.around(q[0][0], decimals=7)
            print(q)
            print(len(q[0][0]))


            som=0
            for j in q[0][0]:
                som+=j

            print("il faut que ca soit à peu pret egal a 1 : ",som)

            game=Game(ech)
            a=outputPrior_to_coup_legaux(q[0][0],game)
            print("coups legaux :",a)
            som=0
            for j in a:
                som+=j[1]

            print("somme doit etre egal a 1",som)
            break
    elif choix == 7:
        model = keras.models.load_model("../../RN/RN_128x20_echiq_depart_apres13_iterations_avec_len.keras")
        les_val_net=[]
        for i in range(100):
            r=randint(2,32)
            ech = generer_un_echiquier_realiste_aleatoire(r)
            r = randint(0,1)
            if r:
                r = randint(2, 32)
                ech_2 = generer_un_echiquier_realiste_aleatoire(r)

                for k in range(12):
                    ech.ech[k]= ech.ech[k] | ech_2.ech[k]
            r = randint(0, 1)
            if ech.ech[12]%2==0:
                ech.ech[12]+=r
            else:
                ech.ech[12]-= r



            inp_ech = ech.echiq_to_input()
            q = model.predict(np.array([inp_ech]))
            les_val_net.append(q[1][0][0])

        print(les_val_net)
        plt.hist(les_val_net,range = (-1,1),bins=100)
        plt.show()
        break


'''
ech=Echiquier()
inp_ech=ech.echiq_to_input()


def afficher_tab(tab):
    ligne=[]
    for i in range(len(tab)):
        ligne.append(tab[i])
        if len(ligne)==8:
            print(ligne)
            ligne=[]
        if i%64==0 and i!=0:
            print('\n')


afficher_tab(inp_ech)


print(inp_ech)
print(len(inp_ech))
'''

'''
ech=Echiquier()
inp_ech=ech.echiq_to_input_13_tab()

def afficher_tab(tab):
    ligne=[]
    for i in range(len(tab)):
        ligne.append(tab[i])
        if len(ligne)==8:
            print(ligne)
            ligne=[]

for j in inp_ech:
    afficher_tab(j)
    print('\n')

print(inp_ech)
print(len(inp_ech))

ech=Echiquier()
inp_ech=ech.echiq_to_13_input()
print(len(inp_ech))
'''

'''

ech=Echiquier()
inp_ech=ech.echiq_to_input()
#inp_ech=generer_un_echiquier_realiste_aleatoire(16).echiq_to_12_input()
neural_network_chess-1.4=[i*100000 for i in range(12)]

model = keras.models.load_model("test_model.keras")
q = model.predict(np.array( [inp_ech] ))

print(q)

print("taille de q",len(q[0]))
som=0
for i in q[0]:
    som+=i

print(som)

'''
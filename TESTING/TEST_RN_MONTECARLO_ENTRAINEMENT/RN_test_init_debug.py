from keras.models import Model
from keras.layers import *
import tensorflow as tf



tf.keras.backend.set_image_data_format('channels_first')
print(tf.__version__)

def block_residuel(couche_precedente):
    couche_1 = Conv2D(filters=256,kernel_size=3, padding='same')(couche_precedente)
    couche_1 = BatchNormalization()(couche_1)
    couche_1 = ReLU()(couche_1)

    couche_2 = Conv2D(filters=256,kernel_size=3, padding='same')(couche_1)
    couche_2 = BatchNormalization()(couche_2)
    couche_2 = ReLU()(couche_2)
    out = Add()([couche_precedente,couche_2])
    return out


inputRN = Input(shape=(13,8,8))


print("inputRN shape",inputRN.shape)

couche1 = Conv2D(filters=256, kernel_size=3,padding="same",activation='relu',data_format="channels_first",input_shape=(13,8,8))(inputRN)
print(couche1)

print("couche1 shape",couche1.shape)
flatten=Flatten()(couche1)
print("flatten shape",flatten.shape)
outputPrior = Dense(1968, name='Prior', activation='softmax')(flatten)
print("outputPrior shape",outputPrior.shape)

#cross entropy binaire
cross_entr_bin = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
model = Model(inputRN, outputPrior)
model.compile(optimizer = 'SGD', loss={'Prior' : cross_entr_bin})

model.save('test_model_3_debug.keras')

'''

inputRN shape (None, 13, 8, 8)
couche1 shape (None, 256, 8, 8)
flatten shape (None, 16384)
outputPrior shape (None, 1968)
KerasTensor(type_spec=TensorSpec(shape=(None, 256, 8, 8), dtype=tf.float32, name=None), name='conv2d/BiasAdd:0', description="created by layer 'conv2d'")

'''
from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import *

while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Test relier fou et roi ")
    print("2 - Test relier tour et roi ")
    print("3 - Test de get_pieces_immobilisees_diagonalement() avec un echiquier aléatoire")
    print("4 - Test de get_pieces_immobilisees_diagonalement() avec un echiquier aléatoire de 16 pieces et les noirs attaquent les blancs")
    print("5 - Test de get_pieces_immobilisees_diagonalement() (noir attaquent blanc) avec uniquement des echiquiers aléatoire interessant")
    print("6 - Test de get_pieces_immobilisees_diagonalement() (blanc attaquent noir) avec uniquement des echiquiers aléatoire interessant")
    print("7 - Test de get_pieces_immobilisees_vert_horiz() (noir attaquent blanc) avec uniquement des echiquiers aléatoire interessant")
    print("8 - Test de get_pieces_immobilisees_diagonalement() (noir attaquent blanc) avec echiquier personnalisé")
    print("9 - Test de get_pieces_immobilisees_vert_horiz() (noir attaquent blanc) avec echiquier personnalisé")
    print("0 - Exit")
    choix=int(input())

    masques = 0

    if choix==0:
        break
    elif choix==1:
        ech=Echiquier()
        print("Entrez la position du fou")
        choix_fou=int(input())
        print("Entrez la position du roi (cohérente avec celle du fou)")
        choix_roi = int(input())
        masque=ech.relier_roi_et_fou(choix_roi,choix_fou)
        afficher_bitboard_dans_console_couleur_et_numero(masque[0])
    elif choix==2:
        ech = Echiquier()
        print("Entrez la position de la tour")
        choix_tour = int(input())
        print("Entrez la position du roi (cohérente avec celle de la tour)")
        choix_roi = int(input())
        masque = ech.relier_roi_et_tour(choix_roi, choix_tour)
        afficher_bitboard_dans_console_couleur_et_numero(masque[0])
    elif choix == 3:
        print("Combien de pièce voulez-vous dans l'echiquier (entre 2 et 32) ? -1 pour un nombre aléatoire")
        choix_nbr_piece = int(input())
        if choix_nbr_piece != -1 and (
                choix_nbr_piece < 2 or choix_nbr_piece > 32):  # on s'assure que le nbr de pièce présent dans l'echiquier soit réaliste
            print(
                "Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32\n")
            continue

        print("Qui est l'allie? 0 : blanc, 1 :  noir")
        choix_attaquant = int(input())
        ech=generer_un_echiquier_realiste_aleatoire(choix_nbr_piece)
        afficher_masque_attaque_dans_console(ech, choix_attaquant)

        masque=0
        if choix_attaquant==0:
            a, b, c,d = ech.get_pieces_immobilisees_diagonalement(0)
            for e in a:
                print(e)
                masque=masque | (1 << e)
        else:
            for e in ech.get_pieces_immobilisees_diagonalement(0):
                print(e)
                masque = masque | (1 << e)

        afficher_bitboard_dans_console_couleur(masque)
    elif choix == 4:
        ech = generer_un_echiquier_realiste_aleatoire(16)
        afficher_masque_attaque_dans_console(ech, 0)

        masque = 0

        a, b, c,d = ech.get_pieces_immobilisees_diagonalement(0)
        for e in a:
            print(e)
            masque = masque | (1 << e)

        afficher_bitboard_dans_console_couleur(masque)

    elif choix == 5:

        L=[]

        for j in range(1000):
            ech = generer_un_echiquier_realiste_aleatoire(16)

            masque = 0

            a,b,c,d=ech.get_pieces_immobilisees_diagonalement(0)
            for e in a:
                masque = masque | (1 << e)

            if masque !=0:
                L.append(ech)

        for echiq in L:
            print("Inserer une lettre pour passer à l'echiquier suivant : ")
            afficher_masque_attaque_dans_console(echiq, 0)

            masque = 0
            a, b, c,d= echiq.get_pieces_immobilisees_diagonalement(0)
            for e in a:
                print(e)
                masque = masque | (1 << e)
            afficher_bitboard_dans_console_couleur(masque)

            suiv=input()

    elif choix == 6:

        L=[]

        for j in range(1000):
            ech = generer_un_echiquier_realiste_aleatoire(16)

            masque = 0
            a, b, c,d = ech.get_pieces_immobilisees_diagonalement(1)
            for e in a:
                masque = masque | (1 << e)

            if masque !=0:
                L.append(ech)

        for echiq in L:
            print("Inserer une lettre pour passer à l'echiquier suivant : ")
            afficher_masque_attaque_dans_console(echiq, 1)

            masque = 0
            a, b, c,d = ech.get_pieces_immobilisees_diagonalement(1)
            for e in a:
                print(e)
                masque = masque | (1 << e)
            afficher_bitboard_dans_console_couleur(masque)

            suiv=input()
    elif choix == 7:

        L=[]

        for j in range(1000):
            ech = generer_un_echiquier_realiste_aleatoire(16)

            masque = 0
            a, b, c,d = ech.get_pieces_immobilisees_vert_horiz(0)
            for e in a:
                masque = masque | (1 << e)

            if masque !=0:
                L.append(ech)

        for echiq in L:
            print("Inserer une lettre pour passer à l'echiquier suivant : ")
            afficher_masque_attaque_dans_console(echiq, 0)

            masque = 0
            a, b, c,d = ech.get_pieces_immobilisees_vert_horiz(0)
            for e in a:
                print(e)
                masque = masque | (1 << e)
            afficher_bitboard_dans_console_couleur(masque)

            suiv=input()
    elif choix == 8:

        print("Placer le roi")
        ech_roi = generer_un_echiquier_personnalise()
        print("Placer l'echiquier des fous enemies")
        ech_fou = generer_un_echiquier_personnalise()
        print("Placer des pions qui bloque la trajectoire")
        ech_pions = generer_un_echiquier_personnalise()

        ech=Echiquier(ech_pions,0,0,0,0,ech_roi,0,0,0,ech_fou,0,0,0,0,0)

        afficher_masque_attaque_dans_console(ech, 0)

        masque = 0
        a, b, c,d = ech.get_pieces_immobilisees_diagonalement(0)
        for e in a:
            print(e)
            masque = masque | (1 << e)

        afficher_bitboard_dans_console_couleur(masque)
    elif choix == 9:

        print("Placer le roi")
        ech_roi = generer_un_echiquier_personnalise()
        print("Placer l'echiquier des tours enemies")
        ech_tour = generer_un_echiquier_personnalise()
        print("Placer des pions qui bloque la trajectoire")
        ech_pions = generer_un_echiquier_personnalise()

        ech=Echiquier(ech_pions,0,0,0,0,ech_roi,0,ech_tour,0,0,0,0,0,0,0)

        afficher_masque_attaque_dans_console(ech, 0)

        masque = 0
        a, b, c,d = ech.get_pieces_immobilisees_vert_horiz(1)
        for e in a:
            print(e)
            masque = masque | (1 << e)

        afficher_bitboard_dans_console_couleur(masque)
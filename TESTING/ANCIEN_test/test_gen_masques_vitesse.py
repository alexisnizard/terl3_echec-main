from terl3_echec.gen_masques import *
import time

#Des tests (pas organisé) pour comparer l'efficacité de diverses fonctions

def main(): #On créer cette fonction uniquement afin de pouvoir mettre un return 0 où l'on veut pour stopper l'execution du programme, comme en C

    print("Comparaison tab numpy")
    ech = [0 for i in range(50)]
    ech3= []
    start = time.time()
    for i in range(1000000):
        for i in range(50):
            ech[i]=1
        ech3=[]
    end = time.time()
    print("Temps de la fonction utilisant pas numpy:", end - start)

    ech2 = []
    start = time.time()
    for i in range(1000000):
        for i in range(50):
            ech2.append(1)
        ech2=[]
    end = time.time()
    print("Temps de la fonction utilisant pas numpy:", end - start)

    return 0  # <===== interrompt les autres tests, comme ca chaque fois qu'on ve faire un neural_network_chess-1.4 de vitesse on le met tout en haut

    #https://stackoverflow.com/questions/16597066/why-is-numpy-slower-than-python-how-to-make-code-perform-better
    #https://towardsdatascience.com/is-numpy-really-faster-than-python-aaa9f8afb5d7
    print("Comparaison index tab vs numpy")
    ech =[55555555555555 for i in range(1000000)]
    start = time.time()
    for i in range(10000000):
        ech[6]
    end = time.time()
    print("Temps de la fonction utilisant pas numpy:", end - start)

    ech2 = np.array([55555555555555 for i in range(1000000)], dtype=np.uint64)
    start = time.time()

    for i in range(10000000):
        ech2[6]
    end = time.time()
    print("Temps de la fonction utilisant numpy:", end - start)


    def soustr_vs_bitshift():
        e=64

        while e>9:
            e-=9
            1 << e

    def bitshift_vs_soustr():
        for e in range(64):
            1 << 64

    print("Comparaison de soustr_vs_bitshift")
    start = time.time()
    for i in range(10000000):
        soustr_vs_bitshift()
    end = time.time()
    print("Temps de la fonction utilisant soustr_vs_bitshift:", end - start)

    start = time.time()
    for i in range(10000000):
        bitshift_vs_soustr()
    end = time.time()
    print("Temps de la fonction utilisant bitshift_vs_soustr:", end - start)



    def get_position(bitboard):
        tab = np.zeros(64,dtype=int)
        ind=0

        for i in range(64):
            if (1 << i) | bitboard == bitboard:
                tab[ind]=int(i)
                ind+=1

        return tab

    def get_position_sans_numpy(bitboard):
        tab = []

        for i in range(64):
            if (1 << i) | bitboard == bitboard:
                tab.append(i)

        return tab



    print("TEST 0 : Comparaison de deux fonctions :get_position et get_position_sans_numpy")
    start = time.time()
    for i in range(1000000): #1000000 donne 13 secondes
        get_position(1111111111111111111111111111111111111111111111111111111111111111)
    end = time.time()
    print("Temps de la fonction utilisant get_position:",end - start)

    start = time.time()
    for i in range(1000000): #1000000 donne 8 secondes !!
        get_position_sans_numpy(1111111111111111111111111111111111111111111111111111111111111111)
    end = time.time()
    print("Temps de la fonction utilisant get_position_sans_numpy:",end - start)





    def gen_masques_tour_avec_modulo(tab):
        nouvelle_ligne=0

        for e in range(64):
            #on passe a une nouvelle ligne de l'echiquier
            if e % 8 ==0 and e!=0:
                nouvelle_ligne+=8

            # on créer la ligne gauche et droite en même temps
            i=nouvelle_ligne
            masque = 0
            while i<8+nouvelle_ligne:
                masque= masque | (1 << i)
                i+=1

            # on créer la colonne du haut
            i=e
            while i<64:
                masque=masque | (1<<i)
                i+=8

            #on créer la colonne du bas
            i=0
            while i<nouvelle_ligne:
                masque=masque | (1 << e-(nouvelle_ligne-i))
                i+=8

            #on vire le 1 où se trouve la tour
            masque= masque ^ (1<<e)

            tab[e]=masque


    print("TEST 1 : Comparaison de deux fonctions générant les masques de la tour")
    start = time.time()
    for i in range(10000): #500000 donne 65 secondes
        masques=gen_masques_tour()
    end = time.time()
    print("Temps de la fonction utilisant deux autres masques pour savoir si on atteint un bord:",end - start)

    masques = np.zeros(64, np.uint64)

    start = time.time()
    for i in range(10000): #500000 donne 71 secondes
        masques = np.zeros(64, np.uint64)
        gen_masques_tour_avec_modulo(masques)
    end = time.time()
    print("Temps de la fonction utilisant modulo pour savoir si on atteint un bord:",end - start)




    ############################################
    ############################################
    ############################################
    ############################################


    def gen_masques_roi_sans_return(tab):
        masque_0 = gen_masque_colonne(0)
        masque_7 = gen_masque_colonne(7)

        for e in range(64):
            masque = 0

            if (1 << e) | masque_0 == masque_0:
                if e==0:
                    masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9)
                elif e==56:
                    masque = masque | (1 << e + 1) | (1 << e - 7) | (1 << e - 8)
                else:
                    masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9) | (1 << e - 7) | (1 << e - 8)

            elif (1 << e) | masque_7 == masque_7:
                if e==7:
                    masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7)
                elif e==63:
                    masque = masque | (1 << e - 1) | (1 << e - 8) | (1 << e - 9)
                else:
                    masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7) | (1 << e - 8) | (1 << e - 9)
            elif e<8:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9)
            elif e > 55:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e - 7) | (1 << e - 8) | (1 << e - 9)
            else:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9) | (
                1 << e - 7) | (1 << e - 8) | (1 << e - 9)

            tab[e] = masque


    def gen_masques_roi_return():
        tab= np.zeros(64, np.uint64)
        masque_0 = gen_masque_colonne(0)
        masque_7 = gen_masque_colonne(7)

        for e in range(64):
            masque = 0

            if (1 << e) | masque_0 == masque_0:
                if e==0:
                    masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9)
                elif e==56:
                    masque = masque | (1 << e + 1) | (1 << e - 7) | (1 << e - 8)
                else:
                    masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9) | (1 << e - 7) | (1 << e - 8)

            elif (1 << e) | masque_7 == masque_7:
                if e==7:
                    masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7)
                elif e==63:
                    masque = masque | (1 << e - 1) | (1 << e - 8) | (1 << e - 9)
                else:
                    masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7) | (1 << e - 8) | (1 << e - 9)
            elif e<8:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9)
            elif e > 55:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e - 7) | (1 << e - 8) | (1 << e - 9)
            else:
                masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9) | (
                1 << e - 7) | (1 << e - 8) | (1 << e - 9)

            tab[e] = masque
        return tab

    #Créer tout les masques du ROI
    def gen_masques_roi_avec_modulo(tab):# Il n'y a pas de return ,on passe directement le tableau qui sera modifié en paramètre
        nouvelle_ligne = 0

        for e in range(64):
            # on passe a une nouvelle ligne de l'echiquier
            # faut bien voir que le modulo 7 est vicieux contrairement au modulo 8
            # tout les modulos 8 = première case (en partant de la droite) de chaque ligne de l'echiquier
            # tout les modulos 7 != derniere case de chaque ligne de l'échiquier
            if e % 8 == 0 and e != 0:
                nouvelle_ligne += 8

            masque = 0

            if e % (7+nouvelle_ligne) != 0 or e == 0: #case à gauche
                masque = masque | (1 << e + 1)
            if e % 8 != 0: #case à droite
                masque = masque | (1 << e - 1)

            if e < 56: #case en haut
                masque = masque | (1 << e + 8)
            if e > 7: #case en bas
                masque = masque | (1 << e - 8)

            if e < 56 and e%(7+nouvelle_ligne) !=0 or e == 0: #case diagonal haute gauche
                masque = masque | (1 << e + 9)

            if e < 56 and e % 8 != 0: #case diagonal haute droite
                masque = masque | (1 << e + 7)

            if e > 7 and e % 8 !=0:#case diagonal basse droite
                masque= masque | (1<< e - 9)

            if e > 7 and e % (7+nouvelle_ligne) !=0:#case diagonal basse gauche
                masque= masque | (1<< e - 7)

            tab[e]=masque


    print("TEST 2 : Comparaison de deux fonctions générant les masques du roi")
    start = time.time()
    for i in range(10000):
        masques = np.zeros(64, np.uint64)
        gen_masques_roi_sans_return(masques)
    end = time.time()
    print("Temps de la fonction utilisant deux autres masques pour savoir si on atteint un bord:",end - start)

    masques = np.zeros(64, np.uint64)

    start = time.time()
    for i in range(10000):
        masques = np.zeros(64, np.uint64)
        gen_masques_roi_avec_modulo(masques)
    end = time.time()
    print("Temps de la fonction utilisant modulo pour savoir si on atteint un bord:",end - start)


    print("TEST 3 : Comparaison de deux fonctions générant les masques du roi avec et sans return")
    start = time.time()
    for i in range(1000):
        masques = np.zeros(64, np.uint64)
        gen_masques_roi_sans_return(masques)
    end = time.time()
    print("Temps de la fonction sans return:",end - start)


    start = time.time()
    for i in range(1000):
        masquess=gen_masques_roi_return()
    end = time.time()
    print("Temps de la fonction avec return:",end - start)


    #########################

    masque_ligne_1=gen_masque_ligne(1) #note : >7 and <16


    print("TEST 4 : Comparaison des comparateur vs masque ligne")
    start = time.time()
    for j in range(10):
        for i in range(64):
            7 < i < 16
    end = time.time()
    print("Temps:",end - start)

    start = time.time()
    for j in range(10):
        for i in range(64):
            (1 << i) | masque_ligne_1 != masque_ligne_1
    end = time.time()
    print("Temps:",end - start)


    ###############################


    def get_position(bitboard):
        tab = np.zeros(8,dtype=int)
        ind=0

        for i in range(64):
            if (1 << i) | bitboard == bitboard:
                tab[ind]=i
                ind+=1

        return tab

    def get_position_modulo(bitboard):
        tab = np.zeros(8,dtype=int)
        ind=0
        cpt = 0

        if bitboard % 2 != 0:
            tab[ind]=0
            cpt+=1
        while (bitboard > 1):
            bitboard = bitboard // 2
            cpt += 1
            if bitboard % 2 != 0:
                tab[ind]= cpt
                ind+=1
        return tab

    print("TEST 5 : Comparaison get_position")
    start = time.time()
    for i in range(1000000):
        get_position(71776119061217280) #7.168468952178955 s pr 1000000
    end = time.time()
    print("Temps:",end - start)

    start = time.time()
    for i in range(1000000):
        get_position_modulo(71776119061217280)#8.266635656356812 s pr 1000000
    end = time.time()
    print("Temps:",end - start)

    return 0

main()
from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import *
from terl3_echec.TESTING.ANCIEN_test.test_gen_all_attaques_ennemie_utils_ANCIENNES_METHODES import *

#Les methodes testé ici ne font pas partit de la vrai class Echiquier ,elles sont obsolètes (elle ont été remplacé par une methode plus efficaces)
#Donc ici on neural_network_chess-1.4 les anciennes méthodes ,pour cela on a créer dans le fichier test_gen_all_attaques_ennemie_util une classe Echiquier2 qui les implémente

while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Generer le masque d'attaques d'un echiquier réaliste aléatoire")
    print("2 - Generer le masque d'attaques (blanc) d'un echiquier réaliste aléatoire")
    print("3 - Generer le masque d'attaques (noir) d'un echiquier réaliste aléatoire")
    print("4 - Generer le masque d'attaques (blanc) d'un echiquier réaliste aléatoire composé de 8 pièces exactement")
    print("5 - Generer le masque d'attaques (noir) d'un echiquier réaliste aléatoire composé de 8 pièces exactement")
    print("6 - Generer un echiquier réaliste aleatoire avec affichage unicode")
    print("0 - Exit")
    choix=int(input())

    masques = 0

    if choix==0:
        break
    elif choix == 1:
        print("Combien de pièce voulez-vous dans l'echiquier (entre 2 et 32) ? -1 pour un nombre aléatoire")
        choix_nbr_piece = int(input())
        if choix_nbr_piece != -1 and  (choix_nbr_piece < 2 or choix_nbr_piece > 32):  # on s'assure que le nbr de pièce présent dans l'echiquier soit réaliste
            print("Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32\n")
            continue

        print("Qui est l'attaquant ? 0 : blanc, 1 :  noir")
        choix_attaquant = int(input())
        afficher_masque_attaque_dans_console2(generer_un_echiquier_realiste_aleatoire2(choix_nbr_piece), choix_attaquant)
    elif choix == 2:
        print("Combien de pièce voulez-vous dans l'echiquier (entre 2 et 32) ? -1 pour un nombre aléatoire")
        choix_nbr_piece = int(input())
        if choix_nbr_piece != -1 and  (choix_nbr_piece < 2 or choix_nbr_piece > 32):
            print("Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32\n")
            continue
        afficher_masque_attaque_dans_console2(generer_un_echiquier_realiste_aleatoire2(choix_nbr_piece),0)
    elif choix == 3:
        print("Combien de pièce voulez-vous dans l'echiquier (entre 2 et 32) ? -1 pour un nombre aléatoire")
        choix_nbr_piece = int(input())
        if choix_nbr_piece != -1 and  (choix_nbr_piece < 2 or choix_nbr_piece > 32):
            print("Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32\n")
            continue
        afficher_masque_attaque_dans_console2(generer_un_echiquier_realiste_aleatoire2(choix_nbr_piece),1)
    elif choix == 4:
        afficher_masque_attaque_dans_console2(generer_un_echiquier_realiste_aleatoire2(8),0)
    elif choix == 5:
        afficher_masque_attaque_dans_console2(generer_un_echiquier_realiste_aleatoire2(8), 1)
    print("Code couleur : ")
    print("Bleu claire = pièces blanches | Bleu foncé = pièces noirs")
    print("Vert = cases attaqué par l'ennemie | Jaune = Pieces attaqué par l'ennemie | Rouge = Le roi est en échec\n")
    if choix==6:
        print("Paramètre idéal pour obtenir un bon rendu visuel dans la console : police High Tower Text, taille de police 24, espacement de ligne 1")
        nbrpiece=randint(2,32)
        print("L'echiquier possède",nbrpiece," pièces")
        afficher_Echiquier_dans_console_unicode(generer_un_echiquier_realiste_aleatoire2(nbrpiece))

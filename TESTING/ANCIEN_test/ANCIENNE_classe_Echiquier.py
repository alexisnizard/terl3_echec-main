from terl3_echec.gen_pseudos_legaux import *
from terl3_echec.gen_pseudos_legaux_sliders_inclus_premier_allie import *

#ancienne classe Echiquier
class Echiquier:
    # constructeur par default et paramétré en même temps
    def __init__(self, pion_b=65280, tour_b=129, cavalier_b=66, fou_b=36, dame_b=16, roi_b=8, pion_n=71776119061217280,
                 tour_n=9295429630892703744, cavalier_n=4755801206503243776, fou_n=2594073385365405696,
                 dame_n=1152921504606846976, roi_n=576460752303423488, tour_et_rock=31, en_passant=0, cpt_demicoup=0):

        self.ech = [pion_b, tour_b, cavalier_b, fou_b, dame_b, roi_b, pion_n, tour_n, cavalier_n, fou_n, dame_n, roi_n,
                    tour_et_rock, en_passant, cpt_demicoup]

        self.ech_position = [self.get_position(self.ech[i]) for i in range(12)]

        self.pseudo_leg = [];
        self.pseudo_leg_attaque = []

        for i in range(12):
            double_return = self.gen_pseudos_legaux_chaque_piece(i)
            self.pseudo_leg.append(double_return[0])  # mettre a jour après le deplacement d'une piece
            self.pseudo_leg_attaque.append(double_return[1])  # mettre a jour après le deplacement d'une piece

    def get_echiquier_blanc(self):
        return self.ech[0] | self.ech[1] | self.ech[2] | self.ech[3] | self.ech[4] | self.ech[5]

    def get_echiquier_noir(self):
        return self.ech[6] | self.ech[7] | self.ech[8] | self.ech[9] | self.ech[10] | self.ech[11]

    def get_echiquier(self):
        return self.get_echiquier_blanc() | self.get_echiquier_noir()

    # Retourne les cases occupées par une pièce dans l'echiquier (bitboard) donnée en paramètre
    @staticmethod
    def get_position(bitboard):
        tab = []

        for i in range(64):
            if (1 << i) & bitboard:
                tab.append(i)

        return tab

    # Genere les coups pseudos legaux et pseudos legaux d'attaque de chaque pièce pour chaque index
    def gen_pseudos_legaux_chaque_piece(self, index):
        nom_fonctions = [gen_pseudos_legaux_tour, gen_pseudos_legaux_cavalier, gen_pseudos_legaux_fou,
                         gen_pseudos_legaux_dame, gen_pseudos_legaux_roi]

        nom_des_masque_non_sliders = [masque_tour, masque_cavalier, masque_fou, masque_dame, masque_roi]
        nom_fonctions_sliders = [0, gen_pseudos_legaux_tour_inc_allie, 0, gen_pseudos_legaux_fou_inc_allie,
                                 gen_pseudos_legaux_dame_inc_allie, 0]

        # tableau des masques de coups legaux pour chaque coup :
        resultat = []

        # tableau des masques de coups legaux incluant les pieces alliés pour chaque coup :
        resultat_attaque = []

        if index < 6:
            # noms des fonction generant le masque d'attaque de NOS pions + les masques pseudos legaux de nos autres pieces
            # on utilise l'attaque possible ici (car on veut tout nos deplacements possibles)
            nom_fonctions = [gen_pseudos_legaux_attaque_possible_pion_blanc] + nom_fonctions

            nom_des_masque_non_sliders = [
                                             masque_pion_blanc_attaque] + nom_des_masque_non_sliders  # /!\ on utilise le tableau de masque directement, pas celui des pseudo legaux

            ech_allie = self.get_echiquier_blanc()
            ech_ennemie = self.get_echiquier_noir()
            avance = gen_pseudos_legaux_avance_possible_pion_blanc
            index_0_ou_6 = 0

            roi_ennemie = 11
            index_t = 1;
            index_f = 3;
            index_d = 4

        else:
            nom_fonctions = [gen_pseudos_legaux_attaque_possible_pion_noir] + nom_fonctions
            nom_des_masque_non_sliders = [masque_pion_noir_attaque] + nom_des_masque_non_sliders
            avance = gen_pseudos_legaux_avance_possible_pion_noir
            ech_allie = self.get_echiquier_noir()
            ech_ennemie = self.get_echiquier_blanc()
            index_0_ou_6 = 6

            roi_ennemie = 5
            index_t = 7;
            index_f = 9;
            index_d = 10

        for j in self.ech_position[index]:  # on parcours toutes les pièces presente dans l'echiquier

            masque_pseudo_leg = nom_fonctions[index - index_0_ou_6](j, ech_allie, ech_ennemie)
            masque_attaque_de_la_piece_allie = masque_pseudo_leg

            # on prend pas en compte le roi pr les sliders

            if index == index_t or index == index_f or index == index_d:
                masque_ignorant_allie = nom_fonctions_sliders[index - index_0_ou_6](j, ech_allie, ech_ennemie,
                                                                                    self.ech[roi_ennemie])

            # on ajoute l'allié dans le masque pseudo legal
            else:
                masque_ignorant_allie = nom_des_masque_non_sliders[index - index_0_ou_6][j]

            masque_avance_du_pion_allie = avance(j, ech_allie, ech_ennemie)
            if index == index_0_ou_6:  # si on est dans la masque des pions on ajoute la masque d'avancement dans les coups legaux
                resultat.append(masque_attaque_de_la_piece_allie | masque_avance_du_pion_allie)
            else:
                resultat.append(masque_attaque_de_la_piece_allie)

            resultat_attaque.append(masque_ignorant_allie)

        return resultat, resultat_attaque

    # On fusionne tout les masques pseudos legaux d'attaque (d'attaque = les pieces mange egalement leurs alliés)
    def gen_attaque_global(self, couleur):
        if couleur == 0:
            index_p = 0
        else:
            index_p = 6

        masque = 0
        for i in range(index_p, index_p + 6):
            for j in self.pseudo_leg_attaque[i]:
                masque = masque | j

        return masque

    # Compte le nombre de piece dans un bitboard
    @staticmethod
    def compte_piece(bitboard):
        cpt = 0

        for i in range(64):
            if (1 << i) & bitboard:
                cpt += 1

        return cpt

    # Retourne la position du roi ,vu qu'on va souvent avoir besoin de cette info, c'est mieux de créer une fonction
    # spécifique plutot que d'utilisé get_position à chaque fois
    def get_position_roi(self, index):
        for i in range(64):
            if (1 << i) & self.ech[index]:
                return i

    @staticmethod
    def relier_roi_et_fou(pos_roi, pos_fou):
        masque_diagonal = 0
        copy_pos_roi = pos_roi  # une copie du roi pour garder sa position initiale au return et l'enlever du masque avec xor

        # Pas de chance 63 est le seul nombre de l'echiquier qui soit multiple de 7 et 9 ,il faut donc le traité à part (car il valide à la fois le if et le else)
        if pos_roi > pos_fou:  # soit on monte du fou vers le roi

            if pos_roi % 7 == pos_fou % 7 and pos_roi != 63:  # la diagonal va vers la droite. Utilisation des modulos car ici des bitshifts prendraient plus de temps
                while pos_fou != pos_roi:  # tant quon a pas rejoins notre roi on continue de monter
                    pos_fou += 7
                    masque_diagonal = masque_diagonal | (1 << pos_fou)

            else:  # la diagonal va vers la gauche.
                while pos_fou != pos_roi:
                    pos_fou += 9
                    masque_diagonal = masque_diagonal | (1 << pos_fou)

        else:  # soit on descend du fou vers le roi

            if pos_roi % 7 == pos_fou % 7 and pos_fou != 63:  # la diagonal va vers la gauche.7
                while pos_fou != pos_roi:
                    pos_fou -= 7
                    masque_diagonal = masque_diagonal | (1 << pos_fou)

            else:  # la diagonal va vers la droite.9
                while pos_fou != pos_roi:
                    pos_fou -= 9
                    masque_diagonal = masque_diagonal | (1 << pos_fou)

        # on renvoit un tableau qui indique le masque + la direction a prendre (car on réutilisera cette direction plus tard) . Le xor sert à virer le roi
        return masque_diagonal ^ (1 << copy_pos_roi)

    @staticmethod
    def relier_roi_et_tour(pos_roi, pos_tour):

        masque_vert_ou_horiz = 0
        copy_pos_roi = pos_roi

        if pos_roi > pos_tour:  # si la tour doit monté vers le roi

            if pos_roi % 8 == pos_tour % 8:  # on est dans une colonne
                while pos_tour != pos_roi:
                    pos_tour += 8
                    masque_vert_ou_horiz = masque_vert_ou_horiz | (1 << pos_tour)

            else:  # on est dans une ligne
                while pos_tour != pos_roi:
                    pos_tour += 1
                    masque_vert_ou_horiz = masque_vert_ou_horiz | (1 << pos_tour)

        else:  # la tour doit descendre vers le roi

            if pos_roi % 8 == pos_tour % 8:  # on est dans une colonne8
                while pos_tour != pos_roi:
                    pos_tour -= 8
                    masque_vert_ou_horiz = masque_vert_ou_horiz | (1 << pos_tour)

            else:  # on est dans une ligne1
                while pos_tour != pos_roi:
                    pos_tour -= 1
                    masque_vert_ou_horiz = masque_vert_ou_horiz | (1 << pos_tour)

        return masque_vert_ou_horiz ^ (1 << copy_pos_roi)

    # Génère la liste des pièces qui sont immobilisé diagonalement car elle protege le roi d'un echec,il faut écrire en parametre la couleur de l'ALLIE (0 pr blanc ,else pour noir)
    # On fait un quadruple return
    # en dernier return : le masque_protection cad là on l'on pourrait placé une piece pour bloqué l'echec
    # en avant dernier : le nombre d'echec que notre roi se prend (en diagonal)
    # en avant avant dernier la case qui met en echec le roi.
    # Si il y a plusieurs echec sur le roi ,cette case est réécrite mais on s'en fiche car elle est utile uniquement lorsqu'il n'y a qu'un seul echec

    # A partir du roi on créer un masque du fou (du fichier gen_masques) puis on verifie si il y a bien un fou ou une dame
    # dans la trajectoire du masque.
    # Si c'est le cas ,on verifie si un ennemie bloque la trajectoire ,si c'est le cas c'est finit ya zero piece immobilisé.
    # Sinon on verifie combien d'allié bloque la trajectoire ,si 0 ya echec direct ,si 1 on a la piece immoblisé ,si >1 aucune piece est immobilisé.
    def get_pieces_immobilisees_diagonalement(self, couleur):

        cpt_nbr_dechec = 0
        case_dou_provient_lechec = []
        masque_protection = []  # la où l'on pourra placé des pièces pour bloqué l'echec
        ya_echec_direct = 0
        copy_e = 0
        ya_echec_direct2 = 0
        copy_e = 0
        copy_e2 = 0

        if couleur == 0:
            pos_roi = self.get_position_roi(5);
            pos_fou_ennemie = self.ech[9];
            pos_dame_ennemie = self.ech[10]
            ech_ennemie = self.get_echiquier_noir();
            ech_allie = self.get_echiquier_blanc()
        else:
            pos_roi = self.get_position_roi(11);
            pos_fou_ennemie = self.ech[3];
            pos_dame_ennemie = self.ech[4]
            ech_ennemie = self.get_echiquier_blanc();
            ech_allie = self.get_echiquier_noir()

        piece_fige = []  # la listes des pieces figés

        ya_til_des_fous = masque_fou[pos_roi] & (
        pos_fou_ennemie | pos_dame_ennemie)  # on fait d'une pierre deux coups en ajoutant également le bitboard de la dame

        if ya_til_des_fous != 0:  # si il ya un fou(s) / dame(s) dans la trajectoire du roi
            ou_sont_les_fous = self.get_position(ya_til_des_fous)

            for e in ou_sont_les_fous:  # les doubles echecs sont très rares au echec, on peut considérer que 99% du temps il y aura qu'une seule itération
                diag_roi_fou = self.relier_roi_et_fou(pos_roi, e)

                # Echec direct : le roi et la piece qui le met en échec sont côtes à côtes
                # Seul échapatoire : soit manger la piece ,soit faire bougé le roi. On ne peut pas bloqué avec un allié !
                if diag_roi_fou == 0:
                    cpt_nbr_dechec += 1
                    ya_echec_direct = 1
                    copy_e = e

                # si ya une piece ennemie qui bloque le fou, on a rien d'autre a calculé car aucune piece allié est immobilisé
                elif self.compte_piece(diag_roi_fou & ech_ennemie) == 0:

                    pieces_allies_dans_la_diagonal = diag_roi_fou & ech_allie
                    nbr_de_piece_allie_bloquant_lechec = self.compte_piece(pieces_allies_dans_la_diagonal)

                    if nbr_de_piece_allie_bloquant_lechec == 0:  # On est en echec direct
                        cpt_nbr_dechec += 1
                        copy_e2 = e
                        copy_masque_protect = diag_roi_fou
                        ya_echec_direct2 = 1

                    elif nbr_de_piece_allie_bloquant_lechec == 1:  # Ca y est, on à la pièce immobilisées
                        piece_fige.append(self.get_position(pieces_allies_dans_la_diagonal)[0])
                        case_dou_provient_lechec.append(e)
                        masque_protection.append(diag_roi_fou)

        if ya_echec_direct:
            masque_protection = []
            case_dou_provient_lechec.insert(0, copy_e)  # mettre le e en indice 0 quand ya echec direct

        if ya_echec_direct2:
            case_dou_provient_lechec.insert(0, copy_e2)
            masque_protection.insert(0, copy_masque_protect)

        return piece_fige, case_dou_provient_lechec, cpt_nbr_dechec, masque_protection  # un quadruble return grace a python

    # Faut entrer la couleur de l'allié en paramètre
    def get_pieces_immobilisees_vert_horiz(self, couleur):

        cpt_nbr_dechec = 0
        case_dou_provient_lechec = []
        masque_protection = []  # la où l'on pourra placé des pièces pour bloqué l'echec
        ya_echec_direct = 0
        ya_echec_direct2 = 0
        copy_e = 0
        copy_e2 = 0

        if couleur == 0:
            pos_roi = self.get_position_roi(5);
            pos_tour_ennemie = self.ech[7];
            pos_dame_ennemie = self.ech[10]
            ech_ennemie = self.get_echiquier_noir();
            ech_allie = self.get_echiquier_blanc()
        else:
            pos_roi = self.get_position_roi(11);
            pos_tour_ennemie = self.ech[1];
            pos_dame_ennemie = self.ech[4]
            ech_ennemie = self.get_echiquier_blanc();
            ech_allie = self.get_echiquier_noir()

        piece_fige = []  # la listes des pieces figés

        ya_til_des_tours = masque_tour[pos_roi] & (
        pos_tour_ennemie | pos_dame_ennemie)  # on fait d'une pierre deux coups en ajoutant également le bitboard de la dame

        if ya_til_des_tours != 0:  # si il ya une tour(s) / dame(s) dans la trajectoire du roi
            ou_sont_les_tours = self.get_position(ya_til_des_tours)

            for e in ou_sont_les_tours:  # les doubles echecs sont très rares au echec, on peut considérer que 99% du temps il y aura qu'une seule itération
                vert_horiz_roi_tour = self.relier_roi_et_tour(pos_roi, e)

                # Echec direct : le roi et la piece qui le met en échec sont côtes à côtes
                # Seul échapatoire : soit manger la piece ,soit faire bougé le roi. On ne peut pas bloqué avec un allié !
                if vert_horiz_roi_tour == 0:
                    cpt_nbr_dechec += 1
                    copy_e = e
                    ya_echec_direct = 1

                # si ya une piece ennemie qui bloque la tour, on a rien d'autre a calculé car aucune piece allié est immobilisé
                elif self.compte_piece(vert_horiz_roi_tour & ech_ennemie) == 0:

                    pieces_allies_dans_la_vert_horiz = vert_horiz_roi_tour & ech_allie
                    nbr_de_piece_allie_bloquant_lechec = self.compte_piece(pieces_allies_dans_la_vert_horiz)

                    if nbr_de_piece_allie_bloquant_lechec == 0:  # On est en echec direct
                        cpt_nbr_dechec += 1
                        copy_e2 = e
                        copy_masque_protect = vert_horiz_roi_tour
                        ya_echec_direct2 = 1

                    elif nbr_de_piece_allie_bloquant_lechec == 1:  # Ca y est, on à la pièce immobilisées
                        piece_fige.append(self.get_position(pieces_allies_dans_la_vert_horiz)[0])
                        case_dou_provient_lechec.append(e)
                        masque_protection.append(vert_horiz_roi_tour)

        if ya_echec_direct:
            masque_protection = []
            case_dou_provient_lechec.insert(0, copy_e)  # mettre le e en indice 0 quand ya echec direct

        if ya_echec_direct2:
            case_dou_provient_lechec.insert(0, copy_e2)
            masque_protection.insert(0, copy_masque_protect)

        return piece_fige, case_dou_provient_lechec, cpt_nbr_dechec, masque_protection

    # on entre la couleur de l'allié en parametre
    def echec_cavaliers(self, couleur):
        if couleur == 0:
            return gen_pseudos_legaux_cavalier(self.ech_position[5][0], self.get_echiquier_blanc(),
                                               self.get_echiquier_noir()) & self.ech[8]
        else:
            return gen_pseudos_legaux_cavalier(self.ech_position[11][0], self.get_echiquier_noir(),
                                               self.get_echiquier_blanc()) & self.ech[2]

    def echec_pions(self, couleur):
        if couleur == 0:
            return masque_pion_blanc_attaque[self.ech_position[5][0]] & self.ech[6]
        else:
            return masque_pion_noir_attaque[self.ech_position[11][0]] & self.ech[0]

    # renvoit la liste de nos pions pouvant capturé en passant
    def pions_pouvant_capturer_en_passant(self):
        pos_pion_pouvant_capturer = []

        if self.ech[13] > 39:  # si c'est une prise pour les blanc
            if masque_pion_noir_attaque[self.ech[13]] & self.ech[
                0]:  # si on a bien un pion blanc dispo pour capture le pion noir

                for i in self.ech_position[0]:  # pour chaque pion blanc :
                    if (i == self.ech[13] - 7) or (i == self.ech[13] - 9):
                        pos_pion_pouvant_capturer.append(i)
        else:  # si c'est une prise pour les noirs
            if masque_pion_blanc_attaque[self.ech[13]] & self.ech[
                6]:  # si on a bien un pion noir dispo pour capture le pion blanc

                for i in self.ech_position[6]:  # pour chaque pion noir :
                    if (i == self.ech[13] + 7) or (i == self.ech[13] + 9):
                        pos_pion_pouvant_capturer.append(i)

        return pos_pion_pouvant_capturer

    def puis_je_rocker(self, couleur, masque_attaque_ennemie):

        quel_side_peut_rock = []
        peut_rocker = 1

        if couleur == 0:

            # uniquement pour les tests : A SUPPRIMER
            if not ((1 << 3) & self.ech[5]):
                return []

            if self.ech[12] & 2:  # si on peut eventuellement rocké coté roi blanc

                if not (6 & self.get_echiquier()):  # si les cases sont vides

                    for i in range(1, 4):  # on parcours chaque case entre la tour et le roi
                        if (1 << i) & masque_attaque_ennemie:  # on vérif qu'il n'y a pas d'echec
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        quel_side_peut_rock = [[3, 1]]  # le roi en case 3 va en case 1

                    # uniquement pour les tests : A SUPPRIMER
                    if not ((1 << 0) & self.ech[1]):
                        quel_side_peut_rock = []

            peut_rocker = 1

            if self.ech[12] & 4:

                if not (112 & self.get_echiquier()):

                    for i in range(3, 7):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        quel_side_peut_rock.append([3, 5])

                    # uniquement pour les tests : A SUPPRIMER
                    if not ((1 << 7) & self.ech[1]):
                        if peut_rocker:
                            quel_side_peut_rock.pop()

        else:

            # uniquement pour les tests : A SUPPRIMER
            if not ((1 << 59) & self.ech[11]):
                return []

            if self.ech[12] & 8:  # si on peut eventuellement rocké coté roi noir

                if not (432345564227567616 & self.get_echiquier()):

                    for i in range(57, 60):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        quel_side_peut_rock = [[59, 57]]

                    # uniquement pour les tests : A SUPPRIMER
                    if not ((1 << 56) & self.ech[7]):
                        quel_side_peut_rock = []

                peut_rocker = 1

            if self.ech[12] & 16:

                if not (8070450532247928832 & self.get_echiquier()):

                    for i in range(59, 63):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        quel_side_peut_rock.append([59, 61])

                    # uniquement pour les tests : A SUPPRIMER
                    if not ((1 << 63) & self.ech[7]):
                        if peut_rocker:
                            quel_side_peut_rock.pop()

        return quel_side_peut_rock

    # couleur de l'allié en parametre (0 blanc ,1 noir)
    def gen_coup_legaux(self, couleur):

        coup_legaux = [[], [], [], [], [], [], [],
                       []]  # on initialise le tableau des masques des coups legaux de chaque pieces + en passant (indice [-2]) + rock (indice [-1])
        # pour chaque index : [sa position ,le masque de ses deplacement]

        if couleur == 0:  # si on est blanc
            roi = 5
            depart = 0
            rock_possible = self.ech[12] & 6
            nom_fonction = gen_pseudos_legaux_avance_possible_pion_blanc
            echic_allie = self.get_echiquier_blanc()
            echic_ennemie = self.get_echiquier_noir()
        else:
            roi = 11
            depart = 6
            rock_possible = self.ech[12] & 24
            nom_fonction = gen_pseudos_legaux_avance_possible_pion_noir
            echic_allie = self.get_echiquier_noir()
            echic_ennemie = self.get_echiquier_blanc()

        # Initialisation ,on lance toutes les fonctions dès le debut et on stock les resultats dans des variables

        # Grace au quadruple return possible avec python on initialise directement ces 4 variables :
        # 1er variable = liste des cases des pieces immobilisé
        # 2eme variable = case d'ou provient l'echec
        # 3eme variable = nbr d'echec que notre roi se prend
        # 4eme variable = masque indiquant les cases où on peut bloqué l'echec
        cases_piece_immobil_diag, case_dou_provient_echec_diag, nbr_echec_diag, masque_protect_diag = self.get_pieces_immobilisees_diagonalement(
            couleur)
        cases_piece_immobil_vert_horiz, case_dou_provient_echec_v_h, nbr_echec_v_h, masque_protect_v_h = self.get_pieces_immobilisees_vert_horiz(
            couleur)
        pieces_immobil_diag = 0
        pieces_immobil_vert_horiz = 0
        # on créer un masque de toutes les pieces immobilisés
        for e in cases_piece_immobil_diag:
            pieces_immobil_diag = pieces_immobil_diag | (1 << e)

        for e in cases_piece_immobil_vert_horiz:
            pieces_immobil_vert_horiz = pieces_immobil_vert_horiz | (1 << e)

        pieces_immobil_total = pieces_immobil_diag | pieces_immobil_vert_horiz
        # Type de retour = ce sont des masque cette fois ci
        cav_enemie_echec = self.echec_cavaliers(couleur)
        pion_enemie_echec = self.echec_pions(couleur)

        # Masque de l'attaque global de l'ennemie
        attaque_ennemie = self.gen_attaque_global(couleur - 1)  # -1 comme ca si c'est 1 c'est zero ,sinn c'est noir

        # int qui indique le nbr d'echec que les cavaliers et pions ennemie nous mettent
        nbr_echec_cav = self.compte_piece(cav_enemie_echec)
        nbr_echec_pion = self.compte_piece(pion_enemie_echec)

        # nbr_echec_diag se charge de compter le nombre de fois qu'un fou ou une dame nous met en echec,
        # nbr_echec_v_h une tour ou une dame, et les deux derniers le cavalier et le pion . Un roi ne peut pas echec.
        # Calcul du nombre de fois que notre roi est en échec :
        get_nbr_echec = nbr_echec_diag + nbr_echec_v_h + nbr_echec_cav + nbr_echec_pion

        # Generation des coups legaux

        # On est obligé de savoir si il ya plus d'1 echec car sinon nos pieces chercherait perpetuellement à bloquer l'echec sans savoir qu'une autre piece nous attaque

        # Si il ya >1 echec alors les coups legaux c'est juste les pseudos legaux du roi (mais faut pas qu'il avance dans une case echec)
        if get_nbr_echec > 1:
            coup_legaux[roi].append([self.ech_position[roi][0],  # position du roi
                                     self.pseudo_leg[roi][0] & ~attaque_ennemie])  # son masque de deplacement possible
            # append plus rapide qu'un compteur qui incremente l'indice du tableau à chaque fois

        elif get_nbr_echec == 1:  # Si il y a exactement 1 echec

            coup_legaux[roi].append([self.ech_position[roi][0], self.pseudo_leg[roi][0] & ~ attaque_ennemie])

            if nbr_echec_diag == 1:  # Si l'echec provient d'une diagonal


                # MANGER la piece :
                for e in range(depart, depart + 5):
                    cpt = 0  # compteur qui nous indiquera l'indice de ech_position
                    for i in self.pseudo_leg[e]:
                        if i & (1 << case_dou_provient_echec_diag[0]):
                            if not ((1 << self.ech_position[e][cpt]) & pieces_immobil_total):
                                coup_legaux[e].append(
                                    [self.ech_position[e][cpt], (1 << case_dou_provient_echec_diag[0])])
                        cpt += 1

                # BLOQUER la piece:
                if masque_protect_diag:  # on s'assure que la piece qui met en echec le roi n'est pas collé à elle
                    for e in range(depart, depart + 5):
                        cpt = 0
                        for i in self.pseudo_leg[e]:
                            if i & masque_protect_diag[0]:  # si on peut bien se déplacer dans la trajectoire
                                if not ((1 << self.ech_position[e][cpt]) & pieces_immobil_total):
                                    coup_legaux[e].append([self.ech_position[e][cpt], i & masque_protect_diag[0]])
                            cpt += 1

            elif nbr_echec_v_h == 1:  # Si l'echec provient d'une vertical ou horizontal


                # MANGER la piece :
                for e in range(depart, depart + 5):
                    cpt = 0
                    for i in self.pseudo_leg[e]:
                        if i & (1 << case_dou_provient_echec_v_h[0]):
                            if not ((1 << self.ech_position[e][cpt]) & pieces_immobil_total):
                                coup_legaux[e].append(
                                    [self.ech_position[e][cpt], (1 << case_dou_provient_echec_v_h[0])])
                        cpt += 1

                # BLOQUER la piece:
                if masque_protect_v_h:
                    for e in range(depart, depart + 5):
                        cpt = 0
                        for i in self.pseudo_leg[e]:
                            if i & masque_protect_v_h[0]:
                                if not ((1 << self.ech_position[e][cpt]) & pieces_immobil_total):
                                    coup_legaux[e].append([self.ech_position[e][cpt], i & masque_protect_v_h[0]])
                            cpt += 1

            elif nbr_echec_cav == 1:  # Si l'echec provient d'un cavalier ,cette fois-ci on peut juste le manger

                # MANGER la piece :
                for e in range(depart, depart + 5):
                    cpt = 0
                    for i in self.pseudo_leg[e]:
                        if i & (1 << self.get_position(cav_enemie_echec)[
                            0]):  # on peut se permettre get_position car on sait qu'il y a qu'UN cavalier qui met en echec le roi
                            if not ((1 << self.ech_position[e][
                                cpt]) & pieces_immobil_total):  # cas ultra vicieux (voir fichier neural_network_chess-1.4 library),on verifie que notre piece n'est pas cloué
                                coup_legaux[e].append(
                                    [self.ech_position[e][cpt], (1 << self.get_position(cav_enemie_echec)[0])])
                        cpt += 1

            elif nbr_echec_pion == 1:  # Si l'echec provient d'un pion

                # MANGER la piece :
                for e in range(depart, depart + 5):
                    cpt = 0
                    for i in self.pseudo_leg[e]:
                        if i & (1 << self.get_position(pion_enemie_echec)[0]):
                            if not ((1 << self.ech_position[e][cpt]) & pieces_immobil_total):
                                coup_legaux[e].append(
                                    [self.ech_position[e][cpt], (1 << self.get_position(pion_enemie_echec)[0])])
                        cpt += 1


        else:  # Si il y a zero echec
            coup_legaux[roi].append([self.ech_position[roi][0], self.pseudo_leg[roi][0] & ~ attaque_ennemie])

            for e in range(depart, depart + 5):
                cpt = 0
                for i in self.ech_position[e]:
                    if not ((1 << i) & pieces_immobil_total):  # si on ne fait pas partit des pieces immobilisés
                        coup_legaux[e].append([self.ech_position[e][cpt], self.pseudo_leg[e][cpt]])

                    # cas particulier : on est "cloué" mais on peut manger la piece qui mettrait notre roi en echec OU se deplacer en continuant de proteger le roi
                    cpt_echec = 0
                    for j in case_dou_provient_echec_diag:
                        if (1 << i) & pieces_immobil_diag and (self.pseudo_leg[e][cpt] & (1 << j)):

                            if e == depart:  # le pion agit differement pr diagonal
                                coup_legaux[e].append([self.ech_position[e][cpt], self.pseudo_leg[e][cpt] & (1 << j)])

                            else:
                                coup_legaux[e].append([self.ech_position[e][cpt],
                                                       (1 << j) | (masque_protect_diag[cpt_echec] & ~ (1 << i))])
                            cpt_echec += 1

                    cpt_echec = 0
                    for k in case_dou_provient_echec_v_h:
                        if e == depart and (
                            1 << i) & pieces_immobil_vert_horiz:  # le pion agit differemnt pr vertical horizontal
                            if abs(k - i) > 7 and masque_protect_v_h[cpt_echec] & (
                                1 << i):  # savoir si c'est pas une ligne et que le pion soit bien sur le masque
                                coup_legaux[e].append(
                                    [self.ech_position[e][cpt], nom_fonction(i, echic_allie, echic_ennemie)])

                        elif (1 << i) & pieces_immobil_vert_horiz and (self.pseudo_leg[e][cpt] & (1 << k)):
                            coup_legaux[e].append(
                                [self.ech_position[e][cpt], (1 << k) | (masque_protect_v_h[cpt_echec] & ~ (1 << i))])
                        cpt_echec += 1

                    cpt += 1

        # si on peut éventuellement faire une prise en passant
        if self.ech[13] != 0:
            coup_legaux[6] = self.pions_pouvant_capturer_en_passant()

        # si on peut éventuellement rocké
        if rock_possible:
            coup_legaux[7] = self.puis_je_rocker(couleur, attaque_ennemie)

        return coup_legaux

        # on va créer des couples (position de la piece ; deplacement de la piece)
        # mais avant ca on renvoit en indice 0 un tableau :
        # si le premiere indice != 0 alors le nombre correspond à l'indice du tableau où c'est un rock
        # si le deuxieme indice != 0 alors le nombre correspond à l'indice du tableau où c'est une prise en passant
        # tout les autres indices existant = indice du tableau où c'est une promotion de pion

    def coup_legaux(self, couleur):
        if couleur == 0:
            ligne_de_promotion = [47, 56]
        else:
            ligne_de_promotion = [7, 16]

        c_legaux = self.gen_coup_legaux(couleur)
        coup_legaux_couple = []

        # promotion pion : (on reserve l'emplacement ,on va le remplir plus tard
        coup_legaux_couple.append([])

        # rock :
        coup_legaux_couple.append(c_legaux[-1])

        # en passant :
        en_passant = []
        for e in c_legaux[-2]:
            en_passant.append([e, self.ech[13]])

        coup_legaux_couple.append(en_passant)

        cpt_index = 3  # les 3 premiers emplacement sont reservés pour promo de pion ,rock et en passant
        for e in range(0, len(c_legaux) - 2):  # les deux derniers index du tableau ne sont pas des déplacements
            for i in c_legaux[e]:
                toute_les_cases = self.get_position(i[1])
                for j in toute_les_cases:
                    if e == 0 and ligne_de_promotion[0] < i[0] < ligne_de_promotion[
                        1]:  # on remplit le tableau promotion de pion en meme temps
                        coup_legaux_couple[0].append(cpt_index)
                    coup_legaux_couple.append([i[0], j])
                    cpt_index += 1

        return coup_legaux_couple

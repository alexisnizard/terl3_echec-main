from terl3_echec import Echiquier
#Ce fichier sert uniquement à créer des fonction permettant de faire fonctionner le vrai fichier de neural_network_chess-1.4 : test_gen_all_attaques_ennemi_ANCIENNES_METHODES



#Nouvelle class Echiquier2 qui implémente d'ancienne méthode
class Echiquier2(Echiquier): #on fait herité la classe Echiquier2 de la classe Echiquier en mettant Echiquier en parametre

    # Génère le masque d'attaque de toutes les pièces d'un même type.
    # gen_pseudo_legaux prend le nom de la fonction gen_pseudos_legaux_... à utiliser
    # exemple : pour generer le masques des tours on passe en parametre gen_pseudos_legaux_tour
    def gen_all_attaques_dun_type_de_piece(self, bitboard, couleur, gen_pseudo_legaux):
        position = self.get_position(bitboard)
        masque = 0
        if couleur == 0:  # si notre echiquier est blanc alors les ennemies sont noirs et inversement
            for i in position:
                masque = masque | gen_pseudo_legaux(i, self.get_echiquier_blanc(), self.get_echiquier_noir())
        else:
            for i in position:
                masque = masque | gen_pseudo_legaux(i, self.get_echiquier_noir(), self.get_echiquier_blanc())

        return masque

    # Génère le masque d'attaque de toutes les pièces ennemies combinés, il faut écrire en parametre la couleur de l'ENNEMIE (0 pr blanc ,else pour noir)
    def gen_all_attaques_ennemie(self, couleur):

        if couleur == 0:  # si l'ennemie est blanc alors ech0...ech5 prennent les valeurs de l'echiquier blanc
            ech0 = self.ech[0];ech1 = self.ech[1];ech2 = self.ech[2];ech3 = self.ech[3];ech4 = self.ech[4];ech5 = self.ech[5]
            fonc_atta_pion = gen_pseudos_legaux_attaque_possible_pion_blanc
        else:  # sinon ils prennent les valeurs de l'echiquier noir
            ech0 = self.ech[6];ech1 = self.ech[7];ech2 = self.ech[8];ech3 = self.ech[9];ech4 = self.ech[10];ech5 = self.ech[11]
            fonc_atta_pion = gen_pseudos_legaux_attaque_possible_pion_noir

        return self.gen_all_attaques_dun_type_de_piece(ech0, couleur, fonc_atta_pion) | \
               self.gen_all_attaques_dun_type_de_piece(ech1, couleur, gen_pseudos_legaux_tour) | \
               self.gen_all_attaques_dun_type_de_piece(ech2, couleur, gen_pseudos_legaux_cavalier) | \
               self.gen_all_attaques_dun_type_de_piece(ech3, couleur, gen_pseudos_legaux_fou) | \
               self.gen_all_attaques_dun_type_de_piece(ech4, couleur, gen_pseudos_legaux_dame) | \
               self.gen_all_attaques_dun_type_de_piece(ech5, couleur, gen_pseudos_legaux_roi)


#Meme fonction qu'au dessus sauf qu'elle utilise la class Echiquier2
def generer_un_echiquier_realiste_aleatoire2(nbr_de_piece=-1):

    if nbr_de_piece != -1 and (nbr_de_piece < 2 or nbr_de_piece > 32): #on s'assure que le nbr de pièce présent dans l'echiquier soit réaliste
        raise ValueError("Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32")

    if nbr_de_piece == -1:
        nbr_de_piece=randint(2,32)

    masque_pour_empecher_deux_pieces_meme_case=0

    roi_blanc=(1 << randint(0,63))
    masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | roi_blanc #ajout du roi blanc

    alea=randint(0,63)
    while masque_pour_empecher_deux_pieces_meme_case & (1 << alea):
        alea = randint(0, 63)
    roi_noir=(1 << alea)

    masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | roi_noir #ajout du roi noir

    #tjr le meme ordre : piont blanc, tour blanc, cavalier blanc ,fou blanc ,dame blanc, piont noir, tour noir, cavalier noir ,fou noir ,dame noir
    tab_nbrmax_des_pieces=[randint(0,8),randint(0,2),randint(0,2),randint(0,2),randint(0,1),randint(0,8),randint(0,2),randint(0,2),randint(0,2),randint(0,1)]

    #le tableau respecte l'ordre ci dessus
    tab_cpt_nbr_des_pieces=[0,0,0,0,0,0,0,0,0,0] #on initialise les compteurs des masques des pieces

    tab_masques_pieces=[0,0,0,0,0,0,0,0,0,0] # on initialise le tableau du masque des pieces



    som_tableau = 0
    for i in tab_nbrmax_des_pieces:
        som_tableau += i

    depart = 9
    match_depart=[8,2,2,2,1,8,2,2,2,1]
    while som_tableau < nbr_de_piece-2: #on egalise le tableau nbrmax avec le nbr de piece voulu
        alea_egalise = randint(0, depart)
        if alea_egalise==0 and tab_nbrmax_des_pieces[0] < 8:
            tab_nbrmax_des_pieces[0]+=1
        elif alea_egalise==1 and tab_nbrmax_des_pieces[1] < 2:
            tab_nbrmax_des_pieces[1]+=1
        elif alea_egalise==2 and tab_nbrmax_des_pieces[2] < 2:
            tab_nbrmax_des_pieces[2]+=1
        elif alea_egalise==3 and tab_nbrmax_des_pieces[3] < 2:
            tab_nbrmax_des_pieces[3]+=1
        elif alea_egalise==4 and tab_nbrmax_des_pieces[4] < 1:
            tab_nbrmax_des_pieces[4]+=1
        elif alea_egalise==5 and tab_nbrmax_des_pieces[5] < 8:
            tab_nbrmax_des_pieces[5]+=1
        elif alea_egalise==6 and tab_nbrmax_des_pieces[6] < 2:
            tab_nbrmax_des_pieces[6]+=1
        elif alea_egalise==7 and tab_nbrmax_des_pieces[7] < 2:
            tab_nbrmax_des_pieces[7]+=1
        elif alea_egalise == 8 and tab_nbrmax_des_pieces[8] < 2:
            tab_nbrmax_des_pieces[8] += 1
        elif alea_egalise == 9 and tab_nbrmax_des_pieces[9] < 1:
            tab_nbrmax_des_pieces[9] += 1
        else:
            som_tableau -= 1
        som_tableau +=1

        if tab_nbrmax_des_pieces[depart]==match_depart[depart] and depart >1: #optimise la recherche aleatoire en decrementant depart
            depart-=1


    cpt=2 #2 car on a deja placer les deux rois

    depart=9

    nbr_de_zero=0
    for i in tab_nbrmax_des_pieces:
        if i ==0:
            nbr_de_zero+=1


    while cpt < nbr_de_piece :

        if depart == 1:
            alea_piece=0
        else:
            alea_piece = randint(0,depart)
            while tab_cpt_nbr_des_pieces[alea_piece] >= tab_nbrmax_des_pieces[alea_piece]:
                alea_piece = randint(0,depart)


        tab_cpt_nbr_des_pieces[alea_piece]+=1

        if tab_cpt_nbr_des_pieces[depart]==tab_nbrmax_des_pieces[depart] and depart > 1:#on fait decrementer la derniere case du tableau pr optimiser un peu
            depart-=1

        if alea_piece == 0 : # un pion blanc pe pas etre dans la premiere ligne
            alea = randint(8, 63)
        elif alea_piece == 5 :# un pion noir pe pas etre dans la derniere ligne
            alea = randint(0, 55)
        else:
            alea = randint(0, 63)

        while masque_pour_empecher_deux_pieces_meme_case & (1 << alea):
            if alea_piece == 0:
                alea = randint(8, 63)
            elif alea_piece == 5:
                alea = randint(0, 55)
            else:
                alea = randint(0, 63)



        tab_masques_pieces[alea_piece]= tab_masques_pieces[alea_piece] | (1 << alea)
        masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | (1 << alea)

        cpt+=1

    return Echiquier2(tab_masques_pieces[0],tab_masques_pieces[1],tab_masques_pieces[2],tab_masques_pieces[3],tab_masques_pieces[4],roi_blanc,
                     tab_masques_pieces[5], tab_masques_pieces[6], tab_masques_pieces[7], tab_masques_pieces[8],tab_masques_pieces[9], roi_noir,)


def afficher_masque_attaque_dans_console2(echiquier,couleur_enemie):
    masque_attaque_enemie=echiquier.gen_all_attaques_ennemie(couleur_enemie)
    derniere_case = (1 << 63)
    decr = 63
    for i in range(64):
        if i % 8 == 0:
            print('')
        if (echiquier.ech[0]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'P' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'P' + '\x1b[0m', end='')
        elif (echiquier.ech[1]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'T' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'T' + '\x1b[0m', end='')
        elif (echiquier.ech[2]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'C' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'C' + '\x1b[0m', end='')
        elif (echiquier.ech[3]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'F' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'F' + '\x1b[0m', end='')
        elif (echiquier.ech[4]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'D' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'D' + '\x1b[0m', end='')
        elif (echiquier.ech[5]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;41m' + 'R' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'R' + '\x1b[0m', end='')

        elif (echiquier.ech[6]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;43m' + 'p' + '\x1b[0m', end='')
            else:
                print('\x1b[0;30;44m' + 'p' + '\x1b[0m', end='')
        elif (echiquier.ech[7]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;43m' + 't' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 't' + '\x1b[0m', end='')
        elif (echiquier.ech[8]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;43m' + 'c' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'c' + '\x1b[0m', end='')
        elif (echiquier.ech[9]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;43m' + 'f' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'f' + '\x1b[0m', end='')
        elif (echiquier.ech[10]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;43m' + 'd' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'd' + '\x1b[0m', end='')
        elif (echiquier.ech[11]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_enemie==0:
                print('\x1b[6;30;41m' + 'r' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'r' + '\x1b[0m', end='')
        else:
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;42m' + '1' + '\x1b[0m', end='')
            else:
                print('0', end='')
    print('\n')

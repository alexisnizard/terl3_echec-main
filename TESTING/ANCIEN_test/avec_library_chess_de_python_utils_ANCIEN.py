#Transforme l'echiquier en notation FEN
#note : y pas en passant et cpt de coup
def to_fen(echiquier):
    res=""
    cpt_ligne=0
    cpt_case_vide=0

    tab_nom_piece=['P','R','N','B','Q','K','p','r','n','b','q','k']
    for e in range(63,-1,-1):
        etait_ce_vide = 1

        for i in range(12):

            if (1 << e) & echiquier.ech[i]:
                if cpt_case_vide==0:
                    res = res + tab_nom_piece[i]
                else:
                    res = res +str(cpt_case_vide)+ tab_nom_piece[i]
                    cpt_case_vide=0

                etait_ce_vide=0
                break

        if etait_ce_vide:
            cpt_case_vide+=1

        if cpt_ligne==7:
            if cpt_case_vide == 0:
                res=res+"/"
            else:
                res = res +str(cpt_case_vide)+"/"
                cpt_case_vide=0
            cpt_ligne=-1


        cpt_ligne+=1

    res=res[:-1] #on vire le / de la fin

    tab_rock=[" K",'Q','k','q']
    tab_non_rock = [" -", '-', '-', '-']

    if echiquier.ech[12] & 1: #le tour
        res = res + ' w'
    else:
        res = res + ' b'

    for i in range(1,5):# les rocks
        if echiquier.ech[12] & 2**i:
            res = res + tab_rock[i-1]
        else:
            res = res + tab_non_rock[i-1]

    return res


def numero_to_case():
    lettre=['h','g','f','e','d','c','b','a']
    tab=[0 for i in range(64)]

    ind_l=0
    ind_c=0
    for e in range(64):
        if e%8==0 and e!=0:
            ind_l= 0
            ind_c+=1
        tab[e]=lettre[ind_l]+str(ind_c+1)
        ind_l+=1

    return tab

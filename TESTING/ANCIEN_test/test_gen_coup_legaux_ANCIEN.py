from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import *
#Echiquiers interresant :

# 282029027491840 67108864 0 0 33554432 524288 36028797289562144 1073741824 0 18023194602504192 134217728 70368744177664
#1073741824 34603008 134217728 2341871806232657920 0 70368744177664 103083933700 140737488355328 4503599627370496 576460752303423488 0 268435456
#17592186044416 , 34359738368 , 0 , 33554432 , 0 , 8796093022208 0 , 0 , 8858370048 , 0 , 8388608 , 36028797018963968
#0 , 0 , 0 , 0 , 8388608 , 4611686018427387904 34359742464 , 2305843009217888256 , 1048576 , 0 , 0 , 4294967296

#fonctions pour aider les tests:

#permet de gener un echiqueir ou ya x fois echec sur notre roi
def genere_un_echiquier_nbr_echec(nbr_piece,couleur,nbr_de_fois_echec):
    while 1:
        ech_alea = generer_un_echiquier_realiste_aleatoire(nbr_piece)
        cases_piece_immobil_diag, case_dou_provient_echec_diag, nbr_echec_diag, masque_protect_diag = ech_alea.get_pieces_immobilisees_diagonalement(
            couleur)
        cases_piece_immobil_vert_horiz, case_dou_provient_echec_v_h, nbr_echec_v_h, masque_protect_v_h = ech_alea.get_pieces_immobilisees_vert_horiz(
            couleur)

        cav_enemie_echec = ech_alea.echec_cavaliers(couleur)
        pion_enemie_echec = ech_alea.echec_pions(couleur)

        nbr_echec_cav = ech_alea.compte_piece(cav_enemie_echec)
        nbr_echec_pion = ech_alea.compte_piece(pion_enemie_echec)

        nbr_echec = nbr_echec_diag + nbr_echec_v_h + nbr_echec_cav + nbr_echec_pion
        if nbr_echec==nbr_de_fois_echec:
            print("diag : ",nbr_echec_diag)
            print("vert horiz : ", nbr_echec_v_h)
            print("cav : ", nbr_echec_cav)
            print("pion : ", nbr_echec_pion)
            return ech_alea



while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Generer le masque d'attaque (noirs attaquent blancs) sur un echiquier aleatoire de 16 pieces")
    print("2 - Generer le masque d'attaque (blancs attaquent noirs) sur un echiquier aleatoire de 16 pieces")
    print("3 - Test gen_coup_legaux (1) ALEATOIRE")
    print("4 - Test gen_coup_legaux (2) DOUBLE ECHEC")
    print("5 - Test gen_coup_legaux (3) UN SEUL ECHEC")
    print("6 - Test gen_coup_legaux (3) ZERO ECHEC")
    print("7 - Test gen_coup_legaux (4) DOUBLE ECHEC avec echiquier prédéfinit")
    print("8 - Genere 10 000 tests de double echec |un echec |zero echec pour voir si ya erreur de compilation")
    print("9 - Test en passant")
    print("10 - Test rock")
    print("0 - Exit")
    choix=int(input())

    masques = 0

    if choix==0:
        break
    elif choix == 1:
        ech_alea=generer_un_echiquier_realiste_aleatoire(16)
        afficher_masque_attaque_dans_console(ech_alea, 1)
    elif choix == 2:
        ech_alea=generer_un_echiquier_realiste_aleatoire(16)
        afficher_masque_attaque_dans_console(ech_alea, 0)
    elif choix == 3:
        ech=generer_un_echiquier_realiste_aleatoire(16)
        afficher_masque_attaque_dans_console(ech,1)
        masque=0
        print(ech.gen_coup_legaux(0))
        print(ech.coup_legaux(0))
        cpt_i = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque=masque | j[1]
                    #afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        afficher_bitboard_dans_console_couleur(masque)

        print("Les masque 1 par 1 : ")
        tab=["pion","tour","cavalier","fou","dame","roi"]
        cpt=0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de",tab[cpt],"")
                    afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt+=1

    elif choix == 4:
        ech = genere_un_echiquier_nbr_echec(8,0,2)
        print("l'echiquier : ")
        print(ech.ech[0],',',ech.ech[1],',',ech.ech[2],',',ech.ech[3],',',ech.ech[4],',',ech.ech[5]
        ,',',ech.ech[6],',',ech.ech[7],',',ech.ech[8],',',ech.ech[9],',',ech.ech[10],',',ech.ech[11])
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        afficher_bitboard_dans_console_couleur(masque)

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1

    elif choix == 5:
        ech = genere_un_echiquier_nbr_echec(16,0,1)
        print("l'echiquier : ")
        print(ech.ech[0], ech.ech[1], ech.ech[2], ech.ech[3], ech.ech[4], ech.ech[5]
              , ech.ech[6], ech.ech[7], ech.ech[8], ech.ech[9], ech.ech[10], ech.ech[11])
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    print(j[1])
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i += 1

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1

    elif choix == 6:
        ech = genere_un_echiquier_nbr_echec(16,0,0)
        print("l'echiquier : ")
        print(ech.ech[0], ech.ech[1], ech.ech[2], ech.ech[3], ech.ech[4], ech.ech[5]
              , ech.ech[6], ech.ech[7], ech.ech[8], ech.ech[9], ech.ech[10], ech.ech[11])
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i=0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1


    elif choix == 7:
        ech = Echiquier(524288,536870912,0,0,0,1152921504606846976,0,4,10995116277760,0,2147483648,274877906944)
        # Echiquier(282029027491840,67108864,0,0,33554432,524288,36028797289562144,1073741824,0,18023194602504192,134217728,70368744177664)
        print("l'echiquier : ")
        print(ech.ech[0],ech.ech[1],ech.ech[2],ech.ech[3],ech.ech[4],ech.ech[5]
              ,ech.ech[6],ech.ech[7],ech.ech[8],ech.ech[9],ech.ech[10],ech.ech[11])
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i=0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        afficher_bitboard_dans_console_couleur(masque)

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1

    elif choix == 8:
        #ne pas oublié de mettre en commentaire les prints de la fonction genere_un_echiquier_nbr_echec avant de lancé
        for i in range(10000):

            ech = genere_un_echiquier_nbr_echec(16, 0, 0)
            a=ech.gen_coup_legaux(0)

    elif choix == 9:
        # print("Placer le roi allié")
        # roi_all = generer_un_echiquier_personnalise()
        # print("Placer le roi ennemie")
        # roi_ennemie = generer_un_echiquier_personnalise()
        # print("Placer le pion ennemie")
        # pion_ennemie = generer_un_echiquier_personnalise()
        # print("Placer le ou les pions alliés")
        # pion_allié = generer_un_echiquier_personnalise()
        #
        # ech=Echiquier(pion_allié,0,0,0,0,roi_all,pion_ennemie,0,0,0,0,roi_ennemie,31,0,0)
        # ech.ech[13]=ech.get_position(pion_ennemie)[0] + 8
        #
        # print(pion_allié,0,0,0,0,roi_all,pion_ennemie,0,0,0,0,roi_ennemie,31,ech.ech[13],0)

        ech=Echiquier(2199023255552,0,0,0,0,1,4398046511104,0,0,0,0,72057594037927936,31,50,0)
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i=0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        afficher_bitboard_dans_console_couleur(masque)

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1

    elif choix == 10:
        # print("Placer le roi allié")
        # roi_all = generer_un_echiquier_personnalise()
        # print("Placer le roi ennemie")
        # roi_ennemie = generer_un_echiquier_personnalise()
        # print("Placer un pion qui bloquerait le rock")
        # pion_block = generer_un_echiquier_personnalise()
        # print("Placer la tour allié")
        # tour_allie = generer_un_echiquier_personnalise()
        # print("Placer une dame qui mettrait en echec")
        # dame_ennemie = generer_un_echiquier_personnalise()
        # print("Placer un cavalier qui mettrait en echec")
        # cavalier_ennemie = generer_un_echiquier_personnalise()
        # print("Placer un pion qui mettrait en echec")
        # pion_ennemie = generer_un_echiquier_personnalise()
        #
        #
        #
        # ech=Echiquier(pion_block,tour_allie,0,0,0,roi_all,pion_ennemie,0,cavalier_ennemie,0,dame_ennemie,roi_ennemie,3,0,0)
        #
        # print(pion_block,tour_allie,0,0,0,roi_all,pion_ennemie,0,cavalier_ennemie,0,dame_ennemie,roi_ennemie,3,0,0)

        ech=Echiquier(34359738368,1,0,0,0,8,576460752303423488,0,1152921504606846976,0,(1<<58),72057594037927936,3,0,0)
        afficher_masque_attaque_dans_console(ech, 1)
        masque = 0
        print(ech.gen_coup_legaux(0))
        cpt_i=0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt_i<6:
                    masque = masque | j[1]
                    # afficher_bitboard_dans_console_couleur(j[1],j[0])
            cpt_i+=1

        afficher_bitboard_dans_console_couleur(masque)

        print("Les masque 1 par 1 : ")
        tab = ["pion", "tour", "cavalier", "fou", "dame", "roi"]
        cpt = 0
        for i in ech.gen_coup_legaux(0):
            for j in i:
                if cpt<6:
                    print("Masque de", tab[cpt], "")
                    afficher_bitboard_dans_console_couleur(j[1], j[0])
            cpt += 1





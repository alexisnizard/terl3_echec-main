from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import *
from terl3_echec.TESTING.ANCIEN_test.avec_library_chess_de_python_utils_ANCIEN import *
import chess
chiffre_lettre=numero_to_case()

#echiquier qui ont posé des problemes (solved depuis) :
#1B6/8/Rp2b1R1/4P1P1/3P3k/2n4P/B1qPK3/N7 w KQkq le pion bouge pour manger le cavalier car ya qu'un echec
#79165107732992 , 2147483648 , 0 , 0 , 2305843009213693952 , 4 , 4398046511104 , 4503599631564800 , 4611686018427650048 , 65536 , 2199023255552 , 137438953472 , 31 , 0 , 0
#8813298073600 , 9007199254872064 , 70368745226240 , 0 , 0 , 1125899906842624 , 8589934592 , 0 , 144115325514809344 , 1073741824 , 18014398509481984 , 17592186044416 , 31 , 0 , 0
#633318697615872 , 65536 , 549755813888 , 137438953472 , 0 , 4294967296 , 35185450024960 , 274877907200 , 9007199254872064 , 0 , 0 , 2251799813685248 , 31 , 0 , 0
#9029258206773248 , 0 , 0 , 1125902054326272 , 0 , 32768 , 2251834477510656 , 9223372054034644992 , 0 , 536870912 , 0 , 131072 , 31 , 0 , 0
#36030446286536704 , 1207959552 , 562949953422336 , 8796093022208 , 0 , 2 , 2251937252638720 , 2199023255552 , 0 , 72057594306363392 , 0 , 9223372036854775808 , 31 , 0 , 0
#309239742464 , 0 , 4096 , 1073741824 , 512 , 131072 , 68719478784 , 4611686018431582208 , 549755879424 , 9007199254741248 , 0 , 8589934592 , 31 , 0 , 0
#2199561175040 , 0 , 281474977234944 , 1125900041060352 , 0 , 72057594037927936 , 4196352 , 144115188075855872 , 70368744177664 , 137438961664 , 1099511627776 , 36028797018963968 , 31 , 0 , 0
#4503669957986816 , 16777216 , 0 , 72057594037927936 , 576460752303423488 , 137438953472 , 4398046511104 , 0 , 0 , 4195328 , 17179869184 , 1048576
#8796361457664 , 8196 , 34359742464 , 18014398509482240 , 70368744177664 , 35184372088832 , 0 , 2305983746702049280 , 66048 , 0 , 144115188075855872 , 1125899906842624 , 31 , 0 , 0
#524544 , 4294967296 , 145241087982698496 , 18014398509481984 , 549755813888 , 576460752303423488 , 268435456 , 9295429630892703744 , 262144 , 33554448 , 2048 , 4194304 , 31 , 0 , 0
#1689399617388800 , 35184372105216 , 0 , 2252074691592192 , 34359738368 , 2199023255552 , 0 , 140737488355328 , 0 , 32768 , 8192 , 1
#36310272096337920 , 0 , 268435456 , 43980465111040 , 137438953472 , 34359738368 , 2147504128 , 1126449662656512 , 0 , 0 , 2251799813685248 , 9223372036854775808 , 31 , 0 , 0


while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Test to_fen sur echiquier aleatoire")
    print("2 - Voir resultat library python")
    print("3 - Voir chiffre lettre")
    print("4 - comparer avec library python")
    print("5 - Tester X echiquier aleatoire (on est les blancs)")
    print("0 - Exit")
    choix = int(input())

    if choix==0:
        break
    elif choix == 1:

        # ech = Echiquier()
        # print(to_fen(ech))

        ech_alea=generer_un_echiquier_realiste_aleatoire(16)
        afficher_Echiquier_dans_console(ech_alea)
        print(to_fen(ech_alea))
    elif choix == 2:
        # board = chess.Board("rnbqkbnr/pppppppp/8/8/8/8/3PPPPP/4K2R w Kkq - 0 1")
        # coups=list(board.legal_moves)
        # for i in coups:
        #     print(i)
        # h1g1
        # h1f1
        # e1f1
        # e1d1
        # e1g1 <=== en gros ca met juste que le roi peut avancé de deux pr le rock


        # h7h8q <=== promotion pion ca créer 4 chemin
        # h7h8r
        # h7h8b
        # h7h8n
        board = chess.Board("8/8/8/4pP2/8/8/8/8 w - e6 0 1")
        coups=list(board.legal_moves)
        for i in coups:
            print(i)
    elif choix == 3:
        print(chiffre_lettre)
    elif choix==4:
        #K1N4n/1P1Nk3/1rrQP3/8/8/3Pb2P/1P5B/1B6 w KQkq
        #9015995347796480 , 0 , 17592202821632 , 18014398509481984 , 4398046511104 , 144115188075855872 , 562953174646784 , 2 , 536870912 , 0 , 137438953472 , 4096
        #79165107732992 , 2147483648 , 0 , 0 , 2305843009213693952 , 4 , 4398046511104 , 4503599631564800 , 4611686018427650048 , 65536 , 2199023255552 , 137438953472 , 31 , 0 , 0
        ech_alea = Echiquier(1689399617388800 , 35184372105216 , 0 , 2252074691592192 , 34359738368 , 2199023255552 , 0 , 140737488355328 , 0 , 32768 , 8192 , 1 , 31 , 0 , 0)#generer_un_echiquier_realiste_aleatoire(16)
        board = chess.Board(to_fen(ech_alea))
        coups = list(board.legal_moves)
        print("Les coups données par la library python:")
        coup_triee=[]
        for i in coups:
            coup_triee.append(str(i))

        print(sorted(coup_triee))
        print("Taille :", len(coups))
        print("Les coups données par l'implementation:")
        coup=ech_alea.coup_legaux(0)
        coup_rendu=[]
        print("coup:",coup)

        # on ajout les promotions de pions:
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

        # on ajoute les rocks:
        for i in coup[1]:
            coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

        for i in range(3,len(coup)):
            if i not in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]]+chiffre_lettre[coup[i][1]])


        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))
        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0],',',ech_alea.ech[1],',',ech_alea.ech[2],',',ech_alea.ech[3],',',ech_alea.ech[4],',',ech_alea.ech[5]
        ,',',ech_alea.ech[6],',',ech_alea.ech[7],',',ech_alea.ech[8],',',ech_alea.ech[9],',',ech_alea.ech[10],',',ech_alea.ech[11])
        afficher_masque_attaque_dans_console(ech_alea, 1)
    elif choix == 5:

        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite=int(input())

        for k in range(ite):
            if choix==-1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2,32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)


            board = chess.Board(to_fen(ech_alea))
            coups = list(board.legal_moves)
            coup_triee=[]
            for i in coups:
                coup_triee.append(str(i))

            coup=ech_alea.coup_legaux(0)
            coup_rendu=[]

            # on ajout les promotions de pions:
            for i in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

            # on ajoute les rocks:
            for i in coup[1]:
                coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

            for i in range(3,len(coup)):
                if i not in coup[0]:
                    coup_rendu.append(chiffre_lettre[coup[i][0]]+chiffre_lettre[coup[i][1]])

            if sorted(coup_triee) != sorted(coup_rendu):
                print("Erreur trouvé sur cette echiquier : ")
                print("Coup legaux de la library : ")
                print(sorted(coup_triee))
                print("Coup legaux de notre Echiquier :")
                print(sorted(coup_rendu))
                print("Notation fen :")
                print(to_fen(ech_alea))
                print("Notation echiquier :")
                print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',',
                      ech_alea.ech[4], ',', ech_alea.ech[5]
                      , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
                      ech_alea.ech[10], ',', ech_alea.ech[11],',', ech_alea.ech[12],',', ech_alea.ech[13],',', ech_alea.ech[14])
                afficher_masque_attaque_dans_console(ech_alea, 1)
                exit(1)

        print("Tout les coups legaux étaient identiques !")





from terl3_echec.TESTING.TEST_ECHIQUIER.test_gen_pseudos_legaux_utils import \
    generer_un_echiquier_aleatoire_allie_ennemie,afficher_bitboard_dans_console_couleur,gen_pseudos_legaux_tour_bloque_par_allie2,gen_pseudos_legaux_fou_bloque_par_allie2
from terl3_echec.gen_pseudos_legaux_sliders_inclus_premier_allie import *

while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Generer un masque pseudo legal à partir d'un echiquier personnalisé")
    print("0 - Exit")
    choix=int(input())

    masques = 0

    if choix==0:
        break
    elif choix == 1:
        # print(
        #     "Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        # choix = int(input())
        # if choix == -1:
        #     nbr_piece = randint(0, 63)
        # else:
        #     nbr_piece = choix
        # print("L'echiquier est composée de ", nbr_piece, " pieces")
        #
        # print(
        #     "Souhaitez-vous placer la tour dans une case précise ?  Entrez le numero de la case (-1 pour une case aléatoire")
        # choix = int(input())
        # if choix == -1:
        #     case = randint(0, 63)
        # else:
        #     case = choix
        for i in range(10):
            nbr_piece=32
            case = 37

            ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
            print("case: ",case)
            print("Le masque de la tour :")
            afficher_bitboard_dans_console_couleur(masque_tour[case], case)
            print("L'echiquier alliés :")
            print(ech[1])
            afficher_bitboard_dans_console_couleur(4829003993980084800, case)
            print("Le masque de la tour après avoir prit en compte les pièces alliés :")
            afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_tour_bloque_par_allie2(case, 4829003993980084800), case)
            print("L'echiquier ennemies :")
            print(ech[2])
            afficher_bitboard_dans_console_couleur(914336760660755850, case)
            print("Voici les déplacement pseudos legaux de la tour : ")
            print(gen_pseudos_legaux_tour_inc_allie(case, 4829003993980084800, 914336760660755850))
            afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_tour_inc_allie(case, 4829003993980084800, 914336760660755850), case)
            print('=====================\n\n\n')
    elif choix == 2:
        for i in range(10):
            nbr_piece = 32
            case = 37

            ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
            print("case: ", case)
            print("Le masque de la tour :")
            afficher_bitboard_dans_console_couleur(masque_fou[case], case)
            print("L'echiquier alliés :")
            print(ech[1])
            afficher_bitboard_dans_console_couleur(4829003993980084800, case)
            print("Le masque de la tour après avoir prit en compte les pièces alliés :")
            afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_fou_bloque_par_allie2(case, 4829003993980084800),
                                                   case)
            print("L'echiquier ennemies :")
            print(ech[2])
            afficher_bitboard_dans_console_couleur(914336760660755850, case)
            print("Voici les déplacement pseudos legaux de la tour : ")
            print(gen_pseudos_legaux_fou_inc_allie(case, 4829003993980084800, 914336760660755850))
            afficher_bitboard_dans_console_couleur(
                gen_pseudos_legaux_fou_inc_allie(case, 4829003993980084800, 914336760660755850), case)
            print('=====================\n\n\n')

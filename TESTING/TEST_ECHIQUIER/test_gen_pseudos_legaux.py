from terl3_echec.TESTING.TEST_ECHIQUIER.test_gen_pseudos_legaux_utils import *
from terl3_echec.Echiquier import *

#ya moyen de bien reduire le code redondant en mettant les options 2,3,4,5,6,7,8 en une seule option mais c'est que des tests on s'en fiche
while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Generer un masque pseudo legal à partir d'un echiquier personnalisé")
    print("2 - Generer un masque pseudo legal du CAVALIER à partir d'un echiquier random")
    print("3 - Generer un masque pseudo legal du ROI à partir d'un echiquier random")
    print("4 - Generer un masque pseudo legal de la TOUR à partir d'un echiquier random")
    print("5 - Generer un masque pseudo legal du FOU à partir d'un echiquier random")
    print("6 - Generer un masque pseudo legal de la DAME à partir d'un echiquier random")
    print("7 - Generer un masque pseudo legal du PIONT BLANC ATTAQUE à partir d'un echiquier random")
    print("8 - Generer un masque pseudo legal du PIONT NOIR ATTAQUE à partir d'un echiquier random")
    print("9 - Generer un masque pseudo legal du PIONT BLANC AVANCE à partir d'un echiquier random")
    print("10 - Generer un masque pseudo legal du PIONT NOIR AVANCE à partir d'un echiquier random")
    print("0 - Exit")
    choix=int(input())

    masques = 0
    if choix==0:
        break
    elif choix == 1:
        print("Note : pionBlanc = pionBlanc attaque ,pionNoir= pionNoir attaque")
        print("Ecrire le nom de la pièce : pionBlanc | pionNoir | tour | cavalier | fou | dame | roi")
        choix=input()

        print("Souhaitez-vous placer ",choix," dans une case précise ? Entrez le numero de la case (-1 pour une case aléatoire)")
        choix_case = int(input())
        if choix_case == -1:
            case = randint(0, 63)
        else:
            case = choix_case

        print("Créer l'echiquier alliée: ")
        ech_allie=generer_un_echiquier_personnalise()

        ech_ennemie=0
        if choix != "cavalier" and choix != "roi":
            print("Créer l'echiquier ennemie: ")
            ech_ennemie = generer_un_echiquier_personnalise()

        masque = 0
        bloque_allie = 0
        pseudo_legaux = 0

        if choix == "pionBlanc":
            masque=masque_pion_blanc_attaque[case]
            pseudo_legaux=gen_pseudos_legaux_attaque_possible_pion_blanc(case, ech_allie, ech_ennemie)
        elif choix == "pionNoir":
            masque=masque_pion_noir_attaque[case]
            pseudo_legaux = gen_pseudos_legaux_attaque_possible_pion_noir(case, ech_allie, ech_ennemie)
        elif choix == "tour":
            masque=masque_tour[case]
            bloque_allie = gen_pseudos_legaux_tour_bloque_par_allie(case, ech_allie)
            pseudo_legaux = gen_pseudos_legaux_tour(case, ech_allie, ech_ennemie)
        elif choix == "cavalier":
            masque=masque_cavalier[case]
            pseudo_legaux = gen_pseudos_legaux_cavalier(case,ech_allie)
        elif choix == "fou":
            masque=masque_fou[case]
            bloque_allie = gen_pseudos_legaux_fou_bloque_par_allie(case, ech_allie)
            pseudo_legaux = gen_pseudos_legaux_fou(case, ech_allie, ech_ennemie)
        elif choix == "dame":
            masque=masque_dame[case]
            bloque_allie = gen_pseudos_legaux_dame_bloque_par_allie(case, ech_allie)
            pseudo_legaux = gen_pseudos_legaux_dame(case, ech_allie, ech_ennemie)
        elif choix == "roi":
            masque=masque_roi[case]
            pseudo_legaux = gen_pseudos_legaux_roi(case,ech_allie)

        print("Le masque de",choix," :")
        afficher_bitboard_dans_console_couleur(masque, case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech_allie, case)

        if choix =="tour" or choix =="fou" or choix =="dame":
            print("Le masque de",choix,"après avoir prit en compte les pièces alliés :")
            afficher_bitboard_dans_console_couleur(bloque_allie, case)

        if choix != "cavalier" and choix != "roi":
            print("L'echiquier ennemies :")
            afficher_bitboard_dans_console_couleur(ech_ennemie, case)

        print("Voici les déplacement pseudos legaux de : ",choix)
        afficher_bitboard_dans_console_couleur(pseudo_legaux,case)



    elif choix==2:
        print(
            "Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix = int(input())
        if choix == -1:
            nbr_piece = randint(0, 63)
        else:
            nbr_piece = choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print(
            "Souhaitez-vous placer le cavalier dans une case précise ?  Entrez le numero de la case (-1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(0, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque du cavalier :")
        afficher_bitboard_dans_console_couleur(masque_cavalier[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du cavalier : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_cavalier(case, ech[1]),
                                               case)

    elif choix==3:
        print(
            "Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix = int(input())
        if choix == -1:
            nbr_piece = randint(0, 63)
        else:
            nbr_piece = choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print(
            "Souhaitez-vous placer le roi dans une case précise ?  Entrez le numero de la case (-1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(0, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque du roi :")
        afficher_bitboard_dans_console_couleur(masque_roi[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du roi : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_roi(case, ech[1]),
                                               case)


    elif choix==4:
        # print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        # choix=int(input())
        # if choix == -1:
        #     nbr_piece=randint(0,63)
        # else :
        #     nbr_piece=choix
        # print("L'echiquier est composée de ", nbr_piece, " pieces")
        #
        # print("Souhaitez-vous placer la tour dans une case précise ?  Entrez le numero de la case (-1 pour une case aléatoire")
        # choix = int(input())
        # if choix == -1:
        #     case = randint(0, 63)
        # else:
        #     case = choix
        for i in range(40):
            nbr_piece=32
            case= randint(0, 63)
            ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
            print("Le masque de la tour :")
            afficher_bitboard_dans_console_couleur(masque_tour[case], case)
            print("L'echiquier alliés :")
            afficher_bitboard_dans_console_couleur(ech[1], case)
            print("Le masque de la tour après avoir prit en compte les pièces alliés :")
            afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_tour_bloque_par_allie(case,ech[1]),case)
            print("L'echiquier ennemies :")
            afficher_bitboard_dans_console_couleur(ech[2], case)
            print("Voici les déplacement pseudos legaux de la tour : ")
            print(gen_pseudos_legaux_tour(case, ech[1], ech[2]))
            afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_tour(case, ech[1], ech[2]),case)
            print('=====================\n\n\n')


    elif choix==5:
        print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix=int(input())
        if choix == -1:
            nbr_piece=randint(0,63)
        else :
            nbr_piece=choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print("Souhaitez-vous placer le fou dans une case précise ?  Entrez le numero de la case (-1 pour une case aléatoire)")
        choix = int(input())
        if choix == -1:
            case = randint(0, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque du fou :")
        afficher_bitboard_dans_console_couleur(masque_fou[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("Le masque du fou après avoir prit en compte les pièces alliés :")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_fou_bloque_par_allie(case, ech[1]), case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du fou : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_fou(case, ech[1], ech[2]),case)


    elif choix==6:
        print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix=int(input())
        if choix == -1:
            nbr_piece=randint(0,63)
        else :
            nbr_piece=choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print("Souhaitez-vous placer la dame dans une case précise ? Entrez la case ou -1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(0, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque de la dame :")
        afficher_bitboard_dans_console_couleur(masque_dame[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("Le masque de la dame après avoir prit en compte les pièces alliés :")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_dame_bloque_par_allie(case, ech[1]), case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux de la dame : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_dame(case, ech[1], ech[2]),case)

    elif choix==7:
        print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix=int(input())
        if choix == -1:
            nbr_piece=randint(0,63)
        else :
            nbr_piece=choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print("Souhaitez-vous placer le pion Blanc dans une case précise ? Entrez la case ou -1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(8, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque d'attaque du pion Blanc :")
        afficher_bitboard_dans_console_couleur(masque_pion_blanc_attaque[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du pion Blanc : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_attaque_possible_pion_blanc(case, ech[1], ech[2]), case)

    elif choix==8:
        print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix=int(input())
        if choix == -1:
            nbr_piece=randint(0,63)
        else :
            nbr_piece=choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print("Souhaitez-vous placer le pion Noir dans une case précise ? Entrez la case ou -1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(0, 55)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("Le masque d'attaque du pion Noir :")
        afficher_bitboard_dans_console_couleur(masque_pion_noir_attaque[case], case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du pion Noir : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_attaque_possible_pion_noir(case, ech[1], ech[2]), case)
    elif choix==9:
        print(
            "Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix = int(input())
        if choix == -1:
            nbr_piece = randint(0, 63)
        else:
            nbr_piece = choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print(
            "Souhaitez-vous placer le pion Blanc dans une case précise ? Entrez la case ou -1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(8, 63)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du pion Blanc : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_avance_possible_pion_blanc(case, ech[1], ech[2]), case)

    elif choix==10:
        print("Souhaitez-vous un nombre précis de pièce dans l'echiquier ? Entrez le nombre (-1 pour un nombre aléatoire)")
        choix=int(input())
        if choix == -1:
            nbr_piece=randint(0,63)
        else :
            nbr_piece=choix
        print("L'echiquier est composée de ", nbr_piece, " pieces")

        print("Souhaitez-vous placer le pion Noir dans une case précise ? Entrez la case ou -1 pour une case aléatoire")
        choix = int(input())
        if choix == -1:
            case = randint(0, 55)
        else:
            case = choix
        ech = generer_un_echiquier_aleatoire_allie_ennemie(nbr_piece, case)
        print("L'echiquier alliés :")
        afficher_bitboard_dans_console_couleur(ech[1], case)
        print("L'echiquier ennemies :")
        afficher_bitboard_dans_console_couleur(ech[2], case)
        print("Voici les déplacement pseudos legaux du pion Noir : ")
        afficher_bitboard_dans_console_couleur(gen_pseudos_legaux_avance_possible_pion_noir(case, ech[1], ech[2]), case)

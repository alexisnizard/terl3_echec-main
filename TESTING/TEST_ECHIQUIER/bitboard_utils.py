from random import randint
from terl3_echec.Echiquier import *

#permet d'afficher le bitboard dans la console
def afficher_bitboard_dans_console(bitboard):
    bitboard=bitboard #les opérations bitwise et bitshift ne marchent pas si un type est numpy et l'autre int, caster en int regle le problème
    derniere_case=(1 << 63)
    for i in range(64):
        if i % 8 == 0:
            print('')
        if bitboard & (derniere_case >> i) :
            print(1, end='')
        else:
            print(0, end='')
    print('\n')

# permet d'afficher le bitboard dans la console en coloriant les 1 en vert et la pièce passé en parametre en rouge
def afficher_bitboard_dans_console_couleur(bitboard, case=-1):
    bitboard = bitboard
    derniere_case = (1 << 63)
    decr=63
    for i in range(64):
        if i % 8 == 0:
            print('')
        if bitboard & (derniere_case >> i):
            if decr == case:
                print('\x1b[6;30;41m' + '1' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;42m' + '1' + '\x1b[0m', end='')
        else:
            if decr == case:
                print('\x1b[6;30;41m' + '0' + '\x1b[0m', end='')
            else:
                print('0', end='')
        decr-=1

    print('\n')

#permet d'afficher le bitboard dans la console en coloriant les 1 en vert et en affichant le premier numero de chaque colonne
def afficher_bitboard_dans_console_couleur_et_numero(bitboard):
    bitboard=bitboard
    derniere_case = (1 << 63)
    decr=48
    for i in range(64):
        if i % 8 == 0:
            if i!=0:
                print(' <= ',i+decr,'')
                decr-=16
            else:
                print('')

        if bitboard & (derniere_case >> i) :
            print('\x1b[6;30;42m' + '1'+'\x1b[0m', end='')
        else:
            print('0', end='')
        if i==63:
            print(' <= ',0,'')
    print('\n')

#permet de generer un echiquier aléatoire avec n pieces à l'interieur
def generer_un_echiquier_aleatoire(n):
    masque=0

    for i in range(n):
        alea=randint(0,63)

        while masque | (1 << alea) == masque:
            alea = randint(0, 63)

        masque= masque | (1 << alea)

    return masque


#permet de generer un echiquier aléatoire avec n pieces à l'interieur ,suivit de l'echiquier allié puis de l'echiquier ennemie
#le parametre c indique une case où on ne veut pas qu'il y ait de piece
def generer_un_echiquier_aleatoire_allie_ennemie(n,c):
    masque=0
    allie=0
    ennemie=0

    sep=randint(0,n)

    for i in range(sep):
        alea=randint(0,63)
        if alea==c:
            i-=1
            continue

        while masque & (1 << alea):
            alea = randint(0, 63)

        masque= masque | (1 << alea)
        allie = allie | (1 << alea)

    for i in range(sep,n):
        alea = randint(0, 63)
        if alea==c:
            i-=1
            continue

        while masque & (1 << alea):
            alea = randint(0, 63)

        masque = masque | (1 << alea)
        ennemie = ennemie | (1<< alea)

    return [masque,allie,ennemie]

#permet de generer un echiquier personnalisé
def generer_un_echiquier_personnalise():

    echiquier=0
    while(1):
        print("Voici votre echiquier :")
        afficher_bitboard_dans_console_couleur_et_numero(echiquier)
        print("Dans quel case placer la pièce ? Ecrire -1 pour terminer :")
        case = int(input())
        if case == -1:
            break
        echiquier=echiquier | (1 << case)

    print("Voici son bitboard : ",echiquier)
    return echiquier



#Le type de retour est une instance de la classe Echiquier ! (avant on renvoyait simplement un bitboard)
def generer_un_echiquier_realiste_aleatoire(nbr_de_piece=-1):

    if nbr_de_piece != -1 and (nbr_de_piece < 2 or nbr_de_piece > 32): #on s'assure que le nbr de pièce présent dans l'echiquier soit réaliste
        raise ValueError("Trop ou pas assez de pièce, pour que l\'echiquier soit réaliste il faut obligatoirement 2 <= nbr de pièces présente <= 32")

    if nbr_de_piece == -1:
        nbr_de_piece=randint(2,32)

    masque_pour_empecher_deux_pieces_meme_case=0

    alea_premier_roi=randint(0,63)
    roi_blanc=(1 << alea_premier_roi)
    masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | roi_blanc #ajout du roi blanc

    alea=randint(0,63)
    while masque_pour_empecher_deux_pieces_meme_case & (1 << alea) or masque_roi[alea_premier_roi] & (1<<alea): #empecher deux mm case / roi cote a coté
        alea = randint(0, 63)




    roi_noir=(1 << alea)

    masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | roi_noir #ajout du roi noir

    #tjr le meme ordre : piont blanc, tour blanc, cavalier blanc ,fou blanc ,dame blanc, piont noir, tour noir, cavalier noir ,fou noir ,dame noir
    tab_nbrmax_des_pieces=[randint(0,8),randint(0,2),randint(0,2),randint(0,2),randint(0,1),randint(0,8),randint(0,2),randint(0,2),randint(0,2),randint(0,1)]

    #le tableau respecte l'ordre ci dessus
    tab_cpt_nbr_des_pieces=[0,0,0,0,0,0,0,0,0,0] #on initialise les compteurs des masques des pieces

    tab_masques_pieces=[0,0,0,0,0,0,0,0,0,0] # on initialise le tableau du masque des pieces



    som_tableau = 0
    for i in tab_nbrmax_des_pieces:
        som_tableau += i

    depart = 9
    match_depart=[8,2,2,2,1,8,2,2,2,1]
    while som_tableau < nbr_de_piece-2: #on egalise le tableau nbrmax avec le nbr de piece voulu
        alea_egalise = randint(0, depart)
        if alea_egalise==0 and tab_nbrmax_des_pieces[0] < 8:
            tab_nbrmax_des_pieces[0]+=1
        elif alea_egalise==1 and tab_nbrmax_des_pieces[1] < 2:
            tab_nbrmax_des_pieces[1]+=1
        elif alea_egalise==2 and tab_nbrmax_des_pieces[2] < 2:
            tab_nbrmax_des_pieces[2]+=1
        elif alea_egalise==3 and tab_nbrmax_des_pieces[3] < 2:
            tab_nbrmax_des_pieces[3]+=1
        elif alea_egalise==4 and tab_nbrmax_des_pieces[4] < 1:
            tab_nbrmax_des_pieces[4]+=1
        elif alea_egalise==5 and tab_nbrmax_des_pieces[5] < 8:
            tab_nbrmax_des_pieces[5]+=1
        elif alea_egalise==6 and tab_nbrmax_des_pieces[6] < 2:
            tab_nbrmax_des_pieces[6]+=1
        elif alea_egalise==7 and tab_nbrmax_des_pieces[7] < 2:
            tab_nbrmax_des_pieces[7]+=1
        elif alea_egalise == 8 and tab_nbrmax_des_pieces[8] < 2:
            tab_nbrmax_des_pieces[8] += 1
        elif alea_egalise == 9 and tab_nbrmax_des_pieces[9] < 1:
            tab_nbrmax_des_pieces[9] += 1
        else:
            som_tableau -= 1
        som_tableau +=1

        if tab_nbrmax_des_pieces[depart]==match_depart[depart] and depart >1: #optimise la recherche aleatoire en decrementant depart
            depart-=1


    cpt=2 #2 car on a deja placer les deux rois

    depart=9

    nbr_de_zero=0
    for i in tab_nbrmax_des_pieces:
        if i ==0:
            nbr_de_zero+=1


    while cpt < nbr_de_piece :

        if depart == 1:
            alea_piece=0
        else:
            alea_piece = randint(0,depart)
            while tab_cpt_nbr_des_pieces[alea_piece] >= tab_nbrmax_des_pieces[alea_piece]:
                alea_piece = randint(0,depart)


        tab_cpt_nbr_des_pieces[alea_piece]+=1

        if tab_cpt_nbr_des_pieces[depart]==tab_nbrmax_des_pieces[depart] and depart > 1:#on fait decrementer la derniere case du tableau pr optimiser un peu
            depart-=1

        if alea_piece == 0 : # un pion blanc pe pas etre dans la premiere ligne et derniere ligne
            alea = randint(8, 55)
        elif alea_piece == 5 :# un pion noir pe pas etre dans la derniere ligne et premiere ligne
            alea = randint(8, 55)
        else:
            alea = randint(0, 63)

        while masque_pour_empecher_deux_pieces_meme_case & (1 << alea):
            if alea_piece == 0:
                alea = randint(8, 55)
            elif alea_piece == 5:
                alea = randint(8, 55)
            else:
                alea = randint(0, 63)



        tab_masques_pieces[alea_piece]= tab_masques_pieces[alea_piece] | (1 << alea)
        masque_pour_empecher_deux_pieces_meme_case = masque_pour_empecher_deux_pieces_meme_case | (1 << alea)

        cpt+=1

    return Echiquier(tab_masques_pieces[0],tab_masques_pieces[1],tab_masques_pieces[2],tab_masques_pieces[3],tab_masques_pieces[4],roi_blanc,
                     tab_masques_pieces[5], tab_masques_pieces[6], tab_masques_pieces[7], tab_masques_pieces[8],tab_masques_pieces[9], roi_noir,)


#on passe en parametre un echiquier de la classe Echiquier ,pas un bitboard !
#Obtenir un bon resultat dans la console : utiliser la police High Tower Text ,font size 24 avec line spacing 1
def afficher_Echiquier_dans_console_unicode(echiquier):

    derniere_case = (1 << 63)
    for i in range(64):
        if i % 8  == 0:
            print('')
        if (echiquier.ech[0]) & (derniere_case >> i):
            print(u'\u265F',' ', end='')
        elif (echiquier.ech[1]) & (derniere_case >> i):
            print(u'\u265C',' ', end='')
        elif (echiquier.ech[2]) & (derniere_case >> i):
            print(u'\u265E',' ', end='')
        elif (echiquier.ech[3]) & (derniere_case >> i):
            print(u'\u265D',' ', end='')
        elif (echiquier.ech[4]) & (derniere_case >> i):
            print(u'\u265B',' ', end='')
        elif (echiquier.ech[5]) & (derniere_case >> i):
            print(u'\u265A',' ', end='')
        elif (echiquier.ech[6]) & (derniere_case >> i):
            print(u'\u2659', ' ', end='')
        elif (echiquier.ech[7]) & (derniere_case >> i):
            print(u'\u2656',' ', end='')
        elif (echiquier.ech[8]) & (derniere_case >> i):
            print(u'\u2658',' ', end='')
        elif (echiquier.ech[9]) & (derniere_case >> i):
            print(u'\u2657',' ', end='')
        elif (echiquier.ech[10]) & (derniere_case >> i):
            print(u'\u2655',' ', end='')
        elif (echiquier.ech[11]) & (derniere_case >> i):
            print(u'\u2654',' ', end='')
        else:
            print('.',u"\u2000",' ', end='')
    print('\n')



#les blanc en majuscules (P T C F R D) ,les noir en minuscule (p t c f r d)
def afficher_Echiquier_dans_console(echiquier):

    derniere_case = (1 << 63)
    for i in range(64):
        if i % 8  == 0:
            print('')
        if (echiquier.ech[0]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'P' + '\x1b[0m', end='')
        elif (echiquier.ech[1]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'T'+ '\x1b[0m', end='')
        elif (echiquier.ech[2]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'C'+ '\x1b[0m', end='')
        elif (echiquier.ech[3]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'F'+ '\x1b[0m', end='')
        elif (echiquier.ech[4]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'D'+ '\x1b[0m', end='')
        elif (echiquier.ech[5]) & (derniere_case >> i):
            print('\x1b[6;30;46m' + 'R'+ '\x1b[0m', end='')

        elif (echiquier.ech[6]) & (derniere_case >> i):
            print('\x1b[0;30;44m' + 'p'+ '\x1b[0m', end='')
        elif (echiquier.ech[7]) & (derniere_case >> i):
            print('\x1b[6;30;44m' + 't'+ '\x1b[0m', end='')
        elif (echiquier.ech[8]) & (derniere_case >> i):
            print('\x1b[6;30;44m' + 'c'+ '\x1b[0m', end='')
        elif (echiquier.ech[9]) & (derniere_case >> i):
            print('\x1b[6;30;44m' + 'f'+ '\x1b[0m', end='')
        elif (echiquier.ech[10]) & (derniere_case >> i):
            print('\x1b[6;30;44m' + 'd'+ '\x1b[0m', end='')
        elif (echiquier.ech[11]) & (derniere_case >> i):
            print('\x1b[6;30;44m' + 'r' + '\x1b[0m', end='')
        else:
            print('0', end='')
    print('\n')


def afficher_masque_attaque_dans_console(echiquier,couleur_ennemie):
    masque_attaque_enemie=echiquier.gen_attaque_global(couleur_ennemie)
    derniere_case = (1 << 63)
    decr = 63
    for i in range(64):
        if i % 8 == 0:
            print('')
        if (echiquier.ech[0]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):# si ya un 1 ds cette case on la transforme en jaune
                print('\x1b[6;30;43m' + 'P' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'P' + '\x1b[0m', end='')
        elif (echiquier.ech[1]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'T' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'T' + '\x1b[0m', end='')
        elif (echiquier.ech[2]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'C' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'C' + '\x1b[0m', end='')
        elif (echiquier.ech[3]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'F' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'F' + '\x1b[0m', end='')
        elif (echiquier.ech[4]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;43m' + 'D' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'D' + '\x1b[0m', end='')
        elif (echiquier.ech[5]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i):# si ya un 1 ds cette case on la transforme en rouge
                print('\x1b[6;30;41m' + 'R' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;46m' + 'R' + '\x1b[0m', end='')

        elif (echiquier.ech[6]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;43m' + 'p' + '\x1b[0m', end='')
            else:
                print('\x1b[0;30;44m' + 'p' + '\x1b[0m', end='')
        elif (echiquier.ech[7]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;43m' + 't' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 't' + '\x1b[0m', end='')
        elif (echiquier.ech[8]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;43m' + 'c' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'c' + '\x1b[0m', end='')
        elif (echiquier.ech[9]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;43m' + 'f' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'f' + '\x1b[0m', end='')
        elif (echiquier.ech[10]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;43m' + 'd' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'd' + '\x1b[0m', end='')
        elif (echiquier.ech[11]) & (derniere_case >> i):
            if masque_attaque_enemie & (derniere_case >> i) and couleur_ennemie==1:
                print('\x1b[6;30;41m' + 'r' + '\x1b[0m', end='')
            else:
                print('\x1b[6;30;44m' + 'r' + '\x1b[0m', end='')
        else:
            if masque_attaque_enemie & (derniere_case >> i):
                print('\x1b[6;30;42m' + '1' + '\x1b[0m', end='')
            else:
                print('0', end='')
    print('\n')

#afficher_bitboard_dans_console_couleur(65280 | (1 <<58))
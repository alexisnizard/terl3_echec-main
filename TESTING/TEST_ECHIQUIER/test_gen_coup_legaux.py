import time

from terl3_echec.TESTING.TEST_ECHIQUIER.avec_library_chess_de_python_utils import *
from terl3_echec.TESTING.TEST_ECHIQUIER.bitboard_utils import *
import chess

chiffre_lettre=numero_to_case()

while(1):
    print("Ne pas oublier de mettre en commentaire l'ajout de rock à cause du soucis FEN ")
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - --")
    print("2 - --")
    print("3 - OBSOLETE neural_network_chess-1.4 brute force debug")
    print("4 - OBSOLETE Debug brute force echiq blanc")
    print("5 - OBSOLETE brute force sur X echiq alea (on est les blancs)")
    print("6 - OBSOLETE brute force sur X echiq alea (on est les noirs)")
    print("7 - OBSOLETE Debug brute force echiq noir")
    print("8 - OBSOLETE comparer speed")
    print("9 - Debug gen coup legaux 2.0 (on est les blancs)")
    print("10 - brute force sur X echiq alea (on est les blancs)")
    print("11 - brute force sur X echiq alea (on est les noir)")
    print("12 - Debug gen coup legaux 2.0 (on est les noirs)")
    print("0 - Exit")
    choix = int(input())

    if choix==0:
        break
    elif choix==3:
        #ech_alea = generer_un_echiquier_realiste_aleatoire(16)
        ech_alea= Echiquier(281474976711680 , 524288 , 140737488355584 , 0 , 0 , 2251799813685248 , 18015498025336832 , 2155872256 , 32 , 35184372088832 , 8796093022208 , 2199023255552 , 31 , 0 , 0)

        afficher_bitboard_dans_console_couleur(1423452654920714267201)
        afficher_bitboard_dans_console_couleur(290482175965396992)
        afficher_bitboard_dans_console_couleur(2251799813685248)
        print("puis")
        afficher_bitboard_dans_console_couleur(2256197860196352)
        afficher_bitboard_dans_console_couleur(2269391999729664)
        afficher_bitboard_dans_console_couleur(3377699720527872)
        afficher_bitboard_dans_console_couleur(6755399441055744)


        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',', ech_alea.ech[4],
              ',', ech_alea.ech[5]
              , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
              ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
                      ech_alea.ech[14])
        afficher_masque_attaque_dans_console(ech_alea, 1)

        coup = ech_alea.gen_coup_legaux(0)


        print("liste coup : ",coup)
        coup_rendu = []
        # on ajout les promotions de pions:
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

        # on ajoute les rocks:
        for i in coup[1]:
            coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

        for i in range(3,len(coup)):
            if i not in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]])

        print("Les coups données par la library python:")
        board = chess.Board(to_fen(ech_alea))
        coups = list(board.legal_moves)
        coup_triee = []
        for i in coups:
            coup_triee.append(str(i))
        print(sorted(coup_triee))
        print("Taille :", len(coups))


        print("Les coups données par l'implementation:")
        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))

    elif choix == 4:
        afficher_bitboard_dans_console_couleur(134805256)
        ech_alea = Echiquier(8796093022208 , 16777216 , 4611686018427387904 , 72057594037932032 , 18014398509481984 , 16384 , 9011614616387584 , 2048 , 0 , 1073741824 , 4 , 131072 , 31 , 0 , 0)#generer_un_echiquier_realiste_aleatoire(16)

        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',', ech_alea.ech[4],
              ',', ech_alea.ech[5]
              , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
              ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
              ech_alea.ech[14])
        afficher_masque_attaque_dans_console(ech_alea, 1)

        coup = ech_alea.gen_coup_legaux(0)

        print("liste coup : ", coup)
        coup_rendu = []
        # on ajout les promotions de pions:
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

        # on ajoute les rocks:
        for i in coup[1]:
            coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

        for i in range(3, len(coup)):
            if i not in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]])

        print("Les coups données par la library python:")

        board = chess.Board(to_fen(ech_alea))#[:-4] pr rock
        coups = list(board.legal_moves)
        coup_triee = []
        for i in coups:
            coup_triee.append(str(i))
        print(sorted(coup_triee))
        print("Taille :", len(coups))

        print("Les coups données par l'implementation:")
        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))

    elif choix == 5:

        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite = int(input())

        for k in range(ite):
            if choix == -1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2, 32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)

            coup = ech_alea.gen_coup_legaux(0)

            coup_rendu = []
            # on ajout les promotions de pions:
            for i in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

            # on ajoute les rocks:
            for i in coup[1]:
                coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

            for i in range(3, len(coup)):
                if i not in coup[0]:
                    coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]])

            if not coup[1]:#si ya pas de rock on met pas les rocks et cest reglé ,bon ya tjr le cas du double rock
                board = chess.Board(to_fen(ech_alea)[:-4])
            else:
                board = chess.Board(to_fen(ech_alea))
            coups = list(board.legal_moves)
            coup_triee = []
            for i in coups:
                coup_triee.append(str(i))

            if sorted(coup_triee) != sorted(coup_rendu):
                print("Erreur trouvé sur cette echiquier : ")
                print("Coup legaux de la library : ")
                print(sorted(coup_triee))
                print("Coup legaux de notre Echiquier :")
                print(sorted(coup_rendu))
                print("Notation fen :")
                print(to_fen(ech_alea))
                print("Notation echiquier :")
                print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',',
                      ech_alea.ech[4], ',', ech_alea.ech[5]
                      , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
                      ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
                      ech_alea.ech[14])
                afficher_masque_attaque_dans_console(ech_alea, 1)
                exit(1)

        print("Tout les coups legaux étaient identiques\n")
    elif choix == 6:

        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite = int(input())

        for k in range(ite):
            if choix == -1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2, 32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)

            ech_alea.ech[12] = 30
            coup = ech_alea.gen_coup_legaux(1)

            coup_rendu = []
            # on ajout les promotions de pions:
            for i in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

            # on ajoute les rocks:
            for i in coup[1]:
                coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

            for i in range(3, len(coup)):
                if i not in coup[0]:
                    coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]])

            if not coup[1]:#si ya pas de rock on met pas les rocks et cest reglé ,bon ya tjr le cas du double rock
                board = chess.Board(to_fen(ech_alea)[:-4])
            else:
                board = chess.Board(to_fen(ech_alea))
            coups = list(board.legal_moves)
            coup_triee = []
            for i in coups:
                coup_triee.append(str(i))

            if sorted(coup_triee) != sorted(coup_rendu):
                print("Erreur trouvé sur cette echiquier : ")
                print("Coup legaux de la library : ")
                print(sorted(coup_triee))
                print("Coup legaux de notre Echiquier :")
                print(sorted(coup_rendu))
                print("Notation fen :")
                print(to_fen(ech_alea))
                print("Notation echiquier :")
                print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',',
                      ech_alea.ech[4], ',', ech_alea.ech[5]
                      , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
                      ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
                      ech_alea.ech[14])
                afficher_masque_attaque_dans_console(ech_alea, 0)
                exit(1)

        print("Tout les coups legaux étaient identiques\n")
    elif choix == 7:
        ech_alea =Echiquier(1239098327040 , 2251799813685248 , 1152921504606847008 , 0 , 0 , 562949953421312 , 1125900043159552 , 0 , 0 , 4611686018427388160 , 72057594037927936 , 128 , 30 , 0 , 0)#generer_un_echiquier_realiste_aleatoire(16)
        ech_alea.ech[12]=30

        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',', ech_alea.ech[4],
              ',', ech_alea.ech[5]
              , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
              ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
              ech_alea.ech[14])
        afficher_masque_attaque_dans_console(ech_alea, 0)

        coup = ech_alea.gen_coup_legaux(1)

        print("liste coup : ", coup)
        coup_rendu = []
        # on ajout les promotions de pions:
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'b')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'n')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'q')
            coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]] + 'r')

        # on ajoute les rocks:
        for i in coup[1]:
            coup_rendu.append(chiffre_lettre[i[0]] + chiffre_lettre[i[1]])

        for i in range(3, len(coup)):
            if i not in coup[0]:
                coup_rendu.append(chiffre_lettre[coup[i][0]] + chiffre_lettre[coup[i][1]])

        print("Les coups données par la library python:")

        board = chess.Board(to_fen(ech_alea))#[:-4] pr rock
        coups = list(board.legal_moves)
        coup_triee = []
        for i in coups:
            coup_triee.append(str(i))
        print(sorted(coup_triee))
        print("Taille :", len(coups))

        print("Les coups données par l'implementation:")
        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))

    elif choix==8:

        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite = int(input())

        tableau_dechiquier=[]

        for k in range(ite):
            if choix == -1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2, 32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)

            tableau_dechiquier.append(ech_alea)

        tableau_dechiquier_fen = []

        for i in tableau_dechiquier:
            tableau_dechiquier_fen.append(chess.Board(to_fen(i)))

        #les deux sets : tableau_dechiquier_fen ,tableau_dechiquier

        start = time.time()
        for i in tableau_dechiquier_fen:
            list(i.legal_moves)
        end = time.time()
        print("Temps mis avec la library :", end - start)

        start = time.time()
        for i in tableau_dechiquier:
            i.gen_coup_legaux(0)
        end = time.time()
        print("Temps mis avec Echiquier :", end - start)

    elif choix == 9:
        #36046393567084544 , 513 , 1073741824 , 17179869184 , 2048 , 8 , 0 , 11258999068426240 , 9223372036854775808 , 576460752303489024 , 0 , 8589934592 , 31 , 0 , 0
        #ech_alea = Echiquier(1409574997346304 , 576460752303423616 , 0 , 1152921504607109120 , 0 , 8 , 0 , 72057594037927936 , 70368777732096 , 0 , 0 , 268435456 , 31 , 0 , 0)#generer_un_echiquier_realiste_aleatoire(16)
        ech_alea=Echiquier(0 , 1125899906842752 , 72057594042122240 , 2594073385365405696 , 0 , 8 , 2251800183078912 , 4611686018427387904 , 134217728 , 0 , 0 , 131072 , 31 , 0 , 0)
        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',', ech_alea.ech[4],
              ',', ech_alea.ech[5]
              , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
              ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
              ech_alea.ech[14])
        afficher_masque_attaque_dans_console(ech_alea, 1)

        coup = ech_alea.gen_coup_legaux(0)

        print("liste coup : ", coup)

        coup_rendu = []

        #on append les coups legaux
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

        # on append les promotions de pions
        for i in coup[1]:
            if i[4] == 4 or i[4] == 10:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
            elif i[4] == 1 or i[4] == 7:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
            elif i[4] == 2 or i[4] == 8:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
            else:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')


        print("Les coups données par la library python:")

        board = chess.Board(to_fen(ech_alea))  # [:-4] pr rock
        coups = list(board.legal_moves)
        coup_triee = []
        for i in coups:
            coup_triee.append(str(i))
        print(sorted(coup_triee))
        print("Taille :", len(coups))

        print("Les coups données par l'implementation:")
        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))
    elif choix==10:
        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite = int(input())

        for k in range(ite):
            if choix == -1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2, 32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)

            coup = ech_alea.gen_coup_legaux(0)

            coup_rendu = []

            # on append les coups legaux
            for i in coup[0]:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

            # on append les promotions de pions
            for i in coup[1]:
                if i[4] == 4 or i[4] == 10:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
                elif i[4] == 1 or i[4] == 7:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
                elif i[4] == 2 or i[4] == 8:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
                else:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')


            #on desactive les rocks
            board = chess.Board(to_fen(ech_alea))#[:-4]
            coups = list(board.legal_moves)
            coup_triee = []
            for i in coups:
                coup_triee.append(str(i))

            if sorted(coup_triee) != sorted(coup_rendu):
                print("Erreur trouvé sur cette echiquier : ")
                print("Coup legaux de la library : ")
                print(sorted(coup_triee))
                print("Coup legaux de notre Echiquier :")
                print(sorted(coup_rendu))
                print("Notation fen :")
                print(to_fen(ech_alea))
                print("Notation echiquier :")
                print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',',
                      ech_alea.ech[4], ',', ech_alea.ech[5]
                      , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
                      ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
                      ech_alea.ech[14])
                afficher_masque_attaque_dans_console(ech_alea, 1)
                exit(1)

        print("Tout les coups legaux étaient identiques\n")
    elif choix==11:
        print("Combien de pièces dans chaque echiquier ? -1 pour aleatoire (entre 2 et 32) à chaque fois ")
        choix = int(input())
        print("Combien d'iteration ?")
        ite = int(input())

        for k in range(ite):
            if choix == -1:
                ech_alea = generer_un_echiquier_realiste_aleatoire(randint(2, 32))
            else:
                ech_alea = generer_un_echiquier_realiste_aleatoire(choix)

            ech_alea.ech[12] = 30
            coup = ech_alea.gen_coup_legaux(1)

            coup_rendu = []

            # on append les coups legaux
            for i in coup[0]:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

            # on append les promotions de pions
            for i in coup[1]:
                if i[4] == 4 or i[4] == 10:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
                elif i[4] == 1 or i[4] == 7:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
                elif i[4] == 2 or i[4] == 8:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
                else:
                    coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')


            #on desactive les rocks
            board = chess.Board(to_fen(ech_alea))#[:-4]
            coups = list(board.legal_moves)
            coup_triee = []
            for i in coups:
                coup_triee.append(str(i))

            if sorted(coup_triee) != sorted(coup_rendu):
                print("Erreur trouvé sur cette echiquier : ")
                print("Coup legaux de la library : ")
                print(sorted(coup_triee))
                print("Coup legaux de notre Echiquier :")
                print(sorted(coup_rendu))
                print("Notation fen :")
                print(to_fen(ech_alea))
                print("Notation echiquier :")
                print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',',
                      ech_alea.ech[4], ',', ech_alea.ech[5]
                      , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
                      ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
                      ech_alea.ech[14])
                afficher_masque_attaque_dans_console(ech_alea, 0)
                exit(1)


        print("Tout les coups legaux étaient identiques\n")
    elif choix == 12:
        #36046393567084544 , 513 , 1073741824 , 17179869184 , 2048 , 8 , 0 , 11258999068426240 , 9223372036854775808 , 576460752303489024 , 0 , 8589934592 , 31 , 0 , 0
        #ech_alea = Echiquier(1409574997346304 , 576460752303423616 , 0 , 1152921504607109120 , 0 , 8 , 0 , 72057594037927936 , 70368777732096 , 0 , 0 , 268435456 , 31 , 0 , 0)#generer_un_echiquier_realiste_aleatoire(16)
        #ech_alea=Echiquier(35184407792896 , 129 , 262208 , 4100 , 2147483648 , 8 , 5357662464114688 , 9295429630892703744 , 144115222435594240 , 2594073385365405696 , 1152921504606846976 , 576460752303423488 , 30 , 0 , 0)
            #Echiquier(18018796564381696, 33554432, 0, 137438953474, 0, 2251799813685248, 335692288, 0, 1152921504606848000, 0, 8796093022208, 4294967296, 30, 0, 0)

        ech_alea=Echiquier(140737622573056 , 4398046511104 , 0 , 8192 , 36028797018963968 , 281474976710656 , 0 , 2050 , 0 , 1 , 0 , 1152921504606846976 , 0 , 0 , 5)


        print("Notation fen :")
        print(to_fen(ech_alea))
        print("Notation echiquier :")
        print(ech_alea.ech[0], ',', ech_alea.ech[1], ',', ech_alea.ech[2], ',', ech_alea.ech[3], ',', ech_alea.ech[4],
              ',', ech_alea.ech[5]
              , ',', ech_alea.ech[6], ',', ech_alea.ech[7], ',', ech_alea.ech[8], ',', ech_alea.ech[9], ',',
              ech_alea.ech[10], ',', ech_alea.ech[11], ',', ech_alea.ech[12], ',', ech_alea.ech[13], ',',
              ech_alea.ech[14])
        afficher_masque_attaque_dans_console(ech_alea, 0)

        ech_alea.ech[12] = 30
        coup = ech_alea.gen_coup_legaux(1)

        print("liste coup : ", coup)

        coup_rendu = []

        #on append les coups legaux
        for i in coup[0]:
            coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]])

        # on append les promotions de pions
        for i in coup[1]:
            if i[4] == 4 or i[4] == 10:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'q')
            elif i[4] == 1 or i[4] == 7:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'r')
            elif i[4] == 2 or i[4] == 8:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'n')
            else:
                coup_rendu.append(chiffre_lettre[i[-2]] + chiffre_lettre[i[-1]] + 'b')


        print("Les coups données par la library python:")

        board = chess.Board(to_fen(ech_alea))  # [:-4] pr rock
        coups = list(board.legal_moves)
        coup_triee = []
        for i in coups:
            coup_triee.append(str(i))
        print(sorted(coup_triee))
        print("Taille :", len(coups))

        print("Les coups données par l'implementation:")
        print(sorted(coup_rendu))
        print("Taille :", len(coup_rendu))
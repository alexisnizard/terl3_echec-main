from terl3_echec.TESTING.TEST_ECHIQUIER.avec_library_chess_de_python_utils import *
from terl3_echec.TESTING.TEST_ECHIQUIER.bitboard_utils import *
import chess
from terl3_echec.Game import *
from terl3_echec.fonctions_utils import *
from random import randint

#Transforme l'echiquier en notation FEN
def to_fen2(echiquier, nb_total_coup_joue):
    res=""
    cpt_ligne=0
    cpt_case_vide=0

    tab_nom_piece=['P','R','N','B','Q','K','p','r','n','b','q','k']
    for e in range(63,-1,-1):
        etait_ce_vide = 1

        for i in range(12):

            if (1 << e) & echiquier.ech[i]:
                if cpt_case_vide==0:
                    res = res + tab_nom_piece[i]
                else:
                    res = res +str(cpt_case_vide)+ tab_nom_piece[i]
                    cpt_case_vide=0

                etait_ce_vide=0
                break

        if etait_ce_vide:
            cpt_case_vide+=1

        if cpt_ligne==7:
            if cpt_case_vide == 0:
                res=res+"/"
            else:
                res = res +str(cpt_case_vide)+"/"
                cpt_case_vide=0
            cpt_ligne=-1


        cpt_ligne+=1

    res=res[:-1] #on vire le / de la fin

    tab_rock=[" K",'Q','k','q']
    tab_non_rock = [" -", '-', '-', '-']

    if echiquier.ech[12] & 1: #le tour
        res = res + ' w'
    else:
        res = res + ' b'

    for i in range(1,5):# les rocks
        if echiquier.ech[12] & 2**i:
            res = res + tab_rock[i-1]
        else:
            res = res + tab_non_rock[i-1]

    # en passant
    print("echiq 13 :",echiquier.ech[13])
    print("transformé en : ",chiffre_lettre[echiquier.ech[13]])
    if echiquier.ech[13]:
        res += ' '+str(chiffre_lettre[echiquier.ech[13]])
    else :
        res += ' -'

    # cpt demi-coup (0 a 50)
    res += ' '+str(echiquier.ech[14])

    # cpt globale
    res += ' '+str(nb_total_coup_joue)

    return res

print(chiffre_lettre)

ech_depart=Echiquier()
game=Game(ech_depart)
#print(game.get_coup_legaux())
#print(to_fen2(game.echiquier,1))
game.jouer(1)
print(to_fen2(game.echiquier,1))
#print(game.get_coup_legaux())
game.jouer(2)
print(to_fen2(game.echiquier,1))
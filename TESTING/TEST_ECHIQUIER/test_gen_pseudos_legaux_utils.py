from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import *
from terl3_echec.Echiquier import *

# Les fonctions de ce fichier servent uniquement pour avoir un affichage plus claire pour le vrai
# fichier de neural_network_chess-1.4 : test_gen_pseudos_legaux



def gen_pseudos_legaux_tour_bloque_par_allie(case,echiq_allie):
    pieces_bloquantes_allie = (masque_tour[case] & echiq_allie)

    n=s=e=o=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n=masque_s=masque_o=masque_e=0

    while (1 << n) | pieces_bloquantes_allie != pieces_bloquantes_allie : #tant que tu trouves des 0 en montant
        masque_n = masque_n | (1 << n)
        if n>63 :
            break
        else:
            n+=8

    while s>=0 and (1 << s) | pieces_bloquantes_allie != pieces_bloquantes_allie: #tant que tu trouves des 0 en descendant
        masque_s = masque_s | (1 << s)
        s-=8

    while (1 << o) | pieces_bloquantes_allie != pieces_bloquantes_allie : #tant que tu trouves des 0 en allant à gauche
        masque_o = masque_o | (1 << o)
        if  (1 << o) | masque_7 == masque_7 :
            break
        else:
            o += 1

    while (1 << e) | pieces_bloquantes_allie != pieces_bloquantes_allie : #tant que tu trouves des 0 en allant à droite
        masque_e = masque_e | (1 << e)
        if  (1 << e) | masque_0 == masque_0 :
            break
        else:
            e -= 1

    masque_allie = (masque_n | masque_s | masque_o | masque_e) ^ (1 << case) # on vire le 1 de la case
    return masque_allie



def gen_pseudos_legaux_fou_bloque_par_allie(case,echiq_allie):
    pieces_bloquantes_allie = (masque_fou[case] & echiq_allie)

    n_o=n_e=s_o=s_e=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n_o=masque_n_e=masque_s_o=masque_s_e=0

    while (1 << n_o) | pieces_bloquantes_allie != pieces_bloquantes_allie : #tant que tu trouves des 0 en diagonal haute gauche
        masque_n_o = masque_n_o | (1 << n_o)
        if n_o>63 or (1 << n_o) | masque_7 == masque_7  :
            break
        else:
            n_o+=9

    while (1 << n_e) | pieces_bloquantes_allie != pieces_bloquantes_allie :#tant que tu trouves des 0 en diagonal haute droite
        masque_n_e = masque_n_e | (1 << n_e)
        if n_e>63 or (1 << n_e) | masque_0 == masque_0 :
            break
        else:
            n_e+=7

    while s_o>=0 and (1 << s_o) | pieces_bloquantes_allie != pieces_bloquantes_allie:#tant que tu trouves des 0 en diagonal basse gauche
        masque_s_o = masque_s_o | (1 << s_o)
        if  (1 << s_o) | masque_7 == masque_7 :
            break
        s_o-=7

    while s_e>=0 and (1 << s_e) | pieces_bloquantes_allie != pieces_bloquantes_allie:#tant que tu trouves des 0 en diagonal basse droite
        masque_s_e = masque_s_e | (1 << s_e)
        if  (1 << s_e) | masque_0 == masque_0 :
            break
        s_e-=9


    masque_allie = (masque_n_o | masque_n_e | masque_s_o | masque_s_e) ^ (1 << case) # on vire le 1 de la case
    return masque_allie

def gen_pseudos_legaux_dame_bloque_par_allie(case,echiq_allie):
    return gen_pseudos_legaux_tour_bloque_par_allie(case, echiq_allie) | gen_pseudos_legaux_fou_bloque_par_allie(case, echiq_allie)


#pour gen_pseudos_legaux sliders inlucs
def gen_pseudos_legaux_tour_bloque_par_allie2(case,echiq_allie):
    pieces_bloquantes_allie = masque_tour[case] & echiq_allie

    # ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n = masque_s = masque_o = masque_e = 0

    n = s = e = o = case

    # on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    while not ((1 << n) & pieces_bloquantes_allie):
        if n > 63:  # gestion de la bordure du haut
            break
        else:
            n += 8
            masque_n = masque_n | (1 << n)

    while s >= 0 and not ((1 << s) & pieces_bloquantes_allie):
        s -= 8
        if s >= 0:
            masque_s = masque_s | (1 << s)

    while not ((1 << o) & pieces_bloquantes_allie):
        if (1 << o) & masque_7:
            break
        else:
            o += 1
            masque_o = masque_o | (1 << o)

    while not ((1 << e) & pieces_bloquantes_allie):
        if (1 << e) & masque_0:
            break
        else:
            e -= 1
            masque_e = masque_e | (1 << e)

    masque_allie = (masque_n | masque_s | masque_o | masque_e) & ~ (1 << case)  # on vire le 1 de la case
    return masque_allie

def gen_pseudos_legaux_fou_bloque_par_allie2(case,echiq_allie):
    pieces_bloquantes_allie = masque_fou[case] & echiq_allie

    n_o = n_e = s_o = s_e = case

    # ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n_o = masque_n_e = masque_s_o = masque_s_e = 0

    while not ((1 << n_o) & pieces_bloquantes_allie):  # tant que tu trouves des 0 en diagonal haute gauche
        if n_o > 63 or (1 << n_o) & masque_7:  # gestions des bordures (uniquement haut et gauche)
            break
        else:
            n_o += 9
            masque_n_o = masque_n_o | (1 << n_o)

    while not ((1 << n_e) & pieces_bloquantes_allie):
        if n_e > 63 or (1 << n_e) & masque_0:
            break
        else:
            n_e += 7
            masque_n_e = masque_n_e | (1 << n_e)

    while s_o >= 0 and not ((1 << s_o) & pieces_bloquantes_allie):
        if (1 << s_o) & masque_7:
            break
        s_o -= 7
        if s_o >= 0:
            masque_s_o = masque_s_o | (1 << s_o)

    while s_e >= 0 and not ((1 << s_e) & pieces_bloquantes_allie):
        if (1 << s_e) & masque_0:
            break
        s_e -= 9
        if s_e >= 0:
            masque_s_e = masque_s_e | (1 << s_e)

    masque_allie = (masque_n_o | masque_n_e | masque_s_o | masque_s_e) & ~ (1 << case)  # on vire le 1 de la case
    return masque_allie
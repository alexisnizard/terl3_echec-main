from terl3_echec.TESTING.ANCIEN_test.bitboard_utils_ANCIEN import afficher_bitboard_dans_console_couleur
from terl3_echec.gen_masques import *

while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Generer tout les masques possible du pion blanc AVANCE")
    print("2 - Generer tout les masques possible du pion noir AVANCE")
    print("3 - Generer tout les masques possible du roi")
    print("4 - Generer tout les masques possible du cavalier")
    print("5 - Generer tout les masques possible du fou")
    print("6 - Generer tout les masques possible de la tour")
    print("7 - Generer tout les masques possible de la dame")
    print("0 - Exit")
    choix=int(input())

    masques = 0

    if choix==0:
        break
    elif choix==1:
        masques= gen_masques_attaque_pion_blanc()
        print("Voici tout les masques du piont blanc :")
    elif choix==2:
        masques= gen_masques_attaque_pion_noir()
        print("Voici tout les masques du piont noir :")
    elif choix==3:
        masques = gen_masques_roi()
        print("Voici tout les masques du roi :")
    elif choix==4:
        masques = gen_masques_cavalier()
        print("Voici tout les masques du cavalier :")
    elif choix==5:
        masques = gen_masques_fou()
        print("Voici tout les masques du fou :")
    elif choix==6:
        masques = gen_masques_tour()
        print("Voici tout les masques de la tour :")
    elif choix==7:
        masques = gen_masques_dame()
        print("Voici tout les masques de la dame :")

    for i in range(64):
        print("Si la pièce est à la case numéro : ",i)
        afficher_bitboard_dans_console_couleur(masques[i], i)

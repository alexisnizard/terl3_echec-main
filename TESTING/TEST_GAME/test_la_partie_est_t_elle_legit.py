# créer un test qui compare avant de joué chaque coup si les coups legaux possibles sont identique a la library python (normalement vu que test_gen_coup_legaux marche pour des
# centaines de milliers d'echiquier c'est deja bon) puis, faire joué un coup a notre echiquier et le meme à l'echiquier de la library python et comparer si les deux echiquiers
# sont identiques.
# faire ca jusqu'a la fin de la partie ,si c'etait identique a chaque fois alors la partie est legit
# faire ce neural_network_chess-1.4 sur des centaines de milliers dechiquier

#debug en passant et rock
from terl3_echec.ANALYSE.GameAnalyse import *
import numpy as np
from random import randint
import time
import chess
from terl3_echec.fonctions_utils import *

print("Sur combien d'échiquier aléatoire voulez vous faire les tests ?")
choix_nbr_echiquier=int(input())
print("Quel couleur êtes vous ? 0 : blanc, 1 : noir ?")
choix_coul=int(input())

i = 0
endgame = [0, 0, 0, 0]  # nul 50 coup ;nul par pat; win blanc ; win noir
nbr_coup_par_partie = []
nbr_coup_avant_capture = [[] for i in range(15)]
nbr_coup_avant_capture_quand_blanc_gagnes = [[] for i in range(15)]
nbr_coup_avant_capture_quand_noir_gagnes = [[] for i in range(15)]
nbr_coup_avant_capture_quand_ya_nul = [[] for i in range(15)]
nbr_no_capture = [0 for i in range(15)]  # on compte le nombre de fois où on a pas atteind la Xeme capture
nbr_coup_avant_ppp = []
taille_sans_zero = 0
while i < choix_nbr_echiquier:
    ech = Echiquier()
    game = GameAnalyse(ech, choix_coul)  # mettre la couleur en param
    tableau_des_choix = []
    res = -2

    debug = []
    indx_debug=0
    while 1:

        quel_piece = [] #index du tab coup_legaux
        quel_deplacement=[[] for i in range(64)]
        cpt_p=0
        la_piece_a_t_elle_deja_ete_choisie=[]
        for k in game.coup_legaux[0]:
            if k[-2] not in la_piece_a_t_elle_deja_ete_choisie:
                quel_piece.append(cpt_p)
            quel_deplacement[k[-2]].append(cpt_p)
            la_piece_a_t_elle_deja_ete_choisie.append(k[-2])
            cpt_p+=1

        for k in game.coup_legaux[1]:
            if k[-2] not in la_piece_a_t_elle_deja_ete_choisie:
                quel_piece.append(cpt_p)
            quel_deplacement[k[-2]].append(cpt_p)
            la_piece_a_t_elle_deja_ete_choisie.append(k[-2])
            cpt_p+=1

        nbr_coup = len(game.coup_legaux[0])
        nbr_coup_promo = len(game.coup_legaux[1])
        nbr_coup_total = nbr_coup + nbr_coup_promo

        if nbr_coup_total == 0:
            le_coup_a_joue = 0
        else:
            if len(quel_piece)==1:
                quel_piece_jouer=quel_piece[0]
            else:
                quel_piece_jouer=randint(0,len(quel_piece)-1)
                quel_piece_jouer=quel_piece[quel_piece_jouer]

            if quel_piece_jouer < nbr_coup:
                if len(quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]]) == 1:
                    le_coup_a_joue = quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]][0] # 4 indices de tableau ! ca devient compliqué à suivre la
                else:
                    le_coup_a_joue = randint(0,len(quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]]) - 1)
                    le_coup_a_joue =  quel_deplacement[game.coup_legaux[0][quel_piece_jouer][-2]][le_coup_a_joue]

            else:

                if len(quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]]) == 1:
                    le_coup_a_joue = quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]][0] # 4 indices de tableau ! ca devient compliqué à suivre la
                else:
                    le_coup_a_joue = randint(0,len(quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]]) - 1)
                    le_coup_a_joue =  quel_deplacement[game.coup_legaux[1][nbr_coup-quel_piece_jouer][-2]][le_coup_a_joue]

        coup_implementation = sorted(game.get_coup_legaux_NA())

        board = chess.Board(to_fen_pour_librarie_python(game.echiquier))
        coups_python = list(board.legal_moves)
        coup_triee = []
        for j in coups_python:
            coup_triee.append(str(j))

        break_vrai_loop=0
        coups_python=sorted(coup_triee)

        tableau_des_choix.append(le_coup_a_joue)

        if coup_implementation!=coups_python:
            print("Notre echec position la tout duiste :",game.echiquier.ech_position)
            print("Erreur trouvé sur cette echiquier : ")
            print("Coup legaux de la library : ")
            print(coups_python)
            print("Coup legaux de notre Echiquier :")
            print(coup_implementation)
            print("Notation fen :")
            print(to_fen_pour_librarie_python(game.echiquier))
            print("Notation echiquier :")
            print(game.echiquier.ech[0], ',', game.echiquier.ech[1], ',', game.echiquier.ech[2], ',', game.echiquier.ech[3], ',',
                  game.echiquier.ech[4], ',', game.echiquier.ech[5]
                  , ',', game.echiquier.ech[6], ',', game.echiquier.ech[7], ',', game.echiquier.ech[8], ',', game.echiquier.ech[9], ',',
                  game.echiquier.ech[10], ',', game.echiquier.ech[11], ',', game.echiquier.ech[12], ',', game.echiquier.ech[13], ',',
                  game.echiquier.ech[14])
            print("historique des coups :")
            print(tableau_des_choix)

            break_vrai_loop=1
            break

        #res = game.jouer(debug[indx_debug])
        #indx_debug+=1
        res = game.jouer(le_coup_a_joue)
        if res == 0 or res == -1 or res == 1 or res == 2:
            break

    if break_vrai_loop:
        break
    i += 1

import webbrowser
from terl3_echec.Game import *
from terl3_echec.fonctions_utils import *
from random import randint

chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
while(1):
    print("Ecrivez le numéro de la fonction à tester : ")
    print("1 - Analyser sur chrome une partie dont l'issue est aléatoire")
    print("2 - Analyser sur chrome une partie dont l'issue est NULLE par règle des 50 coups")
    print("3 - Analyser sur chrome une partie dont l'issue est NULLE par pat")
    print("4 - Analyser sur chrome une partie dont l'issue est WIN des blancs")
    print("5 - Analyser sur chrome une partie dont l'issue est WIN des noirs")
    print("6 - DEBUG une partie")
    print("(A CODER) 7 - Analyser sur chrome une partie dont l'issue est NULLE par insuffiance de pièce")
    print("(A CODER) 8 - Analyser sur chrome une partie dont l'issue est NULLE par regles des 3 echiquiers identiques")
    print("0 - Exit")
    choix=int(input())

    if choix==0:
        break
    elif choix == 1:
        print("lol")

    elif choix== 2:
        fin_partie = 2
    elif choix == 3:
        fin_partie = 0
    elif choix == 4:
        fin_partie = 1
    elif choix == 5:
        fin_partie = -1

    if choix!=6:
        res = -2
        tableau_des_choix = []
        while res != fin_partie:

            ech = Echiquier()
            game1 = Game(ech)
            tableau_des_choix = []

            while 1:
                nbr_coup = len(game1.coup_legaux[0])
                nbr_coup_promo = len(game1.coup_legaux[1])
                nbr_coup_total = nbr_coup + nbr_coup_promo

                if nbr_coup_total == 0:
                    le_coup_a_joue = 0
                else:
                    le_coup_a_joue = randint(0, nbr_coup_total - 1)

                tableau_des_choix.append(le_coup_a_joue)
                res = game1.jouer(le_coup_a_joue)
                if res == 0 or res == -1 or res == 1 or res == 2:
                    break


        #on a le tableau des choix pret , maintenant on remplit le tableau d'url google
        tableau_url=[]
        ech = Echiquier()
        game1 = Game(ech)
        for i in tableau_des_choix:
            tableau_url.append("https://lichess.org/editor/"+to_fen(game1.echiquier))
            game1.jouer(i)


        print("Souhaitez vous voir directement les 10 derniers coups ? Y/N")
        y_n=input()
        if y_n== 'Y' or y_n == 'y':
            taille=len(tableau_url)
            for i in range(taille-10,taille):
                webbrowser.get(chrome_path).open(tableau_url[i])
        else:
            stop_10_urls=0
            for i in tableau_url:
                if stop_10_urls==10:
                    stop_10_urls=0
                    print("Envoyé une lettre pour récuperer les 10 prochains URLS")
                    a = input()
                webbrowser.get(chrome_path).open(i)
                stop_10_urls+=1

    if choix==6:

        '''
        on a rock :  42
[[27, 47], [42], [], [13], [55], [48], [], [11, 1], [], [42, 0], [], [60]]
Erreur trouvé sur cette echiquier : 
Coup legaux de la library : 
['d8c8', 'd8e8', 'e2c2', 'e2d2', 'e2e1', 'e2e3', 'e2e4', 'e2f2', 'e2g2', 'e2h2', 'g1a1', 'g1b1', 'g1c1', 'g1d1', 'g1e1', 'g1f1', 'g1g2', 'g1g3', 'g1g4', 'g1g5', 'g1g6', 'g1g7', 'g1g8', 'h1e4', 'h1f3', 'h1g2']
Coup legaux de notre Echiquier :
['d8c8', 'd8e8', 'e2c2', 'e2d2', 'e2e1', 'e2e3', 'e2e4', 'e2f2', 'e2g2', 'e2h2', 'f6a1', 'f6b2', 'f6c3', 'f6d4', 'f6e5', 'f6e7', 'f6g5', 'f6g7', 'f6h4', 'f6h8', 'g1a1', 'g1b1', 'g1c1', 'g1d1', 'g1e1', 'g1f1', 'g1g2', 'g1g3', 'g1g4', 'g1g5', 'g1g6', 'g1g7', 'g1g8', 'h1e4', 'h1f3', 'h1g2']
Notation fen :
3k4/Q6K/P4R2/8/4P3/8/2B1r3/6rb b - - 5 1
Notation echiquier :
140737622573056 , 4398046511104 , 0 , 8192 , 36028797018963968 , 281474976710656 , 0 , 2050 , 0 , 1 , 0 , 1152921504606846976 , 0 , 0 , 5
historique des coups :
[4, 3, 10, 2, 7, 16, 0, 16, 6, 0, 1, 20, 25, 17, 2, 12, 19, 11, 14, 2, 0, 19, 8, 9, 14, 20, 5, 6, 36, 0, 10, 8, 0, 5, 8, 9, 14, 6, 7, 31, 1, 9, 7, 27, 28, 10, 14, 1, 3, 30, 4, 27, 9, 1, 2, 33, 1, 0, 17, 5, 29, 29, 4, 2, 0, 8, 26, 22, 2, 14, 14, 26, 22, 28, 18, 4, 2, 33, 24, 32, 9, 21, 4, 9, 0, 24, 20, 1, 11, 0, 10, 5, 15, 3, 29, 7, 9, 1, 5, 1, 31, 2, 31, 21, 33, 30, 0, 3, 2, 3, 11, 13, 26, 26, 3, 3, 16, 1, 27, 23, 25, 21, 14, 7, 12, 20, 31, 8, 7, 2, 2, 20, 20, 0, 4, 3, 11, 10, 33, 28, 36, 0, 21, 24, 0, 25, 30, 14, 35, 7, 40, 28, 33, 18, 7, 25, 45, 0, 1, 0, 23, 16, 31, 25, 9, 4, 4, 0, 4, 13]
'''

        '''
        Erreur trouvé sur cette echiquier : 
Coup legaux de la library : 
['a6b8', 'a6c5', 'a8b8', 'b4a3', 'c7c5', 'c7c6', 'c8b7', 'c8d7', 'e6d5', 'e6e5', 'e8e7', 'f5d3', 'f5d5', 'f5e4', 'f5e5', 'f5f2', 'f5f3', 'f5f4', 'f5f6', 'f5g4', 'f5g6', 'f5h3', 'f5h7', 'f7f6', 'f8c5', 'f8d6', 'f8e7', 'f8g7', 'f8h6', 'g5g4', 'g5h4', 'g8e7', 'g8f6', 'g8h6', 'h8h6', 'h8h7']
Coup legaux de notre Echiquier :
['a6b8', 'a6c5', 'a8b8', 'b4a3', 'c7c5', 'c7c6', 'c8b7', 'c8d7', 'e6d5', 'e6e5', 'e8e7', 'f5d3', 'f5d5', 'f5e4', 'f5e5', 'f5f2', 'f5f3', 'f5f4', 'f5f6', 'f5g4', 'f5g6', 'f5h3', 'f5h7', 'f7f6', 'f8c5', 'f8d6', 'f8e7', 'f8g7', 'f8h6', 'g5g4', 'g5h4', 'g8e7', 'g8f6', 'g8h6', 'h5a3', 'h8h6', 'h8h7']
Notation fen :
r1b1kbnr/p1p2p2/n3p3/3Q1qpp/Pp5P/1P1PP3/2P1NPP1/RNB1K2R b KQkq a3 0 1
Notation echiquier :
2170037760 , 129 , 2112 , 32 , 68719476736 , 8 , 46170706232213504 , 9295429630892703744 , 144255925564211200 , 2594073385365405696 , 17179869184 , 576460752303423488 , 30 , 23 , 0
historique des coups :
[6, 12, 26, 8, 10, 26, 37, 10, 20, 2, 22, 0, 34, 1, 7, 21, 1, 4, 13, 13, 10, 6]
        '''

        '''

Notre echec position la tout duiste : [[24, 19, 28, 30], [7], [0, 44], [49], [13], [10], [25, 9, 27, 36, 45, 31, 47], [56], [39], [], [23], [48]]
Erreur trouvé sur cette echiquier : 
Coup legaux de la library : 
['a3a1', 'a3a2', 'a3b2', 'a3b3', 'a3b4', 'a3c1', 'a3c3', 'a3d3', 'a3e3', 'a5b3', 'a5b7', 'a5c4', 'c6c5', 'e4d3', 'g2g1b', 'g2g1n', 'g2g1q', 'g2g1r', 'g2h1b', 'g2h1n', 'g2h1q', 'g2h1r', 'g4g3', 'h7g6', 'h7g7', 'h7g8', 'h8a8', 'h8b8', 'h8c8', 'h8d8', 'h8e8', 'h8f8', 'h8g8']
Coup legaux de notre Echiquier :
['a3a1', 'a3a2', 'a3b2', 'a3b3', 'a3b4', 'a3c1', 'a3c3', 'a3d3', 'a3e3', 'a5b3', 'a5b7', 'a5c4', 'c6c5', 'g2g1b', 'g2g1n', 'g2g1q', 'g2g1r', 'g2h1b', 'g2h1n', 'g2h1q', 'g2h1r', 'g4g3', 'h7g6', 'h7g7', 'h7g8', 'h8a8', 'h8b8', 'h8c8', 'h8d8', 'h8e8', 'h8f8', 'h8g8']
Notation fen :
7r/6Bk/p1pN4/n2p4/pP1Pp1pP/q3P3/2Q2Kp1/R6N b - d3 0 1
Notation echiquier :
1359478784 , 128 , 17592186044417 , 562949953421312 , 8192 , 1024 , 175992895177216 , 72057594037927936 , 549755813888 , 0 , 8388608 , 281474976710656 , 0 , 20 , 0
historique des coups :


[5, 18, 2, 15, 11, 15, 11, 14, 7, 9, 10, 9, 14, 5, 18, 11, 25, 12, 7, 4, 8, 5, 20, 14, 9, 3, 4, 2, 1, 24, 28, 2, 16, 9, 4, 1, 1, 34, 8, 17, 5, 5, 0, 2, 17, 16, 3, 30, 9, 4, 49, 0, 20, 1, 29, 0, 12, 10, 41, 23, 0, 25, 4, 18, 11, 28, 32, 0, 41, 1, 5, 1, 15, 4, 47, 0, 2, 21]Ran 0 tests in 0.000s
      
        '''
        tableau_des_choix=[12, 1, 12, 1, 19, 9, 12, 10, 24, 13, 0, 10, 26, 10, 14, 9, 3, 0, 13, 19, 3, 10, 10, 28, 17, 0, 8, 1, 23, 8, 11, 16, 6, 3]

        tableau_url = []
        ech = Echiquier()
        game1 = Game(ech)
        for i in tableau_des_choix:
            tableau_url.append("https://lichess.org/editor/" + to_fen(game1.echiquier))
            game1.jouer(i)

        print("Souhaitez vous voir directement les 10 derniers coups ? Y/N")
        y_n = input()
        if y_n == 'Y' or y_n == 'y':
            taille = len(tableau_url)
            for i in range(taille - 10, taille):
                webbrowser.get(chrome_path).open(tableau_url[i])
        else:
            stop_10_urls = 0
            for i in tableau_url:
                if stop_10_urls == 10:
                    stop_10_urls = 0
                    print("Envoyé une lettre pour récuperer les 10 prochains URLS")
                    a = input()
                webbrowser.get(chrome_path).open(i)
                stop_10_urls += 1
from terl3_echec.gen_masques import *

masque_pion_blanc_attaque=gen_masques_attaque_pion_blanc()
masque_pion_noir_attaque=gen_masques_attaque_pion_noir()
masque_roi=gen_masques_roi()
masque_tour=gen_masques_tour()
masque_cavalier=gen_masques_cavalier()
masque_fou=gen_masques_fou()
masque_dame=gen_masques_dame()

masque_0 = gen_masque_colonne(0)
masque_7 = gen_masque_colonne(7)

#En enlevant les pièces alliés presente sur le masque, on obtient directement les coups pseudos legaux
#Note : on ajoute echiq_ennemie=0 pour pouvoir optimiser le code plus tard en créant une fonction générique generant n'importe quel masque pseudo legal
def gen_pseudos_legaux_cavalier(case,echiq_allie,echiq_ennemie=0):
    return masque_cavalier[case] & ~ echiq_allie


#En enlevant les pièces alliés presente sur le masque, on obtient directement les coups pseudos legaux
def gen_pseudos_legaux_roi(case,echiq_allie,echiq_ennemie=0):
    return masque_roi[case] & ~ echiq_allie





#Cette fonction genere uniquement le masque pseudo légal d'attaque possible (c-a-d qu'il y a bien un ennemie sur la case de deplacement) du pion blanc
def gen_pseudos_legaux_attaque_possible_pion_blanc(case, echiq_allie, echiq_ennemie):
    if 55 < case < 64:#le cas où on est dans la dernière ligne, les transformation se font dans gen_coup_legaux
        return 0

    masque=masque_pion_blanc_attaque[case]
    masque = masque & ~ echiq_allie #on vire les alliés qui sont sur les 2 positions d'attaque
    masque = masque & echiq_ennemie #on ajoute les enemies qui sont sur les 2 positions d'attaque

    return masque

#Cette fonction genere uniquement le masque d'avancement pseudo légal du pion blanc
def gen_pseudos_legaux_avance_possible_pion_blanc(case, echiq_allie, echiq_ennemie):
    if 55 < case < 64:#le cas où on est dans la dernière ligne, les transformation se font dans gen_coup_legaux
        return 0

    case_8_condamne=0 #la logique derriere cette variable est : si on veut avancé de 2 alors on doit pouvoir avancé de 1

    masque=0

    if not((1 << case + 8) & (echiq_ennemie | echiq_allie)):#si il n'y a pas de piont allié ou ennemie devant tu peux avancé
        masque = (1 << case + 8)
        case_8_condamne=1

    if 7 < case < 16:# si on est ligne 1 (ya ligne 0 avant)
        if case_8_condamne==1 and not((1 << case + 16) & (echiq_ennemie | echiq_allie)):#si il n'y a pas de piont allié ou ennemie 2 case devant tu peux avancé
            masque = masque | (1 << case + 16)

    return masque





def gen_pseudos_legaux_attaque_possible_pion_noir(case, echiq_allie, echiq_ennemie):
    if 0 <= case < 8 :#le cas où on est dans la dernière ligne, les transformation se font dans gen_coup_legaux
        return 0

    masque=masque_pion_noir_attaque[case]
    masque = masque & ~ echiq_allie #on vire les alliés qui sont sur les 2 positions d'attaque
    masque = masque & echiq_ennemie #on ajoute les enemies qui sont sur les 2 positions d'attaque

    return masque

def gen_pseudos_legaux_avance_possible_pion_noir(case, echiq_allie, echiq_ennemie):
    if 0 <= case < 8 :#le cas où on est dans la dernière ligne, les transformation se font dans gen_coup_legaux
        return 0

    case_8_condamne=0 #la logique derriere cette variable est : si on veut avancé de 2 alors on doit pouvoir avancé de 1

    masque=0

    if not((1 << case - 8) & (echiq_ennemie | echiq_allie)):#si il n'y a pas de piont allié ou ennemie devant tu peux avancé
        masque = (1 << case - 8)
        case_8_condamne=1

    if 47 < case < 56:# si on est ligne 1 (ya ligne 0 avant)
        if case_8_condamne==1 and not((1 << case - 16) & (echiq_ennemie | echiq_allie)):#si il n'y a pas de piont allié ou ennemie 2 case devant tu peux avancé
            masque = masque | (1 << case - 16)

    return masque





# coups pseudos legaux de la tour = on limite les déplacement du masque en prenant en compte les pièces alliés
# puis on fait de même avec les pièces ennemies cette fois-ci, seul différence : la première pièce ennemie bloquante peut etre manger
# exist_roi_ennemie est le masque du roi ennemie ,si il est different de 3 ( 11 en binaire, car il ne peut y avoir 2 roi ds un echiquier) ca veut dire qu'on
# ne prend pas la piece du roi en compte lors de la génération des coups peudos legaux
def gen_pseudos_legaux_tour(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    pieces_bloquantes_allie = masque_tour[case] & echiq_allie

    n=s=e=o=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n=masque_s=masque_o=masque_e=0

    while not((1 << n) & pieces_bloquantes_allie): #tant que tu trouves des 0 en montant
        masque_n = masque_n | (1 << n)
        if n>55 :
            break
        else:
            n+=8

    while s>=0 and not((1 << s) & pieces_bloquantes_allie): #tant que tu trouves des 0 en descendant
        masque_s = masque_s | (1 << s)
        s-=8

    while not((1 << o) & pieces_bloquantes_allie): #tant que tu trouves des 0 en allant à gauche
        masque_o = masque_o | (1 << o)
        if  (1 << o) & masque_7 :
            break
        else:
            o += 1

    while not((1 << e) & pieces_bloquantes_allie): #tant que tu trouves des 0 en allant à droite
        masque_e = masque_e | (1 << e)
        if  (1 << e) & masque_0:
            break
        else:
            e -= 1

    masque_allie = (masque_n | masque_s | masque_o | masque_e) & ~ (1 << case) # on vire le 1 de la case

    if exist_roi_ennemie==3:# si il est prit en compte
        pieces_bloquantes_ennemie = masque_allie & echiq_ennemie
    else:
        pieces_bloquantes_ennemie = masque_allie & (echiq_ennemie & ~ exist_roi_ennemie)

    n = s = e = o = case
    masque=0

    # on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    copymasque = masque #copy du masque ,permet d'empecher que si on monte tout en haut ,on ecrase la limite imposé par l'allié
    while not((1 << n) & pieces_bloquantes_ennemie):
        if n>63 :#gestion de la bordure du haut
            masque= copymasque | masque_n
            break
        else:
            n+=8
            masque = masque | (1 << n)

    copymasque = masque
    while s>=0 and not((1 << s) & pieces_bloquantes_ennemie):
        s-=8
        if s >=0:
            masque = masque | (1 << s)
    if s < 0:
        masque = copymasque | masque_s

    copymasque = masque
    while not((1 << o) & pieces_bloquantes_ennemie):
        if  (1 << o) & masque_7:
            masque = copymasque | masque_o
            break
        else:
            o += 1
            masque = masque | (1 << o)

    copymasque=masque
    while not((1 << e) & pieces_bloquantes_ennemie):
        if  (1 << e) & masque_0:
            masque = copymasque | masque_e
            break
        else:
            e -= 1
            masque = masque | (1 << e)

    return masque & ~ (1 << case)



# coups pseudos legaux du fou = on limite les déplacement du masque en prenant en compte les pièces alliés
# puis on fait de même avec les pièces ennemies cette fois-ci, seul différence : la première pièce ennemie bloquante peut etre manger
# exist_roi_ennemie est le masque du roi ennemie ,si il est different de 3 ( 11 en binaire, car il ne peut y avoir 2 roi ds un echiquier) ca veut dire qu'on
# ne prend pas la piece du roi en compte lors de la génération des coups peudos legaux
def gen_pseudos_legaux_fou(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    pieces_bloquantes_allie = masque_fou[case] & echiq_allie

    n_o=n_e=s_o=s_e=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n_o=masque_n_e=masque_s_o=masque_s_e=0

    while not((1 << n_o) & pieces_bloquantes_allie): #tant que tu trouves des 0 en diagonal haute gauche
        masque_n_o = masque_n_o | (1 << n_o)
        if n_o>55 or (1 << n_o) & masque_7:
            break
        else:
            n_o+=9

    while not((1 << n_e) & pieces_bloquantes_allie):#tant que tu trouves des 0 en diagonal haute droite
        masque_n_e = masque_n_e | (1 << n_e)
        if n_e>55 or (1 << n_e) & masque_0:
            break
        else:
            n_e+=7

    while s_o>=0 and not((1 << s_o) & pieces_bloquantes_allie):#tant que tu trouves des 0 en diagonal basse gauche
        masque_s_o = masque_s_o | (1 << s_o)
        if  (1 << s_o) & masque_7:
            break
        s_o-=7

    while s_e>=0 and not((1 << s_e) & pieces_bloquantes_allie):#tant que tu trouves des 0 en diagonal basse droite
        masque_s_e = masque_s_e | (1 << s_e)
        if  (1 << s_e) & masque_0:
            break
        s_e-=9


    masque_allie = (masque_n_o | masque_n_e | masque_s_o | masque_s_e) & ~ (1 << case) # on vire le 1 de la case

    if exist_roi_ennemie==3:# si il est prit en compte
        pieces_bloquantes_ennemie = masque_allie & echiq_ennemie
    else:
        pieces_bloquantes_ennemie = masque_allie & (echiq_ennemie & ~ exist_roi_ennemie)

    n_o = n_e = s_o = s_e = case

    masque = 0

    #on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    copymasque = masque
    while not((1 << n_o) & pieces_bloquantes_ennemie): #tant que tu trouves des 0 en diagonal haute gauche
        if n_o > 63 or (1 << n_o) & masque_7: # gestions des bordures (uniquement haut et gauche)
            masque = copymasque | masque_n_o
            break
        else:
            n_o += 9
            masque = masque | (1 << n_o)

    copymasque = masque

    while not((1 << n_e) & pieces_bloquantes_ennemie):
        if n_e > 63 or (1 << n_e) & masque_0:
            masque = copymasque | masque_n_e
            break
        else:
            n_e += 7
            masque = masque | (1 << n_e)

    copymasque = masque

    while s_o >= 0 and not((1 << s_o) & pieces_bloquantes_ennemie):
        if (1 << s_o) & masque_7:
            masque = copymasque | masque_s_o
            break
        s_o -= 7
        if s_o >= 0:
            masque = masque | (1 << s_o)
    if s_o < 0 :
        masque = copymasque | masque_s_o

    copymasque = masque
    while s_e >= 0 and not((1 << s_e) & pieces_bloquantes_ennemie):
        if (1 << s_e) & masque_0:
            masque = copymasque | masque_s_e
            break
        s_e -= 9
        if s_e >= 0:
            masque = masque | (1 << s_e)
    if s_e < 0:
        masque = copymasque | masque_s_e

    return masque & ~ (1 << case)


#coups pseudos legaux de la dame = ceux du fou OR ceux de la tour
def gen_pseudos_legaux_dame(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    if exist_roi_ennemie==3:# si il est prit en compte
        return gen_pseudos_legaux_tour(case, echiq_allie, echiq_ennemie) | gen_pseudos_legaux_fou(case, echiq_allie, echiq_ennemie)
    else:
        return gen_pseudos_legaux_tour(case, echiq_allie, echiq_ennemie,exist_roi_ennemie) | gen_pseudos_legaux_fou(case, echiq_allie,echiq_ennemie,exist_roi_ennemie)
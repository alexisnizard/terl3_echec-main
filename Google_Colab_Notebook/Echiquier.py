from gen_pseudos_legaux import *
from gen_pseudos_legaux_sliders_inclus_premier_allie import *
import numpy as np
import tensorflow as tf
import copy

#Mémo des indexs du tableau :

# 0 -> pion blanc | 1 -> tour blanc | 2 -> cavalier blanc | 3 -> fou blanc | 4 -> dame blanc | 5 -> roi blanc
# 6 -> pion noir | 7 -> tour noir | 8 -> cavalier noir | 9 -> fou noir | 10 -> dame noir | 11 -> roi noir
# 12 -> a qui est-ce le tour de jouer ? + peut on rocké côté ROI-BLANC,DAME-BLANC,ROI-NOIR,DAME-NOIR
# 13 -> case en passant : la case où la capture est possible
# 14 -> compteur demi-coup

class Echiquier:

    #constructeur par default et paramétré en même temps
    def __init__(self, pion_b=65280, tour_b=129, cavalier_b=66, fou_b=36, dame_b=16, roi_b=8, pion_n=71776119061217280,
                 tour_n=9295429630892703744, cavalier_n=4755801206503243776, fou_n=2594073385365405696,
                 dame_n=1152921504606846976, roi_n=576460752303423488, tour_et_rock=31, en_passant=0, cpt_demicoup=0,ech_position=0):

        self.ech = [pion_b, tour_b, cavalier_b, fou_b, dame_b, roi_b, pion_n, tour_n, cavalier_n, fou_n, dame_n, roi_n,
                    tour_et_rock, en_passant, cpt_demicoup]

        if ech_position: #permet d'utiliser directement l'ech position passé en paramètre plutot que de le recalculer (utilisé dans la classe Game)
            self.ech_position=ech_position
        else:
            self.ech_position = [self.get_position(self.ech[i]) for i in range(12)] # mettre a jour après le deplacement d'une piece

    def get_echiquier_blanc(self):
        return self.ech[0] | self.ech[1] | self.ech[2] | self.ech[3] | self.ech[4] | self.ech[5]

    def get_echiquier_noir(self):
        return self.ech[6] | self.ech[7] | self.ech[8] | self.ech[9] | self.ech[10] | self.ech[11]

    def get_echiquier(self):
        return self.get_echiquier_blanc() | self.get_echiquier_noir()



    # Retourne les cases occupées par une pièce dans l'echiquier (bitboard) donnée en paramètre
    @staticmethod
    def get_position(bitboard):
        tab = []

        for i in range(64):
            if (1 << i) & bitboard:
                tab.append(i)

        return tab

    #flip l'echiquier pour calculer les coups legaux
    def flip_echiquier_verticalement(self):
        for i in range(12):
            for j in range(32):
                s = self.ech[i] & (1 << j)
                s_56 = self.ech[i] & (1 << (j ^ 56))
                pas_swap = 1

                if s:  # si ya une piece en bas
                    self.ech[i] = self.ech[i] & ~ (1 << j)  # vire la piece en bas
                    self.ech[i] = self.ech[i] | (1 << (j ^ 56))  # ajoute la piece en haut
                    pas_swap = 0

                if s_56:  # si y a une piece en haut
                    if pas_swap:
                        self.ech[i] = self.ech[i] & ~ (1 << (j ^ 56))  # vire la piece en haut
                    self.ech[i] = self.ech[i] | (1 << j)  # ajoute la piece en bas

        #en passant :
        if self.ech[13]!=0:
            self.ech[13]=self.ech[13]^56

        #rocks:
        K=Q=k=q=0

        if self.ech[12] & 4:
            K=16
        if self.ech[12] & 2:
            Q=8
        if self.ech[12] & 16:
            k = 4
        if self.ech[12] & 8:
            q = 2

        self.ech[12]=k + q + K + Q + 1 #on ajoute 1 pr le tour de role

        #on swap les cellules
        for i in range(6):
            self.ech[i],self.ech[i+6]= self.ech[i+6],self.ech[i]

    # Transforme echiquier en input pour le RN de la forme 8*8*14 ,avec le flip vertical quand c'est aux noir de joué
    def echiq_to_input(self):

        if self.ech[12] & 1:  # si c'est aux blancs de joué

            tab = np.zeros((14, 8, 8), dtype=int)

            for i in range(12):
                ligne = 7
                col = 7
                for j in range(0, 64):
                    if (1 << j) & self.ech[i]:
                        tab[i][col][ligne] = 1
                    ligne -= 1
                    if ligne == -1:
                        ligne = 7
                        col -= 1

            tab[12] = np.ones((8, 8), dtype=int)# on indique que c'est aux blanc de joué
            tab[13] = np.ones((8, 8), dtype=int)# ce plane sert juste à aider notre RN à détecter le padding ajouté par la convolution

            # tf.convert_to_tensor(tab)
            return tf.convert_to_tensor(tab)
        else:
            #on flip verticalement l'echiquier
            copy_echiquier = copy.deepcopy(self.ech)
            self.flip_echiquier_verticalement()
            tab = np.zeros((14, 8, 8), dtype=int)

            for i in range(12):
                ligne = 7
                col = 7
                for j in range(0, 64):
                    if (1 << j) & self.ech[i]:
                        tab[i][col][ligne] = 1
                    ligne -= 1
                    if ligne == -1:
                        ligne = 7
                        col -= 1

            tab[13] = np.ones((8, 8), dtype=int)# ce plane sert juste à aider notre RN à détecter le padding ajouté par la convolution

            self.ech=copy_echiquier
            return tf.convert_to_tensor(tab)


    #Genere les coups pseudos legaux de chaque pièce pour chaque index
    def gen_pseudos_legaux_chaque_piece(self,index):
        nom_fonctions = [gen_pseudos_legaux_tour, gen_pseudos_legaux_cavalier, gen_pseudos_legaux_fou,
                         gen_pseudos_legaux_dame, gen_pseudos_legaux_roi]

        # tableau des masques de coups legaux pour chaque coup :
        resultat = []


        if index < 6:
            # noms des fonction generant le masque d'attaque de NOS pions + les masques pseudos legaux de nos autres pieces
            # on utilise l'attaque possible ici (car on veut tout nos deplacements possibles)
            nom_fonctions = [gen_pseudos_legaux_attaque_possible_pion_blanc] + nom_fonctions

            ech_allie = self.get_echiquier_blanc()
            ech_ennemie = self.get_echiquier_noir()
            avance = gen_pseudos_legaux_avance_possible_pion_blanc
            index_0_ou_6 = 0


        else:
            nom_fonctions = [gen_pseudos_legaux_attaque_possible_pion_noir] + nom_fonctions
            avance = gen_pseudos_legaux_avance_possible_pion_noir
            ech_allie = self.get_echiquier_noir()
            ech_ennemie = self.get_echiquier_blanc()
            index_0_ou_6 = 6


        for j in self.ech_position[index]:  # on parcours toutes les pièces presente dans l'echiquier

            masque_pseudo_leg = nom_fonctions[index - index_0_ou_6](j, ech_allie, ech_ennemie)
            masque_attaque_de_la_piece_allie = masque_pseudo_leg


            masque_avance_du_pion_allie = avance(j, ech_allie, ech_ennemie)
            if index == index_0_ou_6:  # si on est dans la masque des pions on ajoute la masque d'avancement dans les coups legaux
                resultat.append(masque_attaque_de_la_piece_allie | masque_avance_du_pion_allie)
            else:
                resultat.append(masque_attaque_de_la_piece_allie)

        return resultat

    #si en passant n'est pas vide alors on calcul le masque d'attaque apres que l'allié ai fait une prise en passant
    def gen_pseudos_legaux_attaque_chaque_piece(self,index,cas_special_enpassant=0):
        nom_des_masque_non_sliders = [masque_tour, masque_cavalier, masque_fou, masque_dame, masque_roi]
        nom_fonctions_sliders = [0, gen_pseudos_legaux_tour_inc_allie, 0, gen_pseudos_legaux_fou_inc_allie,gen_pseudos_legaux_dame_inc_allie, 0]

        # tableau des masques de coups legaux incluant les pieces alliés pour chaque coup :
        resultat_attaque = []

        if index < 6:
            nom_des_masque_non_sliders = [masque_pion_blanc_attaque] + nom_des_masque_non_sliders  # /!\ on utilise le tableau de masque directement, pas celui des pseudo legaux

            ech_allie = self.get_echiquier_blanc()

            if cas_special_enpassant:
                ech_ennemie = self.get_echiquier_noir() &~ cas_special_enpassant
            else:
                ech_ennemie = self.get_echiquier_noir()

            index_0_ou_6 = 0

            roi_ennemie = 11
            index_t = 1;index_f = 3;index_d = 4

        else:
            nom_des_masque_non_sliders = [masque_pion_noir_attaque] + nom_des_masque_non_sliders
            ech_allie = self.get_echiquier_noir()

            if cas_special_enpassant:
                ech_ennemie = self.get_echiquier_blanc() &~ cas_special_enpassant
            else:
                ech_ennemie = self.get_echiquier_blanc()

            index_0_ou_6 = 6

            roi_ennemie = 5
            index_t = 7;index_f = 9;index_d = 10

        for j in self.ech_position[index]:  # on parcours toutes les pièces presente dans l'echiquier

            if index == index_t or index == index_f or index == index_d:
                masque_ignorant_allie = nom_fonctions_sliders[index - index_0_ou_6](j, ech_allie, ech_ennemie,self.ech[roi_ennemie])

            # on ajoute l'allié dans le masque pseudo legal
            else :
                masque_ignorant_allie = nom_des_masque_non_sliders[index - index_0_ou_6][j]


            resultat_attaque.append(masque_ignorant_allie)

        return resultat_attaque


    #On fusionne tout les masques pseudos legaux d'attaque (d'attaque = les pieces mange egalement leurs alliés)
    def gen_attaque_global(self,couleur,cas_special_enpassant=0):
        if couleur == 0:
            index_p = 0
        else:
            index_p = 6

        pseudo_leg_attaque=[]
        for i in range(index_p,index_p+6):
            if cas_special_enpassant:
                pseudo_leg_attaque.append(self.gen_pseudos_legaux_attaque_chaque_piece(i,cas_special_enpassant))
            else:
                pseudo_leg_attaque.append(self.gen_pseudos_legaux_attaque_chaque_piece(i))

        masque=0
        for i in range(6):
            for j in pseudo_leg_attaque[i]:
                masque = masque | j


        return masque

    # Compte le nombre de piece dans un bitboard
    @staticmethod
    def compte_piece(bitboard):
        cpt = 0

        for i in range(64):
            if (1 << i) & bitboard:
                cpt += 1

        return cpt

    #renvoit le sextuplet [index de notre echiquier pion (0 ou 6) ,nouvelle echiquier du pion ,index de l'echiquier pion ennemie (0 ou 6),nouvelle echiquier du pion;
    # pos_avant;pos_apres]
    def pions_pouvant_capturer_en_passant(self):
        sextuplet = []


        if self.ech[13] > 39: #si c'est une prise pour les blanc
            if masque_pion_noir_attaque[self.ech[13]] & self.ech[0]: #si on a bien un pion blanc dispo pour capture le pion noir

                for i in self.ech_position[0]:#pour chaque pion blanc :
                    if (i == self.ech[13] - 7) or  (i == self.ech[13] - 9):

                        if (1 << self.ech[13]) & 72340172838076673 :
                            if i == self.ech[13] - 7: #on gere les bordures

                                test_echec = Echiquier((self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), self.ech[1],
                                                       self.ech[2], self.ech[3], self.ech[4],
                                                       self.ech[5], (self.ech[6] & ~ (1 << (self.ech[13] - 8))),
                                                       self.ech[7], self.ech[8], self.ech[9],
                                                       self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                       self.ech[14])

                                attaque_ennemie_test = test_echec.gen_attaque_global(1)
                                if not attaque_ennemie_test & self.ech[5]:
                                    sextuplet.append([0, (self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), 6,
                                                      (self.ech[6] & ~ (1 << (self.ech[13] - 8))), i, self.ech[13]])



                        elif (1 << self.ech[13]) & 9259542123273814144:
                            if i == self.ech[13] - 9:
                                test_echec = Echiquier((self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), self.ech[1],
                                                       self.ech[2], self.ech[3], self.ech[4],
                                                       self.ech[5], (self.ech[6] & ~ (1 << (self.ech[13] - 8))),
                                                       self.ech[7], self.ech[8], self.ech[9],
                                                       self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                       self.ech[14])

                                attaque_ennemie_test = test_echec.gen_attaque_global(1)
                                if not attaque_ennemie_test & self.ech[5]:
                                    sextuplet.append([0, (self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), 6,
                                                      (self.ech[6] & ~ (1 << (self.ech[13] - 8))), i, self.ech[13]])

                        else:
                            test_echec = Echiquier((self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), self.ech[1],
                                                   self.ech[2], self.ech[3], self.ech[4],
                                                   self.ech[5], (self.ech[6] & ~ (1 << (self.ech[13] - 8))),
                                                   self.ech[7], self.ech[8], self.ech[9],
                                                   self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                   self.ech[14])

                            attaque_ennemie_test = test_echec.gen_attaque_global(1)
                            if not attaque_ennemie_test & self.ech[5]:
                                sextuplet.append([0, (self.ech[0] & ~ (1 << i)) | (1 << self.ech[13]), 6,
                                                  (self.ech[6] & ~ (1 << (self.ech[13] - 8))), i, self.ech[13]])


        else:#si c'est une prise pour les noirs
            if masque_pion_blanc_attaque[self.ech[13]] & self.ech[6]:  # si on a bien un pion noir dispo pour capture le pion blanc

                for i in self.ech_position[6]:  # pour chaque pion noir :
                    if (i == self.ech[13] + 7) or (i == self.ech[13] + 9):

                        if (1<<self.ech[13]) & 72340172838076673 :
                            if i == self.ech[13] + 9:  # on gere les bordures

                                test_echec = Echiquier((self.ech[0] & ~ (1 << (self.ech[13] + 8))), self.ech[1],
                                                       self.ech[2], self.ech[3], self.ech[4],
                                                       self.ech[5],  (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]),
                                                       self.ech[7], self.ech[8], self.ech[9],
                                                       self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                       self.ech[14])

                                attaque_ennemie_test = test_echec.gen_attaque_global(0)
                                if not attaque_ennemie_test & self.ech[11]:
                                    sextuplet.append([6, (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]), 0,
                                                      (self.ech[0] & ~ (1 << (self.ech[13] + 8))), i, self.ech[13]])

                        elif (1<<self.ech[13]) & 9259542123273814144 :
                            if i == self.ech[13] + 7:

                                test_echec = Echiquier((self.ech[0] & ~ (1 << (self.ech[13] + 8))), self.ech[1],
                                                       self.ech[2], self.ech[3], self.ech[4],
                                                       self.ech[5], (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]),
                                                       self.ech[7], self.ech[8], self.ech[9],
                                                       self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                       self.ech[14])

                                attaque_ennemie_test = test_echec.gen_attaque_global(0)
                                if not attaque_ennemie_test & self.ech[11]:
                                    sextuplet.append([6, (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]), 0,
                                                      (self.ech[0] & ~ (1 << (self.ech[13] + 8))), i, self.ech[13]])
                        else:

                            test_echec = Echiquier((self.ech[0] & ~ (1 << (self.ech[13] + 8))), self.ech[1],
                                                   self.ech[2], self.ech[3], self.ech[4],
                                                   self.ech[5], (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]),
                                                   self.ech[7], self.ech[8], self.ech[9],
                                                   self.ech[10], self.ech[11], self.ech[12], self.ech[13],
                                                   self.ech[14])

                            attaque_ennemie_test = test_echec.gen_attaque_global(0)
                            if not attaque_ennemie_test & self.ech[11]:
                                sextuplet.append([6, (self.ech[6] & ~ (1 << i)) | (1 << self.ech[13]), 0,
                                                  (self.ech[0] & ~ (1 << (self.ech[13] + 8))), i, self.ech[13]])

        return sextuplet

    # renvoit le sextuplet [index de notre echiquier roi (5 ou 11) ,nouvelle echiquier du roi ,index de notre echiquier tour (1 ou 7),nouvelle echiquier de la tour;
    # pos_avant_roi;pos_apres_roi]
    def puis_je_rocker(self,couleur,masque_attaque_ennemie):

        couple_roi_tour=[]
        peut_rocker=1

        if couleur==0:

            if not ((1 << 3) & self.ech[5]): #on verif que le roi est bien la
                return []

            if self.ech[12] & 2: # si on peut eventuellement rocké coté roi blanc

                if not(6 & self.get_echiquier()):# si les cases sont vides

                    for i in range(1,4): #on parcours chaque case entre la tour et le roi
                        if (1 << i) & masque_attaque_ennemie: #on vérif qu'il n'y a pas d'echec
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        couple_roi_tour = [[5,(1<<1),1, (self.ech[1] & ~ (1<<0)) | (1<<2),3,1]] # le roi en case 3 va en case 1 | la tour va en case 2

                    if not ((1 << 0) & self.ech[1]):  #on verif que la tour est bien la
                        couple_roi_tour = []

            peut_rocker = 1

            if self.ech[12] & 4:

                if not(112 & self.get_echiquier()):

                    for i in range(3,7):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        couple_roi_tour.append([5,(1<<5),1, (self.ech[1] & ~ (1<<7)) | (1<<4),3,5] ) # le roi en case 3 va en case 5 | la tour va en case 4

                    if not ((1 << 7) & self.ech[1]):
                        if peut_rocker:
                            couple_roi_tour.pop()
        else:

            if not ((1 << 59) & self.ech[11]):
                return []

            if self.ech[12] & 8: # si on peut eventuellement rocké coté roi noir

                if not (432345564227567616 & self.get_echiquier()):

                    for i in range(57, 60):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        couple_roi_tour = [[11,(1<<57),7, (self.ech[7] & ~ (1<<56)) | (1<<58),59,57]] # le roi en case 59 va en case 57 | la tour va en case 58

                    if not ((1 << 56) & self.ech[7]):
                        couple_roi_tour = []

                peut_rocker = 1

            if self.ech[12] & 16:

                if not (8070450532247928832 & self.get_echiquier()):

                    for i in range(59,63):
                        if (1 << i) & masque_attaque_ennemie:
                            peut_rocker = 0
                            break

                    if peut_rocker:
                        couple_roi_tour.append([11,(1<<61),7, (self.ech[7] & ~ (1<<63)) | (1<<60),59,61])# le roi en case 59 va en case 61 | la tour va en case 60

                    if not ((1 << 63) & self.ech[7]):
                        if peut_rocker:
                            couple_roi_tour.pop()

        return couple_roi_tour



    #renvoit la liste des coups legaux dans des tableaux de
        # -sextuplet : [index ech;son nouveau masque;index ech ennemie,son nouveau masque;pos_avant;pos_apres]
        # SI on ne mange aucune piece ,au lieu de mettre un -1 aux deux dernier index et de faire un if plus tard lorsqu'on voudra avancé
        # ,pour uniformisé on met l'index du roi blanc suivit de son echiquier identique

        # -octuplet : [ind ech pion; son nouveau masque;index ech ennemie,son nouveau masque;ind ech dame; son nouveau masque;pos_avant;pos_apres] ,suivit du
        # meme tab pr tour cavalier et fou si c'est une promotion de pion

    # note : en passant et rock correspondent à un "on mange" tout simplement
    def gen_coup_legaux(self, couleur):

        nom_tableau=[gen_pseudos_legaux_tour_allie_only, masque_cavalier, gen_pseudos_legaux_fou_allie_only,gen_pseudos_legaux_dame_allie_only]
        if couleur==0:#si on est blanc
            depart=0
            depart_modifie=0
            depart_inv=6
            rock_possible=self.ech[12] & 6
            ech_allie = self.get_echiquier_blanc()
            ech_ennemie=self.get_echiquier_noir()
            ligne_de_promotion = [47, 56]
            nom_tableau = [masque_pion_noir_attaque]+nom_tableau
            pseudo_leg = [self.gen_pseudos_legaux_chaque_piece(i) for i in range(6)]
        else:
            depart=6
            depart_modifie=6
            depart_inv=0
            rock_possible=self.ech[12] & 24
            ech_allie = self.get_echiquier_noir()
            ech_ennemie = self.get_echiquier_blanc()
            ligne_de_promotion = [7, 16]
            nom_tableau = [masque_pion_blanc_attaque]+nom_tableau
            pseudo_leg = [self.gen_pseudos_legaux_chaque_piece(i) for i in range(6,12)]

        # Permet de réduire la complexité de l'algo :
        # On verifie si notre roi n'est pas en echec en dépit du fait qu'une piece allié puisse bloqué l'echec
        ya_til_echec_ignore_allie=0
        for e in range(depart_inv,depart_inv+5):
            for i in self.ech_position[e]:
                if e-depart_inv == 0 or e-depart_inv == 2:
                    ya_til_echec_ignore_allie= ya_til_echec_ignore_allie | nom_tableau[e-depart_inv][i]
                else:
                    ya_til_echec_ignore_allie = ya_til_echec_ignore_allie | nom_tableau[e - depart_inv](i,ech_ennemie,ech_allie)


        coup_legaux = [[],[]] #on initailise notre tableau de coup legaux (un tableau pour "on avance"/"on mange", un autre pour promotion de pion)

        reste_t_il_que_le_roi_a_tester=0

        index_ech_ennemie = 5 # index du roi blanc
        nouvel_ech_ennemie= self.ech[index_ech_ennemie] #echiquier ennemie par defaut ,celui du roi blanc

        on_mange_une_piece = 0  # initialisation du boolean qui dit si on a capturer un ennemie
        pos_avant=0 #initalisation inutile ,juste pour rassurer l'interpretateur python
        echiq_ennemie_avant=0#initalisation inutile ,juste pour rassurer l'interpretateur python

        if not(ya_til_echec_ignore_allie & self.ech[depart+5]):
            for e in range(depart, depart + 5):
                for i in range(len(pseudo_leg[e-depart])):

                    toute_les_cases = self.get_position(pseudo_leg[e-depart][i])  # on prend le numero de toutes les cases correspondant à un deplacement pseudo legal de notre piece

                    position_piece = self.ech_position[e][i]  # notre piece se trouve dans cette case

                    for j in toute_les_cases:

                        j_masque = 1 << j  # on transforme le numero de la case en un masque

                        if not(j_masque & ech_ennemie): #si on ne mange pas de piece :

                            if e == depart and ligne_de_promotion[0] < position_piece < ligne_de_promotion[1]:  # Si c'est une promotion de pion
                                for h in [4, 1, 2, 3]:  # ajout de la dame puis tour,cavalier et fou
                                    coup_legaux[1].append(
                                        [e, (self.ech[e] & ~ (1 << position_piece)), index_ech_ennemie, nouvel_ech_ennemie, e + h,
                                         self.ech[e + h] | j_masque,position_piece,j])
                            else:
                                coup_legaux[0].append(
                                    [e, (self.ech[e] & ~ (1 << position_piece)) | j_masque, index_ech_ennemie,nouvel_ech_ennemie,position_piece,j])  # on ajoute alors le quadruplé dans notre tableau
                        else:# si on capture un ennemie
                            for m in range(depart_inv,depart_inv + 6):  # tant qu'on trouve pas l'echiquier ennemie où la piece se fait capturer
                                if self.ech[m] & j_masque:  # on l'a trouvé


                                    if e == depart and ligne_de_promotion[0] < position_piece < ligne_de_promotion[1]:  # Si c'est une promotion de pion
                                        for h in [4, 1, 2, 3]:  # ajout de la dame puis tour,cavalier et fou
                                            coup_legaux[1].append(
                                                [e, (self.ech[e] & ~ (1 << position_piece)),
                                                 m, self.ech[m] & ~ j_masque , e + h,self.ech[e + h] | j_masque,position_piece,j])
                                    else:
                                        coup_legaux[0].append(
                                            [e, (self.ech[e] & ~ (1 << position_piece)) | j_masque, m, self.ech[m] & ~ j_masque,position_piece,j])  # on ajoute alors le quadruplé dans notre tableau

                                    break

            reste_t_il_que_le_roi_a_tester=depart+5

        if reste_t_il_que_le_roi_a_tester:
            depart_modifie=reste_t_il_que_le_roi_a_tester

        for e in range(depart_modifie,depart+6):
            for i in range(len(pseudo_leg[e-depart])):

                toute_les_cases=self.get_position(pseudo_leg[e-depart][i])# on prend le numero de toutes les cases correspondant à un deplacement pseudo legal de notre piece

                position_piece = self.ech_position[e][i]# notre piece se trouve dans cette case

                for j in toute_les_cases:
                    echiq_avant=self.ech[e]

                    j_masque= 1 << j #on transforme le numero de la case en un masque

                    self.ech[e] = (self.ech[e] & ~ (1 << position_piece)) | j_masque  # on enleve l'ancienne piece et on la fait se deplacer

                    if j_masque & ech_ennemie:# si on capture un ennemie

                        for m in range(depart_inv,depart_inv+6): #tant qu'on trouve pas l'echiquier ennemie où la piece se fait capturer
                            if self.ech[m] & j_masque: #on l'a trouvé

                                on_mange_une_piece = 1  # boolean qui nous dit qu'on devra réinitialisé ce qu'on a modifié
                                pos_avant = self.ech_position[m] #on retiens quel etait le tableau de position (car on va le modifié)
                                echiq_ennemie_avant=self.ech[m] #on retiens quel etait notre echiquier (car on va le modifié)

                                index_ech_ennemie=m #index de lechiquier ennemie quon va modifié

                                nouvel_ech_ennemie= self.ech[m] & ~ j_masque #echiquier ennemie modifié ,la piece a été manger

                                self.ech[m]=nouvel_ech_ennemie #on supprime la piece

                                #on la supprime egalement sa position
                                nouvelle_position=[]
                                for a in range(len(self.ech_position[m])):
                                    if self.ech_position[m][a]!=j:
                                        nouvelle_position.append(self.ech_position[m][a])

                                self.ech_position[m]=nouvelle_position
                                break

                    # Masque de l'attaque global de l'ennemie
                    attaque_ennemie = self.gen_attaque_global(couleur - 1)  # -1 comme ca si c'est 1 c'est zero ,sinn c'est noir

                    if not(attaque_ennemie & self.ech[depart+5]): # on verifie que ca ne met pas notre roi en echec

                        if e == depart and ligne_de_promotion[0] < position_piece < ligne_de_promotion[1]: #Si c'est une promotion de pion
                            for h in [4,1,2,3]:#ajout de la dame puis tour,cavalier et fou
                                self.ech[e] = self.ech[e]  &~ j_masque
                                coup_legaux[1].append(
                                    [e,self.ech[e], index_ech_ennemie,nouvel_ech_ennemie,e+h,self.ech[e+h] | j_masque,position_piece,j])

                        else:
                            coup_legaux[0].append(
                                [e,self.ech[e], index_ech_ennemie,nouvel_ech_ennemie,position_piece,j])  # on ajoute alors le quadruplé dans notre tableau


                    self.ech[e] = echiq_avant #on remet l'ancien echiquier

                    if on_mange_une_piece:
                        self.ech_position[index_ech_ennemie]=pos_avant #on remet l'ancien tableau de position ennemie
                        self.ech[index_ech_ennemie]=echiq_ennemie_avant #on remet l'ancien echiquier ennemie
                        index_ech_ennemie=5 #on remet l'index à l'echiquier du roi blanc
                        nouvel_ech_ennemie = self.ech[index_ech_ennemie] #on remet le nouvel echiquier ennemie à celui du roi blanc
                        on_mange_une_piece=0 #on réinitialise le boolean a faux


        #si on peut éventuellement rocké
        if rock_possible:
            rock = self.puis_je_rocker(couleur, self.gen_attaque_global(couleur - 1))
            if rock:
                for i in rock:
                    coup_legaux[0].append(i)

        # si on peut éventuellement faire une prise en passant
        if self.ech[13] != 0:
            en_passant = self.pions_pouvant_capturer_en_passant()
            if en_passant:
                for i in en_passant:
                    coup_legaux[0].append(i)

        return coup_legaux

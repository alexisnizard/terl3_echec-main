from RN_utils import *
from Game import *
import math
import copy
import numpy as np
import random

#Chaque branche est composé de :

# P : Probabilité renvoyé par le Prior (RN) pour le coup donnant la position de notre noeud

# N : Nombre de fois que le noeud a été visité

# W : récompense total ,initalisé à 0, il s'incrémente de v à chaque fois qu'on visite le noeud en remontant de la feuille jusqu'à la racine.
    # v correspond à l'evaluation de la position d'une feuille renvoyé par le RN

# Q : Moyenne de W par rapport à N

class Branche:
    def __init__(self, coup,index_coup, noeud_PERE):
        self.P = 0
        self.N = 0
        self.W = 0
        self.Q = 0
        self.noeud_PERE = noeud_PERE
        self.coup = coup
        self.index_coup = index_coup

class Noeud:
    def __init__(self, instance_game, branche_PERE):
        self.instance_game = instance_game
        self.branche_PERE = branche_PERE
        self.branche_et_noeud_FILS = []

    # On créer pour chaque fils (<=> nouvelle position d'echiquier apres avoir joué le coup) les branches suivit de leurs noeuds
    # Et on renvoit le ValueNetwork renvoyé par notre RN
    def expansion(self, RN):

        #On récupère les coups legaux de notre echiquier (ils sont sous formes algébrique ,c'est pas des indexs)
        liste_coups, tab_index_des_noirs = self.instance_game.get_coup_legaux_NA_RN()

        # Lorsqu'on interroge notre RN ,si c'est au noirs de joué on flip l'echiquier verticalement pour permettre de simplifié l'apprentissage
        # (le RN comprendra que les pions ne font que de monté et qu'en général les pieces ennemie se trouve en haut et celles alliés en bas)

        # C'est pour ca qu'on renvoit un tab_index_des_noirs , il permet de selectionner le bonne index du coup à joué,
        # car apres le flip vertical, ceux-ci sont dans le désordre
        if tab_index_des_noirs: #si c'est aux blancs de joué c'est une liste vide
            coups = tab_index_des_noirs
        else:
            coups = [index_coup for index_coup in range(len(liste_coups))]

        for c in coups:
            branche_FILS = Branche(liste_coups[c],c, self)
            # Optimisation ultra importante : on met None à l'instance de la game pour ne pas avoir à faire des appels inutiles et couteux
            # à gen_coup_legaux sur des echiquiers qui ne seront jamais exploré
            noeud_FILS = Noeud(None, branche_FILS)

            self.branche_et_noeud_FILS.append([branche_FILS,noeud_FILS])

        RN_output = RN.predict(np.array([self.instance_game.echiquier.echiq_to_input()]))

        som_probas = 0.
        for branche,_ in self.branche_et_noeud_FILS:
            #on cherche dans le dictionnaire à quel indice du prior de notre RN correspond notre coup legal
            m_idx=algebrique_to_index_PRIOR[branche.coup]
            branche.P = RN_output[0][0][m_idx]
            som_probas += branche.P

        for branche,_ in self.branche_et_noeud_FILS:
            branche.P = branche.P / som_probas #on affecte la valeur du prior au P de notre Noeud ,en faisant en sorte que ca soit une probabilité sur 1
        v = RN_output[1][0][0] # ValueNetwork

        return v


class MonteCarlo:

    def __init__(self, noeud_RACINE,RN,couleur_depart=0,c=0.4):
        self.RN = RN
        self.couleur_depart = couleur_depart #ne sert a rien pour l'apprentissage par réinforcement (car on commence tjr avec les blancs) ,mais servira quand on voudra joué les noirs
        self.noeud_RACINE = noeud_RACINE
        self.t = 1.0 # hyperparametre tau, on le laissera tjr à 1 car on utilisera une distribution multinomial plus tard
        self.c = c# hyperparametre cput, plus il est grand plus on favorise l'exploration à l'exploitation

    # Fonction UCT qui permet de balancer entre exploitation et exploration . Plus la constante c est petite plus on favorise l'exploitation
    def UCT(self, branche):
        return self.c * branche.P * (math.sqrt(branche.noeud_PERE.branche_PERE.N) / (1+branche.N))

    def select(self, noeud):

        if not noeud.branche_et_noeud_FILS: # si on est une feuille <=> si notre fils ne correspond pas à la liste vide
            return noeud
        else:
            max_UCT_FILS = None
            max_UCT = -100000000.
            ya_til_plusieurs_fils_avec_le_meme_max_UCT=0

            for branche_FILS, noeud_FILS in noeud.branche_et_noeud_FILS:

                uct = self.UCT(branche_FILS)
                val = branche_FILS.Q

                # Quand c'est à l'adversaire de joué ,on inverse la récompense. Si son noeud est aventageux alors il est désaventageux pour nous
                # Note : on inverse Q et non v car on calcul l'UCT avant de calculer v
                if (self.couleur_depart and branche_FILS.noeud_PERE.instance_game.echiquier.ech[12] & 1) \
                        or not branche_FILS.noeud_PERE.instance_game.echiquier.ech[12] & 1:
                    val = -branche_FILS.Q

                uct_du_FILS = val + uct
                if uct_du_FILS > max_UCT:
                    max_UCT_FILS = noeud_FILS
                    max_UCT = uct_du_FILS
                    ya_til_plusieurs_fils_avec_le_meme_max_UCT=-1

                if uct_du_FILS==max_UCT:
                    ya_til_plusieurs_fils_avec_le_meme_max_UCT+=1

            # Cas rare où on tomberait sur plusieurs probabilité maximum égals (quoique cela peut arrivé quand il y a que 2 ou 3 coups à joué)
            # Si c'est le cas on le tire au hasard plutôt que de tjr choisir le premier
            if ya_til_plusieurs_fils_avec_le_meme_max_UCT:
                liste_des_fils = []
                for branche_FILS, noeud_FILS in noeud.branche_et_noeud_FILS:
                    uct = self.UCT(branche_FILS)
                    val = branche_FILS.Q
                    if (self.couleur_depart and branche_FILS.noeud_PERE.instance_game.echiquier.ech[12] & 1) \
                            or not branche_FILS.noeud_PERE.instance_game.echiquier.ech[12] & 1:
                        val = -branche_FILS.Q
                    uct_du_FILS = val + uct
                    if uct_du_FILS == max_UCT:
                        liste_des_fils.append(noeud_FILS)
                if len(liste_des_fils) > 1:
                    idx = random.randint(0, len(liste_des_fils)-1)
                    return self.select(liste_des_fils[idx])
                else:
                    return self.select(max_UCT_FILS)

            else:
                return self.select(max_UCT_FILS) # on descend dans l'arbre

    def expansion_simulation(self, noeud):

        # C'est maintenant qu'on est sur la feuille (et donc qu'on est certains qu'on va l'exploré) qu'on fait l'appel à gen_coup_legaux
        instance_game_FILS = copy.deepcopy(noeud.branche_PERE.noeud_PERE.instance_game)
        #print("coup legaux du pere",noeud.branche_PERE.noeud_PERE.instance_game.get_coup_legaux_NA())
        #print("yen a ",len(noeud.branche_PERE.noeud_PERE.instance_game.get_coup_legaux_NA())," et je joue le numero ",noeud.branche_PERE.index_coup)
        instance_game_FILS.jouer(noeud.branche_PERE.index_coup)

        # On remplace l'instance None qu'on a créer plus tôt par la vrai nouvelle instance
        noeud.instance_game=instance_game_FILS

        # Si c'est une fin de partie gagnante/perdante ,on le fait bien comprendre à montecarlo en renvoyé un extremum
        game_res=noeud.instance_game.verif_cas_terminaux()
        if game_res!=3: #si la game est finit
            v=0.0
            if self.couleur_depart:
                if game_res == 1:
                    v = -1.0
                if game_res == -1:
                    v = 1.0
            else:
                if game_res == 1:
                    v = 1.0
                if game_res == -1:
                    v = -1.0

            self.retropropagation(v, noeud.branche_PERE)
            return

        # On créer les fils de nos noeuds
        v = noeud.expansion(self.RN)
        self.retropropagation(v, noeud.branche_PERE)

    def retropropagation(self, v, branche):

        branche.N += 1
        branche.W = branche.W + v
        branche.Q = branche.W / branche.N

        # On remonte jusqu'a la racine ,à chaque fois on uptade W en utilisant le meme v
        if branche.noeud_PERE is not None:
            if branche.noeud_PERE.branche_PERE is not None:
                self.retropropagation(v, branche.noeud_PERE.branche_PERE)

    def demarrer_la_recherche(self):

        #on génère les fils de notre racine
        self.noeud_RACINE.expansion(self.RN)
        for i in range(0,50):
            #on parcours l'arbre jusqu'a tombé sur une feuille
            feuille = self.select(self.noeud_RACINE)
            '''
            #########
            # Debug :
            for j,_ in self.noeud_RACINE.branche_et_noeud_FILS:
                print("coup : ", j.coup, ", son N : ", j.N," son Q ",j.Q)
            print()
            #########
            '''

            # on génère les fils de la feuille
            self.expansion_simulation(feuille)


        prior_ameliore = []
        #On utilisé la constante tau pour permettre de l'exploration une fois de plus
        for branche, noeud in self.noeud_RACINE.branche_et_noeud_FILS:
            prob = (branche.N ** (1 / self.t)) / ((self.noeud_RACINE.branche_PERE.N-1) ** (1/self.t))
            prior_ameliore.append((branche.coup, prob,branche.index_coup))

        #On renvoit un tableau de triplet : [nom du coup ,sa probabilité,index du coup]
        return prior_ameliore


from fonctions_utils import *
from gen_masques import *

# Retourne les positions d'une piece dans un echiquier
def get_position_utils(bitboard):
    tab = []

    for i in range(64):
        if (1 << i) & bitboard:
            tab.append(i)

    return tab

# Retourne tous les coups de dames/cavalier et promotion de pions possibles pour chacune des cases de l'echiquier
def remplir_dictionnaire_all_deplacements_possible(res):
    masque_dame_RN = gen_masques_dame()
    masque_cavalier_RN = gen_masques_cavalier()
    masque_pion_blanc_attaque_RN = gen_masques_attaque_pion_blanc()

    les_suffixe = ['q', 'r', 'n', 'b']

    index = 0

    for i in range(len(masque_dame_RN)):  # coup de la dame
        for j in get_position_utils(masque_dame_RN[i]):
            res[str(chiffre_lettre[i]) + str(chiffre_lettre[j])] = index
            index += 1

    for i in range(len(masque_cavalier_RN)):  # coup du cavalier
        for j in get_position_utils(masque_cavalier_RN[i]):
            res[str(chiffre_lettre[i]) + str(chiffre_lettre[j])] = index
            index += 1

    for i in range(48, 56):  # coup des promotions de pions blancs
        for e in les_suffixe:
            res[str(chiffre_lettre[i]) + str(chiffre_lettre[i + 8]) + e] = index  # on avance le pion
            index += 1

        for j in get_position_utils(masque_pion_blanc_attaque_RN[i]):
            for e in les_suffixe:

                res[str(chiffre_lettre[i]) + str(chiffre_lettre[j]) + e] = index  # on mange une pièce
                index += 1

# algebrique_to_index_PRIOR est un dictionnaire qui nous permettra de convertir n'importe quel
# notation algébrique en une des 1968 cases de l'output Prior de notre RN.
# Exemple : algebrique_to_index_PRIOR["h1g1"] nous renvoit 0
algebrique_to_index_PRIOR = {}
remplir_dictionnaire_all_deplacements_possible(algebrique_to_index_PRIOR)  # Remplissage du dictionnaire avec les 1968 déplacements possibles


# Convertie les résultats renvoyé par l'output Prior de notre RN (un tableau de 1880 cases)
# en une probabilité pour chaque coup legaux (un tableau d'environ 100 cases en moyenne)
# On renvoit un tableau 2 dimensions de tout les coups legaux ,avec comme première indice l'indice du coup legaux ,et en deuxieme sa probabilité donné par le Prior
def outputPrior_to_coup_legaux(output_prior, instance_game):
    #output_prior_modif = np.around(output_prior, decimals=4)
    proba_coup_legaux=[]
    coup_legaux,tab_index=instance_game.get_coup_legaux_NA_RN()

    '''
    som = 0
    for i in range(len(coup_legaux)):
        som += output_prior[algebrique_to_index_PRIOR[coup_legaux[i]]]
        # on va chercher la probabilité du coup legaux dispo dans l'output prior à l'index renvoyé par le dictionnaire
    '''
    if not instance_game.echiquier.ech[12] & 1:
        for i in range(len(coup_legaux)):
            proba_coup_legaux.append([tab_index[i], output_prior[algebrique_to_index_PRIOR[coup_legaux[i]]]])
            # proba_coup_legaux.append([tab_index[i], output_prior[algebrique_to_index_PRIOR[coup_legaux[i]]] / som]) si on voulait remettre proba a 1

    else:
        for i in range(len(coup_legaux)):
            proba_coup_legaux.append([i,output_prior[algebrique_to_index_PRIOR[coup_legaux[i]]]])
            #coup_legaux[i] en premier para du append si on veut en notation algebrique
    return proba_coup_legaux
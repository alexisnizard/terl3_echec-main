import numpy as np

#permet de créer le masque d'une colonne
def gen_masque_colonne(c):#chiffre de 0 à 7
    masque=0
    for i in range(c,64,8):
        masque = masque | (1 << i)

    return masque

#permet de créer le masque d'une ligne
def gen_masque_ligne(l):#chiffre de 0 à 7
    masque=0
    depart=l*8
    arrive=(l*8)+8
    for i in range(depart,arrive,1):
        masque = masque | (1 << i)

    return masque


#Créer tout les masques (d'attaque uniquement) du PIONT BLANC
def gen_masques_attaque_pion_blanc():
    tab = [0 for i in range(64)]
    masque_0 = gen_masque_colonne(0)
    masque_7 = gen_masque_colonne(7)

    for e in range(0,56):
        masque=0

        if (1 << e) & masque_0 :
            masque = masque | (1 << e + 9)
        elif (1 << e) & masque_7:
            masque = masque | (1 << e + 7)
        else:
            masque = masque | (1 << e + 7)| (1 << e + 9)


        tab[e]=masque

    return tab

#Créer tout les masques (d'attaque uniquement) du PIONT NOIR
def gen_masques_attaque_pion_noir():
    tab = [0 for i in range(64)]
    masque_0 = gen_masque_colonne(0)
    masque_7 = gen_masque_colonne(7)

    e=63
    while e>7:
        masque = 0

        if (1 << e) & masque_7:
            masque = masque | (1 << e - 9)
        elif (1 << e) & masque_0:
            masque = masque | (1 << e - 7)
        else:
            masque = masque | (1 << e - 7) | (1 << e - 9)

        tab[e] = masque
        e-=1

    return tab

# Créer tout les masques du ROI
def gen_masques_roi():
    tab = [0 for i in range(64)]
    masque_0 = gen_masque_colonne(0)
    masque_7 = gen_masque_colonne(7)

    for e in range(64):
        masque = 0

        if (1 << e) & masque_0:
            if e==0:
                masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9)
            elif e==56:
                masque = masque | (1 << e + 1) | (1 << e - 7) | (1 << e - 8)
            else:
                masque = masque | (1 << e + 1) | (1 << e + 8) | (1 << e + 9) | (1 << e - 7) | (1 << e - 8)

        elif (1 << e) & masque_7:
            if e==7:
                masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7)
            elif e==63:
                masque = masque | (1 << e - 1) | (1 << e - 8) | (1 << e - 9)
            else:
                masque = masque | (1 << e - 1) | (1 << e + 8) | (1 << e + 7) | (1 << e - 8) | (1 << e - 9)
        elif e<8:
            masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9)
        elif e > 55:
            masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e - 7) | (1 << e - 8) | (1 << e - 9)
        else:
            masque = masque | (1 << e + 1) | (1 << e - 1) | (1 << e + 7) | (1 << e + 8) | (1 << e + 9) | (
            1 << e - 7) | (1 << e - 8) | (1 << e - 9)

        tab[e] = masque

    return tab


#Créer tout les masques du CAVALIER
def gen_masques_cavalier():
    tab = [0 for i in range(64)]

    masque_0 = gen_masque_colonne(0)
    masque_7 = gen_masque_colonne(7)
    masque_0_1 = gen_masque_colonne(0) | gen_masque_colonne(1)
    masque_6_7 =  gen_masque_colonne(6) | gen_masque_colonne(7)

    for e in range(64):
        masque = 0

        #déplacement vers le HAUT :

        if not ((1 << e) & masque_0) and e < 48:# 2 cases en haut puis 1 à droite
            masque = masque | (1 << e + 15)

        if not((1 << e) & masque_7) and e < 48:# 2 cases en haut puis 1 à gauche
            masque = masque | (1 << e + 17)

        if not((1 << e) & masque_0_1) and e < 56:# 2 cases à droite puis 1 en haut
            masque = masque | (1 << e + 6)

        if not((1 << e) & masque_6_7) and e < 56:# 2 cases à gauche puis 1 en haut
            masque = masque | (1 << e + 10)

        #déplacement vers le BAS :

        if not((1 << e) & masque_0) and e > 15:#2 cases en bas puis 1 à droite
            masque = masque | (1 << e - 17)

        if not((1 << e) & masque_7) and e > 15:#2 cases en bas puis 1 à gauche
            masque = masque | (1 << e - 15)

        if not((1 << e) & masque_0_1) and e > 7:#2 cases à droite puis 1 en bas
            masque = masque | (1 << e - 10)

        if not((1 << e) & masque_6_7) and e > 7:#2 cases à gauche puis 1 en bas
            masque = masque | (1 << e - 6)

        tab[e]=masque

    return tab

#Créer tout les masques de la TOUR
def gen_masques_tour():
    tab = [0 for i in range(64)]

    cpt=0
    for e in range(8):
        for j in range(8):
            masque= gen_masque_colonne(j) | gen_masque_ligne(e)
            masque = masque ^ (1 << cpt) #on vire le 1 où se trouve la tour

            tab[cpt]=masque
            cpt+=1

    return tab

#Créer tout les masques du FOU
def gen_masques_fou():
    tab = [0 for i in range(64)]

    masque_0 = gen_masque_colonne(0)
    masque_7 = gen_masque_colonne(7)

    for e in range(64):
        masque=0

        #chaque boucles while fait au maximum 7 itérations

        i=e
        while not((1 << i) & masque_7)  and i<56:#diagonal haut gauche
            masque=masque | (1 << i + 9)
            i=i+9

        i=e
        while not((1 << i) & masque_0) and i<56:#diagonal haut droite
            masque=masque | (1 << i + 7)
            i=i+7

        i = e
        while not((1 << i) & masque_0) and i>7:#diagonal bas droite
            masque = masque | (1 << i - 9)
            i = i - 9

        i = e
        while not((1 << i) & masque_7)  and i>7:#diagonal bas gauche
            masque = masque | (1 << i - 7)
            i = i - 7

        tab[e]=masque

    return tab

#Créer tout les masques de la dame
def gen_masques_dame():
    tab = [0 for i in range(64)]

    tab_tour=gen_masques_tour()
    tab_fou=gen_masques_fou()

    for e in range(64):
        tab[e]=tab_tour[e] | tab_fou[e]

    return tab

from Entrainement import *
import time
from tqdm import tqdm # super pratique pour faire des barres de chargement faut juste le mettre dans la boucle for, apres le "in range"
import keras

#################### PARAMETRE ####################

#iteMCTS=50 Nbr d'iterations faite par le MCT pour renvoyé le prior amélioré

nbrEpisode=10 #Nbr de partie allant jusqu'à une fin de game

nbrIteration=10 # Nombre de fois qu'on va entrainé notre RN (on l'attendra jamais)

epochs=256 # Nombre de fois qu'on va modifié les poids du RN. 1 epoque = avoir travailler sur l'entière dataset
# une epoque n'est pas suffisant pour changer suffisament les poids du neurone afin d'arriver à l'erreur minimal (y = y^) il faut en répéter plusieurs ,a chaque fois
#l'erreur minimal va descendre jusqu'a atteindre 0

batch_size=32 # on casse l'entiere dataset en petit paquet ,ca nous permettra d'aller plus vite / utilisé moins de ressource dans le training
#c'est a dire si par exemple on a fait 100 episodes ,supposons qu'un episode dure 1800 coup ,
#on a 1800*100=180000 position dechiquier a feed au RN . Donc si on prend une batch size de 16 par exemple ca donnera 180000/16 =
#11250 batchs de 16 training data. Une fois que les 11250 batchs auront été feed au RN ,on aura fait 1 epoch

#learning_rate :
#on fait confiance a keras il a une fonction pour ça , mais on pourrait le changé manuellement comment l'ont fait les createurs d'alphazero
#sauf que c'est compliqué de trouver les bonnes valeurs

#################### PARAMETRE ####################

start = time.time()

model = keras.models.load_model("RN_128x20_echiq_depart.keras")
entrainement = Entrainement(model)


for i in range(nbrIteration):
    print("Itération numero : ",i)
    Tab_position_echiquier = []
    Tab_prior_ameliore = []
    Tab_endgame = []
    # On joue 10 games par itéreation
    for j in tqdm(range(0,10)):
        position, prior_ameliore, endgame = entrainement.generer_episode()
        print("Une game a été finit ,ça a mis : ",time.time()-start," sc")
        Tab_position_echiquier += position
        Tab_prior_ameliore += prior_ameliore
        Tab_endgame += endgame

    Tab_position_echiquier = np.array(Tab_position_echiquier)
    Tab_prior_ameliore = np.array(Tab_prior_ameliore)
    Tab_endgame = np.array(Tab_endgame)

    model.fit(Tab_position_echiquier,[Tab_prior_ameliore, Tab_endgame], epochs=epochs, batch_size=batch_size)

    model.save('RN_128x20_echiq_depart_apres'+str(i)+'_iterations.keras')


end = time.time()
print("Temps mis par model:", end - start)

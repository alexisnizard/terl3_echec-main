from terl3_echec.gen_pseudos_legaux import masque_tour,masque_fou,masque_0,masque_7

#Ces fonctions sont les mêmes que celles de pseudos_legaux ,à la différence que pour les pièces sliders, elles incluent la premiere case allié rencontré

#Pour les pièces non sliders (pions ,cavalier,roi),pas la peine d'implémentation ,on utilise directement les masques de depart (pas masque pseudo legaux !)

#tour :
def gen_pseudos_legaux_tour_inc_allie(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    pieces_bloquantes_allie = masque_tour[case] & echiq_allie

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n=masque_s=masque_o=masque_e=0

    n = s = e = o = case

    # on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    while not((1 << n) & pieces_bloquantes_allie):
        if n>63 :#gestion de la bordure du haut
            break
        else:
            n+=8
            masque_n = masque_n | (1 << n)

    while s>=0 and not((1 << s) & pieces_bloquantes_allie):
        s-=8
        if s >=0:
            masque_s = masque_s | (1 << s)

    while not((1 << o) & pieces_bloquantes_allie):
        if  (1 << o) & masque_7:
            break
        else:
            o += 1
            masque_o = masque_o | (1 << o)

    while not((1 << e) & pieces_bloquantes_allie):
        if  (1 << e) & masque_0:
            break
        else:
            e -= 1
            masque_e = masque_e | (1 << e)

    masque_allie = (masque_n | masque_s | masque_o | masque_e) & ~ (1 << case) # on vire le 1 de la case

    if exist_roi_ennemie==3:# si il est prit en compte
        pieces_bloquantes_ennemie = masque_allie & echiq_ennemie
    else:
        pieces_bloquantes_ennemie = masque_allie & (echiq_ennemie & ~ exist_roi_ennemie)

    n = s = e = o = case
    masque=0

    # on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    copymasque = masque
    while not((1 << n) & pieces_bloquantes_ennemie):
        if n>63 :#gestion de la bordure du haut
            masque= copymasque | masque_n
            break
        else:
            n+=8
            masque = masque | (1 << n)

    copymasque = masque
    while s>=0 and not((1 << s) & pieces_bloquantes_ennemie):
        s-=8
        if s >=0:
            masque = masque | (1 << s)
    if s < 0:
        masque = copymasque | masque_s

    copymasque = masque
    while not((1 << o) & pieces_bloquantes_ennemie):
        if  (1 << o) & masque_7:
            masque = copymasque | masque_o
            break
        else:
            o += 1
            masque = masque | (1 << o)

    copymasque=masque
    while not((1 << e) & pieces_bloquantes_ennemie):
        if  (1 << e) & masque_0:
            masque = copymasque | masque_e
            break
        else:
            e -= 1
            masque = masque | (1 << e)

    return masque & ~ (1 << case)



#fou:
def gen_pseudos_legaux_fou_inc_allie(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    pieces_bloquantes_allie = masque_fou[case] & echiq_allie

    n_o=n_e=s_o=s_e=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n_o=masque_n_e=masque_s_o=masque_s_e=0

    while not ((1 << n_o) & pieces_bloquantes_allie):  # tant que tu trouves des 0 en diagonal haute gauche
        if n_o > 63 or (1 << n_o) & masque_7:  # gestions des bordures (uniquement haut et gauche)
            break
        else:
            n_o += 9
            masque_n_o = masque_n_o | (1 << n_o)


    while not ((1 << n_e) & pieces_bloquantes_allie):
        if n_e > 63 or (1 << n_e) & masque_0:
            break
        else:
            n_e += 7
            masque_n_e = masque_n_e | (1 << n_e)


    while s_o >= 0 and not ((1 << s_o) & pieces_bloquantes_allie):
        if (1 << s_o) & masque_7:
            break
        s_o -= 7
        if s_o >= 0:
            masque_s_o = masque_s_o | (1 << s_o)

    while s_e >= 0 and not ((1 << s_e) & pieces_bloquantes_allie):
        if (1 << s_e) & masque_0:
            break
        s_e -= 9
        if s_e >= 0:
            masque_s_e = masque_s_e | (1 << s_e)


    masque_allie = (masque_n_o | masque_n_e | masque_s_o | masque_s_e) & ~ (1 << case) # on vire le 1 de la case


    if exist_roi_ennemie==3:# si il est prit en compte
        pieces_bloquantes_ennemie = masque_allie & echiq_ennemie
    else:
        pieces_bloquantes_ennemie = masque_allie & (echiq_ennemie & ~ exist_roi_ennemie)

    n_o = n_e = s_o = s_e = case

    masque = 0

    #on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    copymasque = masque
    while not((1 << n_o) & pieces_bloquantes_ennemie): #tant que tu trouves des 0 en diagonal haute gauche
        if n_o > 63 or (1 << n_o) & masque_7: # gestions des bordures (uniquement haut et gauche)
            masque = copymasque | masque_n_o
            break
        else:
            n_o += 9
            masque = masque | (1 << n_o)

    copymasque = masque

    while not((1 << n_e) & pieces_bloquantes_ennemie):
        if n_e > 63 or (1 << n_e) & masque_0:
            masque = copymasque | masque_n_e
            break
        else:
            n_e += 7
            masque = masque | (1 << n_e)

    copymasque = masque

    while s_o >= 0 and not((1 << s_o) & pieces_bloquantes_ennemie):
        if (1 << s_o) & masque_7:
            masque = copymasque | masque_s_o
            break
        s_o -= 7
        if s_o >= 0:
            masque = masque | (1 << s_o)
    if s_o < 0 :
        masque = copymasque | masque_s_o

    copymasque = masque
    while s_e >= 0 and not((1 << s_e) & pieces_bloquantes_ennemie):
        if (1 << s_e) & masque_0:
            masque = copymasque | masque_s_e
            break
        s_e -= 9
        if s_e >= 0:
            masque = masque | (1 << s_e)
    if s_e < 0:
        masque = copymasque | masque_s_e

    return masque & ~ (1 << case)


#dame:
def gen_pseudos_legaux_dame_inc_allie(case,echiq_allie,echiq_ennemie,exist_roi_ennemie=3):
    if exist_roi_ennemie==3:# si il est prit en compte
        return gen_pseudos_legaux_tour_inc_allie(case, echiq_allie, echiq_ennemie) | gen_pseudos_legaux_fou_inc_allie(case, echiq_allie, echiq_ennemie)
    else:
        return gen_pseudos_legaux_tour_inc_allie(case, echiq_allie, echiq_ennemie,exist_roi_ennemie) | gen_pseudos_legaux_fou_inc_allie(case, echiq_allie,echiq_ennemie,exist_roi_ennemie)


#####

#tour qui prend en compte que ses alliés
def gen_pseudos_legaux_tour_allie_only(case,echiq_allie,echiq_ennemie):
    pieces_bloquantes_allie = masque_tour[case] & echiq_allie

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n=masque_s=masque_o=masque_e=0

    n = s = e = o = case

    # on refait les itérations mais pour les enemies cette fois-ci
    # idem, 14 itérations en tout
    while not((1 << n) & pieces_bloquantes_allie):
        if n>63 :#gestion de la bordure du haut
            break
        else:
            n+=8
            masque_n = masque_n | (1 << n)

    while s>=0 and not((1 << s) & pieces_bloquantes_allie):
        s-=8
        if s >=0:
            masque_s = masque_s | (1 << s)

    while not((1 << o) & pieces_bloquantes_allie):
        if  (1 << o) & masque_7:
            break
        else:
            o += 1
            masque_o = masque_o | (1 << o)

    while not((1 << e) & pieces_bloquantes_allie):
        if  (1 << e) & masque_0:
            break
        else:
            e -= 1
            masque_e = masque_e | (1 << e)

    return (masque_n | masque_s | masque_o | masque_e) & ~ (1 << case) # on vire le 1 de la case


#fou qui prend en compte que ses alliés:
def gen_pseudos_legaux_fou_allie_only(case,echiq_allie,echiq_ennemie):
    pieces_bloquantes_allie = masque_fou[case] & echiq_allie

    n_o=n_e=s_o=s_e=case

    #ces 4 boucles whiles réunit font au maximum en tout 14 itérations dans le pire des cas
    masque_n_o=masque_n_e=masque_s_o=masque_s_e=0

    while not ((1 << n_o) & pieces_bloquantes_allie):  # tant que tu trouves des 0 en diagonal haute gauche
        if n_o > 63 or (1 << n_o) & masque_7:  # gestions des bordures (uniquement haut et gauche)
            break
        else:
            n_o += 9
            masque_n_o = masque_n_o | (1 << n_o)


    while not ((1 << n_e) & pieces_bloquantes_allie):
        if n_e > 63 or (1 << n_e) & masque_0:
            break
        else:
            n_e += 7
            masque_n_e = masque_n_e | (1 << n_e)


    while s_o >= 0 and not ((1 << s_o) & pieces_bloquantes_allie):
        if (1 << s_o) & masque_7:
            break
        s_o -= 7
        if s_o >= 0:
            masque_s_o = masque_s_o | (1 << s_o)

    while s_e >= 0 and not ((1 << s_e) & pieces_bloquantes_allie):
        if (1 << s_e) & masque_0:
            break
        s_e -= 9
        if s_e >= 0:
            masque_s_e = masque_s_e | (1 << s_e)


    return (masque_n_o | masque_n_e | masque_s_o | masque_s_e) & ~ (1 << case) # on vire le 1 de la case


#dame qui prend en compte que ses alliés:
def gen_pseudos_legaux_dame_allie_only(case,echiq_allie,echiq_ennemie):
    return gen_pseudos_legaux_tour_allie_only(case, echiq_allie, echiq_ennemie) | gen_pseudos_legaux_fou_allie_only(case, echiq_allie, echiq_ennemie)

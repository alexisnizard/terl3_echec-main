from asyncio.windows_events import NULL
from tkinter import *
from tkinter import font
from tkinter.font import BOLD
import string
from terl3_echec.Echiquier import *
from terl3_echec.Game import *
from terl3_echec.RN_utils import *

import keras


import random


# Création de la fenêtre d'accueil
fenAcc = Tk()

fenAcc.title("Jeu d'échecs")
fenAcc.geometry("600x600")
fenAcc.resizable(height=False, width=False)

police = font.Font(family='Times New Roman', size=20, weight="bold")

# imageO = PhotoImage(file = "img/dora3.png")

imageR = PhotoImage(file="img/Tour noire.png")
imageB = PhotoImage(file="img/Fou noir.png")
imageN = PhotoImage(file="img/Cavalier noir.png")
imageK = PhotoImage(file="img/Roi noire.png")
imageQ = PhotoImage(file="img/Reine noire.png")
imageP = PhotoImage(file="img/Pion noir.png")
imager = PhotoImage(file="img/Tour blanche.png")
imageb = PhotoImage(file="img/Fou blanc.png")
imagen = PhotoImage(file="img/Cavalier blanc.png")
imagek = PhotoImage(file="img/Roi blanc.png")
imageq = PhotoImage(file="img/Reine blanche.png")
imagep = PhotoImage(file="img/Pion blanc.png")

currentColor = "blancs"
color1 = '#d1a045'
color2 = '#ebc091'
colorbg = "#cf6d04"
chaineB = ["R", "B", "Q", "P", "K", "N"]
chaineW = ["r", "b", "q", "p", "k", "n"]

chaineA = ["R", "B", "Q", "P", "K", "N", "r", "b", "q", "p", "k", "n"]

# photo = PhotoImage(file = r"C:\\Users\ASUS\Desktop\\projet\\des.png")
# bouton de sortie
# bouton=Button(fenAcc, text="Fermer", command=fenAcc.quit)


# label
label = Label(fenAcc, text="Bienvenue sur Deep Echecs", bg="grey", font=police)
label.place(x=150, y=10)


def labelExample(A):
    labelExample = Label(A, text="Jeu en cours: Aux " + currentColor + " de jouer", bg="#ebc091", fg="grey",
                         font=police)
    labelExample.place(x=720, y=10)


def afficher(A, B):
    B = B.split()
    chaine = []
    for i in range(len(B[0])):
        if (B[0][i] in chaineA):
            chaine.append(B[0][i])
        elif (B[0][i] != "/"):
            for j in range(int(B[0][i])):
                chaine.append(" ")
    chaine.append(B[1])
    for k in range(64):
        A[k].config(image=imagerie(chaine[k]))
    A[64].config(text=chaine[64])
    fenAcc.update()

def resetDico(A):
    for cle in range(64):
        A[cle] = 0


def resetDicoCoul(A, B):
    for i in range(64):
        if (B[i] in chaineB):
            A[i] = [B[i], 0, 'B']
        elif (B[i] in chaineW):
            A[i] = [B[i], 0, 'W']
        else:
            A[i] = [B[i], 0, '0']


def commandButton(A, B, D):
    chaine = ''
    for k in range(65):
        chaine = chaine + A[k].cget("text")
    tempstr = ''
    tempint = -1
    tempimg = ''
    # A.config(text= chaine[B])
    if (D[B][1] == 0):
        for i in range(64):
            A[i].config(bg=colorisation(i))
        resetDicoCoul(D, chaine)
        if (D[B][2] == chaine[64]):
            D[B][1] = 2
            A[B].config(bg=colorbg)
            # getcoupslegaux()
            # if(matrice coups == 1):
            # dico de la meme position prend la valeur 1 et la couleur change
            A[B + 8].config(bg="yellow")
            D[B + 8][1] = 1
    elif (D[B][1] == 1):
        for cle in range(64):
            if (D[cle][1] == 2):
                tempint = cle
        tempimg = A[tempint].cget("image")
        tempstr = A[tempint].cget("text")
        for i in range(64):
            if (i == tempint):
                A[tempint].config(text=" ", image="")
            elif (i == B):
                A[B].config(text=tempstr, image=tempimg)
            A[i].config(bg=colorisation(i))
        # l = list(C)
        # l[B] = tempstr
        # C = "".join(l)
        # chaine = chaine[:(B-1)]+tempstr+chaine[B:]
        # # l = list(C)
        # # l[tempint] = ' '
        # # C = "".join(l)
        # chaine = chaine[:(tempint-1)]+' '+chaine[tempint:]
        resetDicoCoul(D, chaine)

        if (chaine[64] == 'B'):
            A[64].config(text="W")
            currentColor = "blancs"
        else:
            A[64].config(text="B")
            currentColor = "noirs"
    else:
        resetDicoCoul(D, chaine)
        for i in range(64):
            A[i].config(bg=colorisation(i))


def imagerie(A):
    if (A == "r"):
        return imageR
    elif (A == "b"):
        return imageB
    elif (A == "n"):
        return imageN
    elif (A == "q"):
        return imageQ
    elif (A == "k"):
        return imageK
    elif (A == "p"):
        return imageP
    elif (A == "R"):
        return imager
    elif (A == "B"):
        return imageb
    elif (A == "N"):
        return imagen
    elif (A == "Q"):
        return imageq
    elif (A == "K"):
        return imagek
    elif (A == "P"):
        return imagep
    else:
        return ""


def popUpFin(A):
    return 0


def colorisation(x):
    if (((x // 8) % 2) == 0 and x % 2 == 0 or ((x // 8) % 2) == 1 and x % 2 == 1):  # )
        # print('--------',x,'',color2)
        return color2
    else:
        # print('--------',x,'',color1)
        return color1


def creerfenetre1v1():
    fen1v1 = Toplevel(fenAcc)
    fen1v1.geometry("1150x700")
    fen1v1.resizable(height=False, width=False)
    dico = {}
    dicocoul = [[None] * 3] * 64
    chaine = 'RNBQKBNRPPPPPPPP                                pppppppprnbqkbnrW'
    resetDico(dico)
    resetDicoCoul(dicocoul, chaine)

    listeButton = [None] * 65

    for num in range(64):
        listeButton[num] = Button(fen1v1,
                                  relief=FLAT,
                                  bg=colorisation(num),
                                  activebackground=colorbg,
                                  bd=0,
                                  text=chaine[num],
                                  image=imagerie(chaine[num]),
                                  command=lambda idx=num: commandButton(listeButton, idx, dicocoul))

        listeButton[num].place(x=10 + (85 * (num % 8)), y=10 + (85 * (num // 8)), height=85, width=85)

    listeButton[64] = Button(fen1v1, text="W")
    listeButton[64].place(x=800, y=400, height=85, width=85)

    labelExample(fen1v1)
    boutonreset = Button(fen1v1, text="Relancer une partie", bg="#2596be", font=police)
    boutonreset.place(x=800, y=305)
    boutonret = Button(fen1v1, text="Retour au menu", bg="#2596be", font=police, command=fen1v1.destroy)
    boutonret.place(x=820, y=550)


from time import sleep


def gameIAvIA(A):
    print("jai ete appele")
    model = keras.models.load_model("RN/RN_128x20_echiq_depart_apres10_iterations_nul_puni_et_10_coups_alea_noir.keras")
    ech = Echiquier()
    game = Game(ech)

    while game.verif_cas_terminaux() == 3:
        '''
        r = [random.uniform(0, 1) for j in range(1880)]

        som = 0
        for i in r:
            som += i

        for i in range(len(r)):
            r[i] = r[i] / som

        som = 0
        for i in r:
            som += i
        '''

        # x = game.get_coup_legaux_NA()

        RN_output = model.predict(np.array([game.echiquier.echiq_to_input()]))
        liste_coups, tab_index_des_noirs = game.get_coup_legaux_NA_RN()
        if tab_index_des_noirs:  # si c'est aux blancs de joué c'est une liste vide
            coups = tab_index_des_noirs
        else:
            coups = [index_coup for index_coup in range(len(liste_coups))]

        les_probas_du_RN = []

        for c in coups:
            m_idx = algebrique_to_index_PRIOR[liste_coups[c]]
            #les_probas_du_RN.append(r[m_idx])
            les_probas_du_RN.append(RN_output[0][0][m_idx])

        plus_grand_prior = 0
        coup_a_jouer = -1

        for c in range(len(coups)):
            if les_probas_du_RN[c] > plus_grand_prior:
                coup_a_jouer = coups[c]
                plus_grand_prior = les_probas_du_RN[c]

        '''        
        RN_output = model.predict(np.array([game.echiquier.echiq_to_input()]))
        B = 0
        coup_a_jouer = -1
        for i in range(len(x)):
            A = RN_output[0][0][
                algebrique_to_index_PRIOR[ x[i] ] ]
        if (A > B):
            coup_a_jouer = i
            B = A
        '''
        # interroger le RN
        # if (coup_a_jouer == -1):
        # error
        print(coups)
        print(coup_a_jouer)
        print(len(coups))
        if (coup_a_jouer != -1):
            game.jouer(coup_a_jouer)
        afficher(A, to_fen(game.echiquier))
        print(to_fen(game.echiquier))
        sleep(1)
    game_res = game.verif_cas_terminaux()
    print("resultatr", game_res)

    # return game.verif_cas_terminaux()


def creerfenetreIAvsIA():
    fenIAvIA = Toplevel(fenAcc)
    fenIAvIA.geometry("1150x700")
    fenIAvIA.resizable(height=False, width=False)
    boutonreset = Button(fenIAvIA, text="Relancer une partie", bg="#2596be", font=police)
    boutonreset.place(x=800, y=150)
    boutonret = Button(fenIAvIA, text="Retour au menu", bg="#2596be", font=police, command=fenIAvIA.destroy)
    boutonret.place(x=820, y=550)
    listeLabel = [None] * 65

    for num in range(64):
        listeLabel[num] = Label(fenIAvIA,
                                bg=colorisation(num))

        listeLabel[num].place(x=10 + (85 * (num % 8)), y=10 + (85 * (num // 8)), height=85, width=85)

    listeLabel[64] = Button(fenIAvIA, text="W")
    listeLabel[64].place(x=800, y=400, height=85, width=85)

    # gameIAvIA(listeLabel,0)
    # 1 WW, -1 BW, 0 Pat MN, 2 50 coups MN

    # popUpFin(finGame)

    #labelExample(fenIAvIA)
    boutonstart = Button(fenIAvIA, text="Lancer une partie", bg="#2596be", font=police,command=lambda x=listeLabel:gameIAvIA(x))
    boutonstart.place(x=800, y=300)

    for num in range(64):
        listeLabel[num].config(image="")
    listeLabel[64].config(text="")


bouton1 = Button(fenAcc, text="1 vs 1", font=police, padx=58, pady=3, bg='#2596be', fg='white',
                 activebackground="#b3ecff", overrelief="raised", command=creerfenetre1v1)
bouton1.place(x=210, y=120)
bouton2 = Button(fenAcc, text="Jouer contre IA", font=police, padx=0, pady=3, bg='#2596be', fg='white',
                 activebackground="#b3ecff", overrelief="raised", command=creerfenetre1v1)
bouton2.place(x=210, y=240)
bouton3 = Button(fenAcc, text="IA vs IA", font=police, padx=44, pady=3, bg='#2596be', fg='white',
                 activebackground="#b3ecff", overrelief="raised", command=creerfenetreIAvsIA)
bouton3.place(x=210, y=360)
boutonFermer = Button(fenAcc, text="Quitter le jeu", bg="#D62e2e", padx=15, pady=3, font=police, command=fenAcc.quit)
boutonFermer.place(x=210, y=525)
fenAcc.mainloop()
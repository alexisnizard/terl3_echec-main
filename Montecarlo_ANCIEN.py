from terl3_echec.RN_utils import *

from terl3_echec.Game import *
import math
import copy


import keras
import numpy as np
from terl3_echec.TESTING.TEST_ECHIQUIER.bitboard_utils import generer_un_echiquier_realiste_aleatoire

from keras.models import Model
from keras.layers import *
import tensorflow as tf

import random

'''
c=0.05 semble pas mal avec v=aleatoire entre -1 et 1 :

noeud fils : 0  son N 1
noeud fils : 1  son N 1
noeud fils : 2  son N 207
noeud fils : 3  son N 1
noeud fils : 4  son N 384
noeud fils : 5  son N 4
noeud fils : 6  son N 1
noeud fils : 7  son N 1
noeud fils : 8  son N 1
noeud fils : 9  son N 35
noeud fils : 10  son N 1
noeud fils : 11  son N 258
noeud fils : 12  son N 3
noeud fils : 13  son N 5
noeud fils : 14  son N 1
noeud fils : 15  son N 4
noeud fils : 16  son N 88
noeud fils : 17  son N 1
noeud fils : 18  son N 1
noeud fils : 19  son N 1
'''

#Chaque noeud est composé de :

# N : Nombre de fois que le noeud a été visité

# W : récompense total ,initalisé à 0, il s'incrémente de v à chaque fois qu'on visite le noeud en remontant de la feuille jusqu'à la racine.
    # v correspond à l'evaluation de la position d'une feuille renvoyé par le RN

# Q : Moyenne de W par rapport à N

# P : Probabilité renvoyé par le Prior (RN) pour le coup donnant la position de notre noeud


class Noeud:
    def __init__(self,index_du_coup,P,pere,fils,instance_game):
        self.coup = index_du_coup
        self.P=P
        self.W=0
        self.Q=0
        self.N=0
        self.noeud_pere=pere
        self.noeud_fils=fils
        self.instance_game=instance_game



class MonteCarlo:
    def __init__(self,instance_game,RN,couleur_depart=0,c=1 ,t=1):
        # l'arguement couleur_depart n'est pas utilisé lorsqu'on entraine le RN mais uniquement lorsqu'on joue la partie pour de vrai et qu'on souhaite être les noirs
        # (car pour entrainé le RN on part à chaque fois de l'echiquier de départ => donc couleur_depart=0 )
        self.couleur_depart=couleur_depart
        self.RN=RN #Notre RN

        self.N=1 #N de la racine
        self.instance_game=instance_game #instance de la game de la racine, elle est deep copied une fois au debut

        self.input_RN = self.instance_game.echiquier.echiq_to_input()
        self.RN_output = RN.predict(np.array([self.input_RN]))
        self.RN_output_Prior=self.RN_output[0][0]

        self.coup_legaux=outputPrior_to_coup_legaux(self.RN_output_Prior,self.instance_game)
        '''
        print("Voici mon echiquier (je suis la racine) : ", to_fen(self.instance_game.echiquier))
        print("Je dois avoir un total de ", len(self.instance_game.get_coup_legaux_NA()), " fils")
        '''
        self.fils=[]
        for coup_lg in self.coup_legaux:
            f=Noeud(coup_lg[0],coup_lg[1],self,[],None) #on calcule pas le nouvelle echiquier pour le fils car il se peut qu'on l'explorera jamais
            self.fils.append(f)

        self.racine = Noeud(0,0, None,self.fils,self.instance_game)
        '''
        print("voici mes fils : ", end='')
        for ll in self.racine.noeud_fils:
            print(ll.coup, " ", end='')

        print()
        print("ils sont au nombre de ", len(self.racine.noeud_fils))
        print()
        '''

        #les deux hyperparamètres :
        self.c=c #utilisé pour UCT
        self.t=t #utilisé lors du renvoit final du coup


    def UCT(self,noeud):
        return self.c * noeud.P * (math.sqrt(noeud.noeud_pere.N)/ 1 + noeud.N)

    def selection(self,noeud):

        if not noeud.noeud_fils: #si c'est une feuille
            return noeud
        else:
            index_de_luct_max=0
            uct_max=-100000000 # on ne l'initalise pas à 0 car un UCT peut être négatif
            #print()
            #On cherche l'UCT MAX
            for ind_fils in range(len(noeud.noeud_fils)):

                uct_courant=self.UCT(noeud.noeud_fils[ind_fils])
                #print("inde", ind_fils, "uct courant", uct_courant, "uct max", uct_max)


                uctVal=self.UCT(noeud.noeud_fils[ind_fils])
                val=noeud.Q

                if noeud.noeud_pere is not None :
                    if (self.couleur_depart and noeud.noeud_pere.instance_game.echiquier.ech[12] & 1) or not noeud.noeud_pere.instance_game.echiquier.ech[12] & 1:
                        val=-noeud.Q

                uctValChild=val+uctVal
                print("uct val child ",uctValChild)

                if uctValChild>uct_max:
                    #print("inde", ind_fils, "uct courant", uct_courant, "uct max", uct_max)
                    uct_max=uctValChild
                    #TODO : faire le cas où y a plusieurs uct identiques on tire aleatoire
                    index_de_luct_max=ind_fils
            #print()

            return self.selection(noeud.noeud_fils[index_de_luct_max])

    def expansion_simulation(self,noeud,RN):

        #debug :
        #coup_debug=noeud.noeud_pere.instance_game.get_coup_legaux_NA()
        #print("Mon pere est ",to_fen(noeud.noeud_pere.instance_game.echiquier)," et je suis l'echiquier apres le coup ",coup_debug[noeud.coup])

        self.N += 1  # on augmente le N de la racine

        # On fait une copy de l'objet Game car on va faire avancé la partie
        # On obtient l'echiquier correspondant à notre Noeud après que notre père ai joué son coup
        game=copy.deepcopy(noeud.noeud_pere.instance_game)

        game_res=game.jouer(noeud.coup)




        noeud.instance_game=game # on ajoute l'echiquier à l'instance_game du noeud
        '''
        
        print("D'ailleur voici mon echiquier : ", to_fen(noeud.instance_game.echiquier))
        print("Je dois avoir un total de ", len(game.get_coup_legaux_NA()), " fils")
        
        '''


        if game_res is not None: #si la game est finit
            v=0.0
            if self.couleur_depart:
                if game_res==1:
                    v=-1.0
                if game_res==-1:
                    v=1.0
            else:
                if game_res==1:
                    v=1.0
                if game_res==-1:
                    v=-1.0

            self.retropropagation(v, noeud)
            return


        input_RN = noeud.instance_game.echiquier.echiq_to_input()

        RN_output = RN.predict(np.array([input_RN]))

        coup_legaux = outputPrior_to_coup_legaux(RN_output[0][0], noeud.instance_game) #Le premier arguement correspond au Prior de notre RN


        for coup_lg in coup_legaux:
            f = Noeud(coup_lg[0],coup_lg[1], noeud, [],None)  # on calcule pas le nouvelle echiquier pour le fils car il se peut qu'on ne l'explorera jamais
            noeud.noeud_fils.append(f)

        '''
        #debug :
        print("voici mes fils : ",end = '')
        for l in noeud.noeud_fils:
            print(l.coup," ",end = '')

        print()
        print("ils sont au nombre de ",len(noeud.noeud_fils))
        print()
        '''

        v=RN_output[1][0][0] #Value network de notre RN

        # Si c'est à l'ennemie de joué on inverse la valeur v ,car le job du RN est juste d'evaluer une position .
        # Si la pos de l'ennemie est favorable alors joué ce coup est défavorable pour nous.
        #if (self.couleur_depart and noeud.instance_game.echiquier.ech[12] & 1) or not noeud.instance_game.echiquier.ech[12] & 1:
        #    v=-v

        self.retropropagation(v,noeud)


    def retropropagation(self,v,noeud):

        #debug :
        noeud.W+=v # on incremente de v la valeur de récompense du noeud (rappel : si notre noeud est une position ennemie v = -v)
        noeud.N+=1 # on incremente la visite du noeud
        noeud.Q = noeud.W/noeud.N # on fait la moyenne des récompense

        if noeud.noeud_pere is not self:# on remonte jusqu'a à la racine
            self.retropropagation(v, noeud.noeud_pere)

    def start(self,nbr_iteration):

        for i in range(nbr_iteration):
            noeud=self.selection(self.racine)
            for j in self.racine.noeud_fils:
                print("noeud fils :",j.coup," son N",j.N)
            print()
            self.expansion_simulation(noeud,self.RN)



model = keras.models.load_model("TESTING/TEST_RN_MONTECARLO_ENTRAINEMENT/test_model_14-8-8.keras")
ech=Echiquier()
game1=Game(ech)

input_RN = game1.echiquier.echiq_to_input()

RN_output = model.predict(np.array([input_RN]))
arr=np.array(outputPrior_to_coup_legaux(RN_output[0][0],game1))
print(np.array(outputPrior_to_coup_legaux(RN_output[0][0],game1)))
minnn=10
maxx=-1000000000
ind_max=0
ind_min=0
print("taile",len(arr))
for l in range(len(arr)):
    print("{:.16f}".format(arr[l][1]))
    if arr[l][1]<minnn:
        minnn=arr[l][1]
        ind_min=l

    if arr[l][1]>maxx:
        maxx=arr[l][1]
        ind_max=l


print("index min : ",ind_min, " index max ",ind_max)

a=MonteCarlo(game1,model)

a.start(100)


Pour tous ces tests :
	- L'échiquier de départ à été utiliser comme première position
	- Nous considérons un coup comme le déplacement d'une pièce blanche puis d'une piece noire.

Exemple: Les blancs avancent un pion ,puis les noirs avancent un pion =>  Nous considérons qu'un coup à été jouer ,et non deux !

2 mins 45 secs en moyenne pour tester 1000 echiquiers sur ALEA 0
2 mins en moyenne pour tester 1000 echiquiers sur ALEA 1

27.0  mins  49.17965745925903  secs pour tester 10 000 echiquiers sur ALEA 0
24.0  mins  27.752373456954956  secs pour tester 10 000 echiquiers sur ALEA 1


===========
Explication de la courbe noir :

Les résultats de la courbe noir s'expliquent par la faible quantité de données à analyser lorsque perd la partie et que l'on s'approche des 13, 14 et 15ème capture de pièce.
On peut voir ici le tableau qu'on obtient pour 200 echiquiers :

[list([8, 19, 19, 10, 15, 9, 14, 21, 23, 43, 7, 15, 12, 9, 29, 14, 9, 10, 14, 20, 29])
 list([24, 21, 29, 14, 51, 19, 29, 23, 24, 45, 28, 23, 17, 11, 30, 20, 38, 19, 49, 34, 32])
 list([25, 26, 57, 25, 52, 37, 25, 25, 63, 39, 28, 29, 16, 31, 25, 61, 22, 59, 42])
 list([39, 27, 69, 61, 57, 33, 32, 40, 33, 36, 32, 31, 70, 29, 64, 64])
 list([48, 40, 71, 64, 65, 38, 35, 45, 37, 42, 36, 41, 83, 37, 77])
 list([50, 44, 77, 68, 67, 39, 37, 47, 41, 45, 54, 54, 98, 44, 84])
 list([59, 83, 69, 77, 43, 40, 71, 47, 58, 70, 56, 100, 55, 86])
 list([63, 84, 85, 78, 54, 43, 81, 50, 60, 71, 60, 103, 56, 90])
 list([71, 91, 90, 89, 78, 47, 90, 58, 67, 63, 59, 101])
 list([72, 101, 106, 96, 83, 51, 95, 89, 71, 70, 68, 109])
 list([79, 116, 103, 52, 96, 77, 80, 74, 141])
 list([86, 118, 119, 60, 121, 85, 114, 93]) list([96, 131, 62, 177, 113])
 list([142, 121, 197]) list([147])]

Pour 200 parties, on est arrivé à capturer la 15 ème pièce de l'adversaire qu'une seule fois lorsqu'on a perdu la game et il nous as fallu 147 coups pour y arrivé. Resultat la moyenne sera de 147 sur le graph.
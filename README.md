# Deep learning pour le jeu d’échecs

### Sujet

L’objectif de ce projet de programmation était de concevoir et de réaliser une intelligence artificielle capable de jouer au jeu d’échecs grâce au Deep learning. Pour ce faire, un paradigme bien précis devait être utilisé : **celui de l’apprentissage par renforcement**.

Aucune heuristique ne devait aider notre intelligence artificielle à choisir quel coup jouer, celle-ci devait apprendre en ne connaissant que le strict minimum, à savoir **les simples règles du jeu**.

Le processus d'apprentissage serait qualifié de succès si l’intelligence artificielle était capable de **mieux jouer qu'un programme choisissant ses coups aléatoirement**.

Dans un premier temps, un long travail en amont a été réalisé afin de mieux comprendre ce qu’est une intelligence artificielle, le domaine du Deep learning en général et surtout, son utilisation pour la problématique donnée.

Une fois ces notions assimilées, un cahier des charges a été établi permettant de mettre en évidence les quatre phases majeures définissant ce projet :
- L’implémentation du jeu d'échecs
- L’implémentation de l’intelligence artificielle
- L'entraînement du réseau de neurones
- L’analyse des résultats

Enfin, par la suite, ont été ajoutés au projet :
- L’implémentation d’une communication avec l’API de Stockfish, un moteur d'échecs très puissant, afin de faire s’affronter ce dernier contre notre intelligence artificielle.
- L’implémentation d’une interface graphique utilisateur nous permettant de voir notre intelligence artificielle jouer.

L’intégralité du code a été réalisé dans le langage de programmation Python, ce dernier disposant de nombreuses bibliothèques adaptées à l'intelligence artificielle.

### Utilisation du programme
L’entraînement du réseau de neurones a été principalement réalisé à l’aide des ordinateurs de Google Collab. Pour avoir accès à celui-ci, il suffit de se connecter au compte Google suivant :

- **Email** : `terl3.echec@gmail.com`
- **Mot de passe** : `terl3echec34`

Ensuite, suivez les étapes ci-dessous :
1. Accédez au drive de ce compte.
2. À l’intérieur du dossier nommé `clean`, ouvrez le notebook `lancer_lentrainement.ipynb`.
3. Il ne reste plus qu’à lancer les cellules qui s’y trouvent à l’intérieur pour démarrer un entraînement.

Lancer le fichier GUI.py permet de jouer contre l'IA via une interface graphique.

### UML du projet
<p align="center">
  <img src="UML.PNG" width="800">
</p>

### Architecture du réseau de neurones
<p align="center">
  <img src="exemple1.png" alt="Exemple d'exécution du programme" width="800">
</p>


### Exemple d'affichage
Ci-dessous, vous trouverez un exemple d'affichage du programme  :

<p align="center">
  <img src="exemple2.PNG" alt="Exemple d'exécution du programme" width="800">
</p>






